﻿using asanito.Repository.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Repository.Modules.Interface.User
{
    public interface IUserRepository 
    {
        Domain.Module.User._User.User selectByUserName(string username);
        Domain.Module.User._User.User Select(Expression<Func<Domain.Module.User._User.User, bool>> expression);

        bool update(Domain.Module.User._User.User model);

        bool delete(Expression<Func<Domain.Module.User._User.User, bool>> expression);

        Domain.Module.User._User.User insert(Domain.Module.User._User.User model);

        List<Domain.Module.User._User.User> SelectListWith(int ID);

        List<Domain.Module.User._User.User> SelectList(Expression<Func<Domain.Module.User._User.User, bool>> expression, int skip
         , int take, bool notDeleted = false);

        List<Domain.Module.User._User.User> SelectDropList(Expression<Func<Domain.Module.User._User.User, bool>> expression, int skip
        , int take, Expression<Func<Domain.Module.User._User.User, int>> orderBy = null);

        void getEmail(int ID, out string email, out string password, out string error);

        List<Domain.Module.User._User.User> SelectList();

        List<Domain.Module.User._User.User> getListByOldSystemID(List<int> oldSystemIDs);

        List<Domain.Module.User._User.User> getUsersForProject(int projectID);

    }
}
