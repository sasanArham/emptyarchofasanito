﻿using asanito.Repository.SeedWorks.Base;
using asanito.Domain.Module.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Repository.Modules.Interface
{
    public interface IUserGroupRepository : IRepository<UserGroup>
    {
        
    }
}
