﻿using asanito.Repository.SeedWorks.Base;
using asanito.Domain.Module.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace asanito.Repository.Modules.Interface.Address
{
    public interface IProvinceRepository : IRepository<Province>
    {
        List<Province> SelectList(Expression<Func<Province, bool>> expression);
    }
}
