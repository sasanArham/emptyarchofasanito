﻿using asanito.Repository.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Repository.Modules.Interface.Address
{
    public interface IAddressRepository : IRepository<Domain.Module.Address._Adress.Adress>
    {
    }
}
