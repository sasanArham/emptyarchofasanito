﻿using asanito.Domain.SeedWorks.Context;
using asanito.Repository.Modules.Interface.Address;
using asanito.Repository.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Repository.Modules.Implement.Address
{
    public class AddressRepository : Repository<Domain.Module.Address._Adress.Adress> , IAddressRepository
    {
        private IDataBaseContext dataBaseContext;

        public AddressRepository(IDataBaseContext dbContext) : base(dbContext)
        {
            dataBaseContext = dbContext;
        }
    }
}
