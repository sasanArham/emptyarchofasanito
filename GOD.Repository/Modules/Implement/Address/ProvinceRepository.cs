﻿using asanito.Domain.SeedWorks.Context;
using asanito.Repository.SeedWorks.Base;
using asanito.Domain.Module.Address;
using asanito.Repository.Modules.Interface.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using System.Linq.Expressions;

namespace asanito.Repository.Modules.Implement.Address
{
    public class ProvinceRepository : Repository<Province> , IProvinceRepository
    {
        private IDataBaseContext databaseContext;

        public ProvinceRepository(IDataBaseContext dbContext) : base(dbContext)
        {
            databaseContext = dbContext;
        }

        

        public  List<Province> SelectList(Expression<Func<Province,bool>> expression)
        {
            try
            {
                var query = DbSet.Where(expression).OrderBy(c => c.Title);
                return query.ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public List<Province> SelectListWith(int ID)
        {
            try
            {
                var query = DbSet.Where(c=> !c.Deleted || c.ID == ID ).OrderBy(c => c.Title);
                return query.ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
