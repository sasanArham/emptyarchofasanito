﻿using asanito.Domain.SeedWorks.Context;
using asanito.Repository.SeedWorks.Base;
using asanito.Domain.Module.Address;
using asanito.Repository.Modules.Interface.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace asanito.Repository.Modules.Implement.Address
{
    public class CityRepository : Repository<City> , ICityRepository
    {
        private IDataBaseContext databaseContext;


        public CityRepository(IDataBaseContext dbContext) : base(dbContext)
        {
            databaseContext = dbContext;
        }


        public List<City> SelectList (Expression<Func<City,bool>> expression)
        {
            try
            {
                var query = DbSet.Where(expression).OrderBy(c => c.Title);
                return query.ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
