﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Repository.Modules.Implement
{
    public static class IQueryableExtensions
    {
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> query, string propertyName, IComparer<object> comparer = null)
        {
            return CallOrderedQueryable(query, "OrderBy", propertyName, comparer);
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> query, List<string> propertyNames, ParameterExpression typeExpr, IComparer<object> comparer = null)
        {
            if (propertyNames.Count == 1)
            {
                return OrderBy(query, propertyNames.First());
            }

            List<Expression> lambdas = new List<Expression>();
            foreach (var item in propertyNames)
            {
                var lambda = createQuery<T>(item, typeExpr);
                lambdas.Add(lambda);
            }

            var expr = createNestedConditionExpression(lambdas);
            return CallOrderedQueryable(query, "OrderBy", expr, typeExpr);

            //for (int i = 0; i < lambdas.Count; i++)
            //{
            //    var nullValue = Expression.Constant(null);
            //    var notEqual = Expression.NotEqual(lambdas[i], nullValue);
            //    var finalLambda = Expression.Condition(notEqual, lambdas[i], lambdas[i + 1]);
            //    return CallOrderedQueryable(query, "OrderBy", finalLambda,typeExpr);
            //}



            return null;
        }

        private static Expression createNestedConditionExpression(List<Expression> lambdas)
        {
            var nullValue = Expression.Constant(null);
            var notEqual = Expression.NotEqual(lambdas.First(), nullValue);
            if (lambdas.Count == 2)
            {
                var expr = Expression.Condition(notEqual, lambdas.First(), lambdas[1]);
                return expr;
            }
            else
            {
                var expr = Expression.Condition(notEqual, lambdas.First(), createNestedConditionExpression(lambdas.Skip(1).ToList()));
                return expr;
            }


        }

        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> query, string propertyName, IComparer<object> comparer = null)
        {
            return CallOrderedQueryable(query, "OrderByDescending", propertyName, comparer);
        }
        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> query, List<string> propertyNames, ParameterExpression typeExpr, IComparer<object> comparer = null)
        {
            if (propertyNames.Count == 1)
            {
                return OrderByDescending(query, propertyNames.First());
            }
            List<Expression> lambdas = new List<Expression>();
            foreach (var item in propertyNames)
            {
                var lambda = createQuery<T>(item, typeExpr);
                lambdas.Add(lambda);
            }

            var expr = createNestedConditionExpression(lambdas);
            return CallOrderedQueryable(query, "OrderByDescending", expr, typeExpr);

            //for (int i = 0; i < lambdas.Count; i++)
            //{
            //    var nullValue = Expression.Constant(null);
            //    var notEqual = Expression.NotEqual(lambdas[i], nullValue);
            //    var finalLambda = Expression.Condition(notEqual, lambdas[i], lambdas[i + 1]);
            //    return CallOrderedQueryable(query, "OrderBy", finalLambda,typeExpr);
            //}



            return null;
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> query, string propertyName, IComparer<object> comparer = null)
        {
            return CallOrderedQueryable(query, "ThenBy", propertyName, comparer);
        }

        public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> query, string propertyName, IComparer<object> comparer = null)
        {
            return CallOrderedQueryable(query, "ThenByDescending", propertyName, comparer);
        }

        /// <summary>
        /// Builds the Queryable functions using a TSource property name.
        /// </summary>
        public static IOrderedQueryable<T> CallOrderedQueryable<T>(this IQueryable<T> query, string methodName, string propertyName,
                IComparer<object> comparer = null)
        {
            var param = Expression.Parameter(typeof(T), "x");

            Expression body;

            if (propertyName.Contains(".Where"))
            {
                string outerProp = propertyName.Substring(0, propertyName.IndexOf(".Where"));
                string innerProp = propertyName.Remove(0, propertyName.IndexOf(".Where")).Replace(".Where.", "");
                var firstPara = outerProp.Split('.').Aggregate<string, Expression>(param, Expression.PropertyOrField);
                var innerObjectType = firstPara.Type.GenericTypeArguments.First();

                MemberExpression propertyOuter = Expression.Property(param, outerProp);

                //Create  First Method
                MethodInfo method = typeof(Enumerable).GetMethods().Where(c => c.Name == "FirstOrDefault").First().MakeGenericMethod(innerObjectType);
                MethodCallExpression callFirst = Expression.Call(method, propertyOuter);



                body = innerProp.Split('.').Aggregate<string, Expression>(callFirst, Expression.PropertyOrField);


            }
            else
            {
                body = propertyName.Split('.').Aggregate<string, Expression>(param, Expression.PropertyOrField);

            }




            return comparer != null
                ? (IOrderedQueryable<T>)query.Provider.CreateQuery(
                    Expression.Call(
                        typeof(Queryable),
                        methodName,
                        new[] { typeof(T), body.Type },
                        query.Expression,
                        Expression.Lambda(body, param),
                        Expression.Constant(comparer)
                    )
                )
                : (IOrderedQueryable<T>)query.Provider.CreateQuery(
                    Expression.Call(
                        typeof(Queryable),
                        methodName,
                        new[] { typeof(T), body.Type },
                        query.Expression,
                        Expression.Lambda(body, param)
                    )
                );
        }

        public static IOrderedQueryable<T> CallOrderedQueryable<T>(this IQueryable<T> query, string methodName, Expression body
               , ParameterExpression param, IComparer<object> comparer = null)
        {
            //var param = Expression.Parameter(typeof(T), "x");





            return comparer != null
                ? (IOrderedQueryable<T>)query.Provider.CreateQuery(
                    Expression.Call(
                        typeof(Queryable),
                        methodName,
                        new[] { typeof(T), body.Type },
                        query.Expression,
                        Expression.Lambda(body, param),
                        Expression.Constant(comparer)
                    )
                )
                : (IOrderedQueryable<T>)query.Provider.CreateQuery(
                    Expression.Call(
                        typeof(Queryable),
                        methodName,
                        new[] { typeof(T), body.Type },
                        query.Expression,
                        Expression.Lambda(body, param)
                    )
                );
        }

        public static Expression createQuery<T>(string propertyName, Expression param, IComparer<object> comparer = null)
        {
            //var param = Expression.Parameter(typeof(T), "x");

            Expression body;

            if (propertyName.Contains(".Where"))
            {
                string outerProp = propertyName.Substring(0, propertyName.IndexOf(".Where"));
                string innerProp = propertyName.Remove(0, propertyName.IndexOf(".Where")).Replace(".Where.", "");
                var firstPara = outerProp.Split('.').Aggregate<string, Expression>(param, Expression.PropertyOrField);
                var innerObjectType = firstPara.Type.GenericTypeArguments.First();

                MemberExpression propertyOuter = Expression.Property(param, outerProp);

                //Create  First Method
                MethodInfo method = typeof(Enumerable).GetMethods().Where(c => c.Name == "FirstOrDefault").First().MakeGenericMethod(innerObjectType);
                MethodCallExpression callFirst = Expression.Call(method, propertyOuter);



                body = innerProp.Split('.').Aggregate<string, Expression>(callFirst, Expression.PropertyOrField);


            }
            else
            {
                body = propertyName.Split('.').Aggregate<string, Expression>(param, Expression.PropertyOrField);

            }




            return body;
        }




    }
}
