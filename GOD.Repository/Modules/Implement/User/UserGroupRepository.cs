﻿using asanito.Repository.SeedWorks.Base;
using asanito.Domain.Module.User;
using asanito.Repository.Modules.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using asanito.Domain.SeedWorks.Context;

namespace asanito.Repository.Modules.Implement.User
{
    public class UserGroupRepository : Repository<UserGroup>, IUserGroupRepository
    {
        private IDataBaseContext context;
        
        
        public UserGroupRepository(IDataBaseContext context) : base(context)
        {
            this.context = context;
        }

        
    }
}
