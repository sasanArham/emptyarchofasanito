﻿using asanito.Repository.SeedWorks.Base;
using asanito.Repository.Modules.Interface.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using asanito.Domain.SeedWorks.Context;
using System.Linq.Expressions;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using asanito.Infrastructure.Consts;
using asanito.Domain.Module.User._User;
using asanito.Infrastructure.Helpers;
using asanito.Infrastructure.Enum.User;

namespace asanito.Repository.Modules.Implement.User
{
    public class UserRepository : IUserRepository
    {
        private DataBaseContext context;
        public UserRepository(IDataBaseContext context)
        {
            this.context = (DataBaseContext)context;
        }

        protected System.Data.Entity.DbSet<Domain.Module.User._User.User> DbSet => this.context.Set<Domain.Module.User._User.User>();

        public Domain.Module.User._User.User selectByUserName(string username)
        {
            try
            {
                var user = DbSet.FirstOrDefault(c => c.Username == username);
                return user;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public Domain.Module.User._User.User Select(Expression<Func<Domain.Module.User._User.User, bool>> expression)
        {
            try
            {
                var user = DbSet.FirstOrDefault(expression);
                return user;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool update(Domain.Module.User._User.User model)
        {
            try
            {

                var entry = this.context.Entry(model);
                entry.State = System.Data.Entity.EntityState.Modified;
                int res = this.context.SaveChanges();

                if (res == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {

                if (e.InnerException == null)
                {
                    return false;
                }
                if (e.InnerException.InnerException == null)
                {
                    return false;
                }

                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 2627:  // Unique constraint error
                            {
                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }
                        case 2601:  // Unique constraint error
                            {

                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        case 547:   // Constraint check violation


                        default:
                            {
                                return false;
                            }
                    }
                }
                else
                {

                    return false;
                }

                //db_context.Dispose();
                //return null;


            }


        }

        public Domain.Module.User._User.User insert(Domain.Module.User._User.User model)
        {


            try
            {

                var newEntry = this.DbSet.Add(model);


                int res = this.context.SaveChanges();

                if (res != 0)
                {

                    model = this.context.User
                        .Include("City")
                        .Include("City.Province")
                        .Include("Stations")
                        .Include("Stations.Station")
                        .Include("UserGroup")
                        .Include("SalarySettings")
                        .Include("SalarySettings.Organization")
                        .SingleOrDefault(c => c.ID == model.ID);


                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    return null;
                }
                if (e.InnerException.InnerException == null)
                {
                    return null;
                }

                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 2627:  // Unique constraint error
                            {
                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }
                        case 2601:  // Unique constraint error
                            {

                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        case 547:
                            {
                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + "پارامتر ارسالی صحیح نمیباشد" + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);
                            }


                        default:
                            {
                                return null;
                            }
                    }
                }
                else
                {

                    return null;
                }

                //db_context.Dispose();
                //return null;



            }


        }

        public bool delete(Expression<Func<Domain.Module.User._User.User, bool>> expression)
        {
            try
            {
                var objects = this.Filter(expression);
                foreach (var obj in objects.ToList())
                {
                    this.DbSet.Remove(obj);
                    int res = this.context.SaveChanges();
                    if (res == 0)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 547:  // cascade error (cannot delete)
                            {
                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + ErrorMessage.deleteCascadeError + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        default:
                            {
                                return false;
                            }
                    }
                }
                else
                {

                    return false;
                }
            }



        }


        public virtual IQueryable<Domain.Module.User._User.User> Filter(Expression<Func<Domain.Module.User._User.User, bool>> expression)
        {
            return this.DbSet.Where(expression).AsQueryable<Domain.Module.User._User.User>();
        }


        List<Domain.Module.User._User.User> IUserRepository.SelectListWith(int ID)
        {
            try
            {
                var result = this.DbSet
                    .Where(c => !c.Deleted || c.ID == ID).ToList();
                return result;
            }
            catch (Exception exp)
            {
                Exception inner = exp.InnerException;
                while (inner.InnerException != null)
                {
                    inner = inner.InnerException;
                }
                return null;
            }
        }

        List<Domain.Module.User._User.User> IUserRepository.SelectList(Expression<Func<Domain.Module.User._User.User, bool>> expression, int skip
         , int take, bool notDeleted = false)
        {
            try
            {
                if (notDeleted)
                {
                    Expression<Func<Domain.Module.User._User.User, bool>> notDeletedExpression = c => c.Deleted == false;

                    var lambda = Expression.Lambda<Func<Domain.Module.User._User.User, bool>>(Expression.AndAlso(
                       new SwapVisitor(expression.Parameters[0], notDeletedExpression.Parameters[0]).Visit(expression.Body),
                       notDeletedExpression.Body), notDeletedExpression.Parameters);


                    List<Domain.Module.User._User.User> result = this.DbSet.Where(lambda).OrderByDescending(c => c.ID).Skip(skip).Take(take).ToList();
                    return result;
                }
                else
                {
                    List<Domain.Module.User._User.User> result = DbSet.Where(expression).OrderByDescending(c => c.ID).Skip(skip).Take(take).ToList();
                    return result;
                }


            }
            catch (Exception exp)
            {
                return null;
            }
        }

        List<Domain.Module.User._User.User> IUserRepository.SelectDropList(Expression<Func<Domain.Module.User._User.User, bool>> expression, int skip
        , int take, Expression<Func<Domain.Module.User._User.User, int>> orderBy = null)
        {
            try
            {
                var query = this.DbSet.Where(expression);

                if (orderBy != null)
                    query = query.OrderBy(orderBy).ThenByDescending(p => p.ID);
                else
                    query = query.OrderByDescending(p => p.ID);

                return query.Skip(skip).Take(take).ToList();
            }
            catch (Exception exp)
            {
                return null;
            }
        }


        public void getEmail(int ID, out string email, out string password, out string error)
        {
            error = "";
            email = "";
            password = "";

            try
            {
                var user = DbSet.FirstOrDefault(c => c.ID == ID);
                if (user == null)
                {
                    error = ErrorMessage.User.notValidID;
                    return;
                }
                email = user.Email;
                if (!string.IsNullOrEmpty(user.AppPassword))
                {
                    password = StringCipher.Decrypt(user.AppPassword);
                }
            }
            catch (Exception e)
            {
                error = ErrorMessage.serverError;
                return;
            }
        }


        List<Domain.Module.User._User.User> IUserRepository.SelectList()
        {
            try
            {
                var result = this.DbSet.Where(c =>!c.Deleted ).ToList();
                return result;
            }
            catch (Exception exp)
            {
                Exception inner = exp.InnerException;
                while (inner.InnerException != null)
                {
                    inner = inner.InnerException;
                }
                return null;
            }
        }


        public List<Domain.Module.User._User.User> getListByOldSystemID(List<int> oldSystemIDs)
        {
            try
            {
                var units = DbSet.Where(c => oldSystemIDs.Contains(c.OldSystemID ?? 0));
                return units.ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Domain.Module.User._User.User> getUsersForProject(int projectID)
        {
            throw new NotImplementedException();
        }
    }
}
