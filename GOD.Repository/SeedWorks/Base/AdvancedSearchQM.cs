﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Repository.SeedWorks.Base
{
    public class AdvancedSearchQM
    {
        public int Take { get; set; }

        public int Skip { get; set; }

        public bool OrderType { get; set; }

        public string SortProperty { get; set; }

        public string Value { get; set; }
    }
}
