﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using asanito.Domain.SeedWorks.Context;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using asanito.Infrastructure.Consts;
using FluentValidation;
using asanito.Domain.SeedWorks.Validator;
using asanito.Domain.SeedWorks.Base;
using System.Data.Entity.Migrations;
using System.Data.Common;
using asanito.Infrastructure.Enum;
using System.Reflection;
using System.ComponentModel;
using Newtonsoft.Json;
using asanito.Infrastructure.Helpers;
using asanito.Repository.Modules.Implement;

namespace asanito.Repository.SeedWorks.Base
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {

        public int UserID { get; set; }
        public void setCurrentUser(int userID)
        {
            this.UserID = userID;
        }


        //protected DataBaseContext Context = null;
        protected DataBaseContext Context;

        private readonly bool shareContext = false;
        //public Repository()
        //{
        //    this.Context = new DataBaseContext();
        //}

        public Repository(IDataBaseContext context)
        {
            this.Context = (DataBaseContext)context;
        }

        public Repository()
        {

        }


        protected DbSet<T> DbSet => this.Context.Set<T>();

        
        public virtual IQueryable<T> TableNoTracking => this.Context.Set<T>().AsNoTracking();

        #region select
        public T Select(Expression<Func<T, bool>> expression, bool notDeleted = false)
        {
            try
            {

                var result = this.DbSet.FirstOrDefault(expression);
                if (notDeleted && result.Deleted)
                {

                    return null;

                }

                return result;
            }
            catch (Exception exp)
            {
                return null;
            }

        }


        public T SelectWithTransaction(Expression<Func<T, bool>> expression, DbTransaction transaction)
        {
            try
            {
                if (Context.Database.CurrentTransaction == null)
                {
                    Context.Database.UseTransaction(transaction);
                }
                var result = this.DbSet.FirstOrDefault(expression);


                return result;
            }
            catch (Exception exp)
            {
                return null;
            }

        }



        public T SelectAsNoTracing(Expression<Func<T, bool>> expression)
        {
            try
            {
                var result = this.DbSet.AsNoTracking().FirstOrDefault(expression);
                return result;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public Domain.Module.User._User.User selectByUserName(string userName)
        {
            try
            {

                var user = Context.User.Where(c => c.Username == userName).ToList()[0];
                return user;
            }
            catch (Exception exp)
            {
                return null;
            }
        }


        public List<T> Select()
        {
            try
            {
                var result = this.DbSet.ToList();
                return result;
            }
            catch (Exception exp)
            {
                return null;
            }

        }


        public int CNT(Expression<Func<T, bool>> expression)
        {
            try
            {
                var result = this.DbSet.Count(expression);
                return result;
            }
            catch (Exception exp)
            {
                return -1;
            }
        }

        public int CNT()
        {
            try
            {
                var result = this.DbSet.Count();
                return result;
            }
            catch (Exception exp)
            {
                return -1;
            }
        }


        public List<T> SelectList(Expression<Func<T, bool>> expression
            , bool notDeleted = false)
        {
            try
            {
                List<T> result = this.DbSet.Where(expression).OrderByDescending(c => c.ID).ToList();
                if (notDeleted)
                {
                    result = result.Where(c => !c.Deleted).OrderByDescending(c => c.ID).ToList();
                }
                return result;
            }
            catch (Exception exp)
            {
                return null;
            }
        }


        public List<T> SelectList(Expression<Func<T, bool>> expression, int skip, int take, bool notDeleted = false)
        {
            try
            {
                if (notDeleted)
                {
                    Expression<Func<T, bool>> notDeletedExpression = c => c.Deleted == false;

                    var lambda = Expression.Lambda<Func<T, bool>>(Expression.AndAlso(
           new SwapVisitor(expression.Parameters[0], notDeletedExpression.Parameters[0]).Visit(expression.Body),
           notDeletedExpression.Body), notDeletedExpression.Parameters);


                    List<T> result = this.DbSet.Where(lambda).OrderByDescending(c => c.ID).Skip(skip).Take(take).ToList();
                    return result;
                }
                else
                {
                    List<T> result = this.DbSet.Where(expression).OrderByDescending(c => c.ID).Skip(skip).Take(take).ToList();
                    return result;
                }


            }
            catch (Exception exp)
            {
                return null;
            }
        }


        public List<T> SelectList(Expression<Func<T, bool>> expression, int skip, int take, List<string> sortProperties
             , out int queriedCnt, bool orderType, bool notDeleted = false)
        {
            queriedCnt = 0;
            try
            {
                IQueryable<T> query;
                if (notDeleted)
                {
                    Expression<Func<T, bool>> notDeletedExpression = c => c.Deleted == false;
                    if (expression != null)
                    {
                        var lambda = Expression.Lambda<Func<T, bool>>(Expression.AndAlso(
                        new SwapVisitor(expression.Parameters[0], notDeletedExpression.Parameters[0]).Visit(expression.Body),
                        notDeletedExpression.Body), notDeletedExpression.Parameters);
                        query = this.DbSet.Where(lambda);
                    }
                    else
                    {
                        query = DbSet.Where(notDeletedExpression);
                    }
                }
                else
                {
                    query = this.DbSet.Where(expression);
                }
                queriedCnt = query.Count();
                if (sortProperties == null)
                {
                    query = query.OrderByDescending(c => c.ID);
                }
                else if (sortProperties.Count == 0)
                {
                    query = query.OrderByDescending(c => c.ID);
                }
                else
                {
                    try
                    {
                        var x = Expression.Parameter(typeof(T), "x");
                        if (orderType)
                        {
                            //query = IQueryableExtensions.OrderBy(query, searchConditions.SortProperty);
                            query = IQueryableExtensions.OrderBy(query, sortProperties, x);
                        }
                        else
                        {
                            query = IQueryableExtensions.OrderByDescending(query, sortProperties, x);
                        }
                    }
                    catch (Exception e)
                    {
                        query = query.OrderByDescending(c => c.ID);
                    }
                }
                var result = query.ToList();
                return result;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
        public List<SV> SelectViewList<SV>(Expression<Func<SV, bool>> expression, int skip, int take, List<string> sortProperties
             , out int queriedCnt, bool orderType) where SV : SqlView
        {
            queriedCnt = 0;
            try
            {
                DbSet<SV> viewDbSet = this.Context.Set<SV>();
                IQueryable<SV> query;
                query = viewDbSet.Where(expression);
                queriedCnt = query.Count();
                if (sortProperties == null)
                {
                    query = IQueryableExtensions.OrderByDescending(query, "ID");
                }
                else if (sortProperties.Count == 0)
                {
                    query = IQueryableExtensions.OrderByDescending(query, "ID");
                }
                else
                {
                    try
                    {
                        var x = Expression.Parameter(typeof(SV), "x");
                        if (orderType)
                        {
                            //query = IQueryableExtensions.OrderBy(query, searchConditions.SortProperty);
                            query = IQueryableExtensions.OrderBy(query, sortProperties, x);
                        }
                        else
                        {
                            query = IQueryableExtensions.OrderByDescending(query, sortProperties, x);
                        }
                    }
                    catch (Exception e)
                    {
                        query = IQueryableExtensions.OrderByDescending(query, "ID");
                    }
                }
                var result = query.ToList();
                return result;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public List<T> SelectListWithTransaction(Expression<Func<T, bool>> expression, DbTransaction transaction)
        {
            try
            {
                if (Context.Database.CurrentTransaction == null)
                {
                    Context.Database.UseTransaction(transaction);
                }
                List<T> result = this.DbSet.Where(expression).OrderByDescending(c => c.ID).ToList();

                return result;
            }
            catch (Exception exp)
            {
                return null;
            }
        }





        //public List<T> SelectDropList(Expression<Func<T, bool>> expression, int skip , int take, int id = 0)
        //{
        //    try
        //    {
        //        var query = this.DbSet.Where(expression).OrderBy(p => (p.ID == id) ? 0 : 1)
        //          .ThenByDescending(p => p.ID).Skip(skip).Take(take);

        //        //var query = this.DbSet.Where(expression).Select(x => new
        //        //{
        //        //    T = x,
        //        //    Sort = x.ID == id ? "0" : "1"
        //        //}).OrderBy(c => c.Sort).ThenByDescending(c => c.T.ID).Skip(skip).Take(take);

        //        return query.ToList();


        //    }
        //    catch (Exception exp)
        //    {
        //        return null;
        //    }
        //}

        public List<T> SelectDropList(Expression<Func<T, bool>> expression, int skip, int take, Expression<Func<T, int>> orderBy = null)
        {
            try
            {
                var query = this.DbSet.Where(expression);

                if (orderBy != null)
                    query = query.OrderBy(orderBy).ThenByDescending(p => p.ID);
                else
                    query = query.OrderByDescending(p => p.ID);

                return query.Skip(skip).Take(take).ToList();
            }
            catch (Exception exp)
            {
                return null;
            }
        }

 


        public List<SV> selectView<SV>(int skip, int take) where SV : SqlView
        {
            DbSet<SV> viewDbSet = this.Context.Set<SV>();
            var query = viewDbSet.Skip(skip).Take(take);
            var result = query.ToList();

            return result;
        }
        public List<SV> selectView<SV>() where SV : SqlView
        {
            DbSet<SV> viewDbSet = this.Context.Set<SV>();
            var query = viewDbSet.AsQueryable();
            var result = query.ToList();

            return result;
        }

        public List<SV> selectView<SV>(Expression<Func<SV, bool>> expression) where SV : SqlView
        {
            DbSet<SV> viewDbSet = this.Context.Set<SV>();
            var query = viewDbSet.Where(expression);
            var result = query.ToList();

            return result;
        }


        private Expression createExpression(string prop, object value, Type type, string typeNotation)
        {
            try
            {
                var x = Expression.Parameter(type, typeNotation);
                MethodInfo containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
                Expression currentBody;

                var para = prop.Split('.').Aggregate<string, Expression>(x, Expression.PropertyOrField);
                var val = Expression.Constant(value.ToString());
                MethodInfo toStringMethod = null;


                if (para.Type == typeof(string))
                {
                    currentBody = Expression.Call(para, containsMethod, val);
                }
                else if (para.Type == typeof(DateTime))
                {
                    toStringMethod = para.Type.GetMethod("ToString", System.Type.EmptyTypes);
                    var toStringExpr = Expression.Call(para, toStringMethod);
                    var date = G.stringToDate(value.ToString());
                    if (date != DateTime.MinValue)
                    {
                        var dateval = Expression.Constant(date.ToString());
                        currentBody = Expression.Equal(toStringExpr, dateval);
                    }
                    else
                    {
                        return null;
                    }

                }
                else if (para.Type == typeof(DateTimeOffset))
                {
                    toStringMethod = para.Type.GetMethod("ToString", System.Type.EmptyTypes);
                    var toStringExpr = Expression.Call(para, toStringMethod);
                    var date = G.stringToDate(value.ToString());
                    if (date != DateTimeOffset.MinValue)
                    {
                        var dateval = Expression.Constant(date.ToString());
                        currentBody = Expression.Equal(toStringExpr, dateval);
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (para.Type.BaseType == (typeof(Enum)))
                {
                    var enumValue = EnumEx.GetValueFromDescription(value.ToString(), para.Type);
                    if (enumValue == null)
                    {
                        return null;
                    }
                    var enumValExpression = Expression.Constant(enumValue);
                    currentBody = Expression.Equal(para, enumValExpression);
                }
                else
                {
                    toStringMethod = para.Type.GetMethod("ToString", System.Type.EmptyTypes);
                    var toStringExpr = Expression.Call(para, toStringMethod);
                    currentBody = Expression.Equal(toStringExpr, val);
                }

                return currentBody;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private Expression createExpression(string prop, object value, Expression x)
        {
            try
            {
                MethodInfo containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
                Expression currentBody;

                var para = prop.Split('.').Aggregate<string, Expression>(x, Expression.PropertyOrField);
                var val = Expression.Constant(value.ToString());
                MethodInfo toStringMethod = null;


                if (para.Type == typeof(string))
                {
                    currentBody = Expression.Call(para, containsMethod, val);
                }
                else if (para.Type == typeof(DateTime) || para.Type == typeof(DateTimeOffset))
                {
                    toStringMethod = para.Type.GetMethod("ToString", System.Type.EmptyTypes);
                    var toStringExpr = Expression.Call(para, toStringMethod);
                    var stringDate = G.incompleteShamsiToStringDate(value.ToString());
                    //var date = G.stringToDate(value.ToString());
                    //if (date != DateTime.MinValue)
                    //{
                    //var dateval = Expression.Constant(date.ToString());
                    var dateval = Expression.Constant(stringDate);
                    //currentBody = Expression.Equal(toStringExpr, dateval);
                    currentBody = Expression.Call(toStringExpr, containsMethod, dateval);
                    //}
                    //else
                    //{
                    //    return null;
                    //}

                }
                //else if (para.Type == typeof(DateTimeOffset))
                //{
                //    toStringMethod = para.Type.GetMethod("ToString", System.Type.EmptyTypes);
                //    var toStringExpr = Expression.Call(para, toStringMethod);
                //    var date = G.stringToDate(value.ToString());
                //    if (date != DateTimeOffset.MinValue)
                //    {
                //        var dateval = Expression.Constant(date.ToString());
                //        currentBody = Expression.Equal(toStringExpr, dateval);
                //    }
                //    else
                //    {
                //        return null;
                //    }
                //}
                else if (para.Type.BaseType == (typeof(Enum)))
                {
                    var enumValue = EnumEx.GetValueFromDescription(value.ToString(), para.Type);
                    if (enumValue == null)
                    {
                        return null;
                    }
                    var enumValExpression = Expression.Constant(enumValue);
                    currentBody = Expression.Equal(para, enumValExpression);
                }
                else
                {
                    toStringMethod = para.Type.GetMethod("ToString", System.Type.EmptyTypes);
                    var toStringExpr = Expression.Call(para, toStringMethod);
                    currentBody = Expression.Equal(toStringExpr, val);
                }

                return currentBody;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        private Expression createNestedExpression(ParameterExpression parameterOuter, object valueObject, Type innerObjectType
            , string outerProp, string innerProp)
        {
            MemberExpression propertyOuter = Expression.Property(parameterOuter, outerProp);

            //Build the comparison inside of the Any clause
            ParameterExpression parameterInner = Expression.Parameter(innerObjectType, "cd");
            var innerExpression = this.createExpression(innerProp, valueObject, parameterInner);
            LambdaExpression lambdaInner = Expression.Lambda(innerExpression, parameterInner);


            //Create Generic Any Method
            Func<MethodInfo, bool> methodLambda = m => m.Name == "Any" && m.GetParameters().Length == 2;
            MethodInfo method = typeof(Enumerable).GetMethods().Where(methodLambda).Single().MakeGenericMethod(innerObjectType);

            //Create the Any Expression Tree and convert it to a Lambda
            MethodCallExpression callAny = Expression.Call(method, propertyOuter, lambdaInner);
            LambdaExpression lambdaAny = Expression.Lambda(callAny, parameterOuter);

            return callAny;
        }






        List<T> IRepository<T>.SelectList()
        {
            try
            {
                List<T> result = this.DbSet.Where(c => !c.Deleted).OrderByDescending(c => c.ID).ToList();
                return result;
            }
            catch (Exception exp)
            {
                Exception inner = exp.InnerException;
                while (inner.InnerException != null)
                {
                    inner = inner.InnerException;
                }
                return null;
            }
        }

        List<T> IRepository<T>.SelectList(int take)
        {
            try
            {
                List<T> result = this.DbSet.Where(c => !c.Deleted).OrderByDescending(c => c.ID).Take(take).ToList();
                return result;
            }
            catch (Exception exp)
            {
                Exception inner = exp.InnerException;
                while (inner.InnerException != null)
                {
                    inner = inner.InnerException;
                }
                return null;
            }
        }

        List<T> IRepository<T>.SelectListWith(int ID)
        {
            try
            {
                List<T> result = this.DbSet
                    .Where(c => !c.Deleted || c.ID == ID).OrderByDescending(c => c.ID).ToList();
                return result;
            }
            catch (Exception exp)
            {
                Exception inner = exp.InnerException;
                while (inner.InnerException != null)
                {
                    inner = inner.InnerException;
                }
                return null;
            }
        }

        public virtual IQueryable<T> Filter(Expression<Func<T, bool>> expression)
        {
            return this.DbSet.Where(expression).AsQueryable<T>();
        }

        #endregion


        public virtual T insert(T model)
        {

            try
            {

                var newEntry = this.DbSet.Add(model);

                int res = this.Context.SaveChanges();

                if (res != 0)
                {
                    // log_z 1
                    //if (model.EntityType != LogableEntityType.NotLogable && (int)model.EntityType != 0)
                    //{
                    //    var activityLog = new ActivityLog(model.EntityType, ActivityLogType.Insert, model.ID, model.CreatorUserID);
                    //    this.insertActivityLog(activityLog);
                    //}
                    //if (model.EntityType != LogableEntityType.NotLogable && (int)model.EntityType != 0)
                    //{
                    //    bool logSaved = this.insertActivityLog(model, ActivityLogType.Insert);
                    //}`
                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    return null;
                }
                if (e.InnerException.InnerException == null)
                {
                    return null;
                }

                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 2627:  // Unique constraint error
                            {
                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }
                        case 2601:  // Unique constraint error
                            {

                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        case 547:
                            {
                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + "پارامتر ارسالی صحیح نمیباشد" + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);
                            }


                        default:
                            {
                                return null;
                            }
                    }
                }
                else
                {

                    return null;
                }

                //db_context.Dispose();
                //return null;



            }


        }

        public virtual T insert(T model, bool saveLog = true)
        {

            try
            {

                var newEntry = this.DbSet.Add(model);

                int res = this.Context.SaveChanges();

                if (res != 0)
                {
                    
                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    return null;
                }
                if (e.InnerException.InnerException == null)
                {
                    return null;
                }

                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 2627:  // Unique constraint error
                            {
                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }
                        case 2601:  // Unique constraint error
                            {

                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        case 547:
                            {
                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + "پارامتر ارسالی صحیح نمیباشد" + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);
                            }


                        default:
                            {
                                return null;
                            }
                    }
                }
                else
                {

                    return null;
                }

                //db_context.Dispose();
                //return null;



            }


        }

        public virtual T insert(T model, DbTransaction transaction = null)
        {


            try
            {
                if (transaction != null)
                {
                    if (Context.Database.CurrentTransaction == null)
                    {
                        Context.Database.UseTransaction(transaction);
                    }

                }

                var newEntry = this.DbSet.Add(model);


                int res = this.Context.SaveChanges();

                if (res != 0)
                {
                    
                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    return null;
                }
                if (e.InnerException.InnerException == null)
                {
                    return null;
                }

                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 2627:  // Unique constraint error
                            {
                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }
                        case 2601:  // Unique constraint error
                            {

                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        case 547:
                            {
                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + "پارامتر ارسالی صحیح نمیباشد" + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);
                            }


                        default:
                            {
                                return null;
                            }
                    }
                }
                else
                {

                    return null;
                }

                //db_context.Dispose();
                //return null;



            }


        }

        public virtual T insertWithCode(T model)
        {

            try
            {
                int code = 0;
                var lastinsertedEntity = DbSet.OrderByDescending(c => c.Code).FirstOrDefault();
                if (lastinsertedEntity != null)
                {
                    code = lastinsertedEntity.Code ?? 0;
                    code++;
                }
                model.Code = code;

                var newEntry = this.DbSet.Add(model);

                int res = this.Context.SaveChanges();

                if (res != 0)
                {
                    
                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    return null;
                }
                if (e.InnerException.InnerException == null)
                {
                    return null;
                }

                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 2627:  // Unique constraint error
                            {
                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }
                        case 2601:  // Unique constraint error
                            {

                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        case 547:
                            {
                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + "پارامتر ارسالی صحیح نمیباشد" + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);
                            }


                        default:
                            {
                                return null;
                            }
                    }
                }
                else
                {

                    return null;
                }

                //db_context.Dispose();
                //return null;



            }


        }
        public virtual T insertWithCode(T model, bool saveLog)
        {

            try
            {
                int code = 0;
                var lastinsertedEntity = DbSet.OrderByDescending(c => c.Code).FirstOrDefault();
                if (lastinsertedEntity != null)
                {
                    code = lastinsertedEntity.Code ?? 0;
                    code++;
                }
                model.Code = code;

                var newEntry = this.DbSet.Add(model);

                int res = this.Context.SaveChanges();

                if (res != 0)
                {
                    
                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    return null;
                }
                if (e.InnerException.InnerException == null)
                {
                    return null;
                }

                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 2627:  // Unique constraint error
                            {
                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }
                        case 2601:  // Unique constraint error
                            {

                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        case 547:
                            {
                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + "پارامتر ارسالی صحیح نمیباشد" + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);
                            }


                        default:
                            {
                                return null;
                            }
                    }
                }
                else
                {

                    return null;
                }

                //db_context.Dispose();
                //return null;



            }


        }

        public T validateAndInsert<V>(T model) where V : BaseValidator<T>
        {

            V validator = (V)Activator.CreateInstance(typeof(V), new object[] { });
            var validateRes = validator.Validate(model);

            if (!validateRes.IsValid)
            {

                string errorMessage = validateRes.Errors[0].ErrorMessage;
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("{\"Message\": \"  " + errorMessage + "   \"}", Encoding.UTF8, "application/json"),
                    ReasonPhrase = "Badrequest",
                };
                throw new HttpResponseException(resp);
            }




            try
            {

                var newEntry = this.DbSet.Add(model);
                int res = this.Context.SaveChanges();

                if (res != 0)
                {
                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    return null;
                }
                if (e.InnerException.InnerException == null)
                {
                    return null;
                }

                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 2627:  // Unique constraint error
                            {
                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }
                        case 2601:  // Unique constraint error
                            {

                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        case 547:   // Constraint check violation


                        default:
                            {
                                return null;
                            }
                    }
                }
                else
                {

                    return null;
                }

                //db_context.Dispose();
                //return null;



            }


        }

        public virtual bool update(T model, DbTransaction transaction = null)
        {
            try
            {
                if (transaction != null)
                {
                    if (Context.Database.CurrentTransaction == null)
                    {
                        Context.Database.UseTransaction(transaction);
                    }
                }
                var entry = this.Context.Entry(model);
                entry.State = EntityState.Modified;
                int res = this.Context.SaveChanges();

                if (res == 0)
                {
                    return false;
                }
                else
                {
                    


                    return true;
                }
            }
            catch (Exception e)
            {

                if (e.InnerException == null)
                {
                    return false;
                }
                if (e.InnerException.InnerException == null)
                {
                    return false;
                }

                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 2627:  // Unique constraint error
                            {
                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }
                        case 2601:  // Unique constraint error
                            {

                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        case 547:   // Constraint check violation


                        default:
                            {
                                return false;
                            }
                    }
                }
                else
                {

                    return false;
                }

                //db_context.Dispose();
                //return null;


            }


        }

        public bool updateWithAttached(T model, DbTransaction transaction = null, List<T> attachedEntities = null)
        {
            try
            {
                if (transaction != null)
                {
                    if (Context.Database.CurrentTransaction == null)
                    {
                        Context.Database.UseTransaction(transaction);
                    }

                }
                var entry = this.Context.Entry(model);
                entry.State = EntityState.Modified;
                if (attachedEntities != null)
                {
                    foreach (var item in attachedEntities)
                    {
                        var attached = this.Context.Entry(item);
                        attached.State = EntityState.Modified;
                    }

                }
                int res = this.Context.SaveChanges();

                if (res == 0)
                {
                    return false;
                }
                else
                {
                    // log_z
                    //if (model.EntityType != LogableEntityType.NotLogable && (int)model.EntityType != 0)
                    //{
                    //    var log = new ActivityLog(model.EntityType, ActivityLogType.Update, model.ID, this.UserID);
                    //    this.insertActivityLog(log);
                    //}
                    //if (model.EntityType != LogableEntityType.NotLogable && (int)model.EntityType != 0)
                    //{
                    //    bool logSaved = this.insertActivityLog(model, ActivityLogType.Update);
                    //}


                    return true;
                }
            }
            catch (Exception e)
            {

                if (e.InnerException == null)
                {
                    return false;
                }
                if (e.InnerException.InnerException == null)
                {
                    return false;
                }

                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 2627:  // Unique constraint error
                            {
                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }
                        case 2601:  // Unique constraint error
                            {

                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        case 547:   // Constraint check violation


                        default:
                            {
                                return false;
                            }
                    }
                }
                else
                {

                    return false;
                }

                //db_context.Dispose();
                //return null;


            }


        }

        public bool delete(T model)
        {

            try
            {
                int deletingEntityID = model.ID;

                var entry = this.Context.Entry(model);
                entry.State = EntityState.Modified;
                DbSet.Remove(model);
                int res = this.Context.SaveChanges();

                if (res == 0)
                {
                    return false;
                }


                return true;
            }
            catch (Exception e)
            {
                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 547:  // cascade error (cannot delete)
                            {
                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + ErrorMessage.deleteCascadeError + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        default:
                            {
                                return false;
                            }
                    }
                }
                else
                {

                    return false;
                }
            }


        }

        private bool deleteNotCascade(T model)
        {

            try
            {

                var entry = this.Context.Entry(model);
                entry.State = EntityState.Modified;
                DbSet.Remove(model);
                int res = this.Context.SaveChanges();

                if (res == 0)
                {
                    return false;
                }


                return true;
            }
            catch (Exception e)
            {
                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 547:  // cascade error (cannot delete)
                            {

                                throw e;

                            }


                        default:
                            {
                                return false;
                            }
                    }
                }
                else
                {

                    return false;
                }
            }


        }



        public bool delete(Expression<Func<T, bool>> expression, DbTransaction transaction = null)
        {
            try
            {
                if (transaction != null)
                {
                    if (Context.Database.CurrentTransaction == null)
                    {
                        Context.Database.UseTransaction(transaction);
                    }

                }
                var objects = this.Filter(expression);
                foreach (var obj in objects.ToList())
                {
                    this.DbSet.Remove(obj);
                    int res = this.Context.SaveChanges();
                    if (res == 0)
                    {
                        return false;
                    }


                }
                return true;
            }
            catch (Exception e)
            {
                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 547:  // cascade error (cannot delete)
                            {
                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + ErrorMessage.deleteCascadeError + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        default:
                            {
                                return false;
                            }
                    }
                }
                else
                {

                    return false;
                }
            }



        }


        public bool deleteOrSetAsDeleted(int ID)
        {
            T item;
            try
            {
                item = DbSet.FirstOrDefault(c => c.ID == ID);
                if (item == null)
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                return false;
            }

            try
            {
                bool deleted = this.deleteNotCascade(item);



                return deleted;
            }
            catch (Exception e)
            {
                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 547:  // cascade error (cannot delete)
                            {
                                item.Deleted = true;
                                bool updated = this.update(item);
                                
                                return updated;
                            }

                        default:
                            {
                                return false;
                            }
                    }
                }
                else
                {
                    return false;
                }
            }

        }



        public void Dispose()
        {
            if (this.shareContext) this.Context?.Dispose();
        }


        public bool addOrUpdate(List<T> models)
        {
            try
            {
                foreach (var item in models)
                {
                    this.DbSet.AddOrUpdate(item);
                }


                int res = this.Context.SaveChanges();

                if (res != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    return false;
                }
                if (e.InnerException.InnerException == null)
                {
                    return false;
                }

                if (e.InnerException.InnerException is SqlException)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    switch (sqlException.Number)
                    {
                        case 2627:  // Unique constraint error
                            {
                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }
                        case 2601:  // Unique constraint error
                            {

                                string duplicateKey = sqlException.Message;
                                int index1 = duplicateKey.IndexOf("(") + 1;
                                int index2 = duplicateKey.IndexOf(")");
                                int len = index2 - index1;
                                duplicateKey = duplicateKey.Substring(index1, len);

                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + duplicateKey + " قبلا استفاده شده است " + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);

                            }


                        case 547:
                            {
                                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                {
                                    Content = new StringContent("{\"Message\": \"  " + "پارامتر ارسالی صحیح نمیباشد" + "   \"}", Encoding.UTF8, "application/json"),
                                    ReasonPhrase = "Badrequest",

                                };
                                throw new HttpResponseException(resp);
                            }


                        default:
                            {
                                return false;
                            }
                    }
                }
                else
                {

                    return false;
                }

                //db_context.Dispose();
                //return null;



            }

        }


        public bool exist(int ID)
        {
            try
            {
                var item = DbSet.FirstOrDefault(c => c.ID == ID);
                if (item == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool exist(Expression<Func<T, bool>> expression)
        {
            try
            {
                var item = DbSet.FirstOrDefault(expression);
                if (item == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool existAndNotDeleted(int ID)
        {
            try
            {
                var item = DbSet.FirstOrDefault(c => c.ID == ID && !c.Deleted);
                if (item == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }




        public bool setAsDeleted(int ID, out string error)
        {
            error = "";
            try
            {
                var model = DbSet.Where(c => c.ID == ID).FirstOrDefault();
                if (model == null)
                {
                    error = ErrorMessage.loadError;
                    return false;
                }
                model.Deleted = true;
                bool updated = this.update(model);
                if (!updated)
                {
                    error = ErrorMessage.deleteError;
                }
                return updated;

            }
            catch (Exception e)
            {
                error = ErrorMessage.serverError;
                return false;
            }

        }


       










        public void test(T entity)
        {
            var type = entity.GetType();
            string g = type.BaseType.Name;
            return;
        }


        public List<T> getListByOldSystemID(List<int> oldSystemIDs)
        {
            try
            {
                var units = DbSet.Where(c => oldSystemIDs.Contains(c.OldSystemID ?? 0));
                var result = units.ToList();
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }


    }
}
