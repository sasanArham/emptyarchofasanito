﻿

using asanito.Domain.Module.User;
using asanito.Domain.SeedWorks.Base;
using asanito.Domain.SeedWorks.Validator;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Repository.SeedWorks.Base
{
    public interface IRepository<T> : IDisposable where T : BaseEntity
    {

        int UserID { get; set; }

        void setCurrentUser(int userID);

        IQueryable<T> TableNoTracking { get; }

        #region select
        T Select(Expression<Func<T, bool>> expression, bool notDeleted = false);

        T SelectWithTransaction(Expression<Func<T, bool>> expression, DbTransaction transaction );
        T SelectAsNoTracing(Expression<Func<T, bool>> expression);

        

        Domain.Module.User._User.User selectByUserName(string userName);
        List<T> Select();

        //User selectByUserName(string userName);

        List<T> SelectList(Expression<Func<T, bool>> expression, bool notDeleted = false);
        List<T> SelectList(Expression<Func<T, bool>> expression, int skip, int take, bool notDeleted = false);

        List<T> SelectList(Expression<Func<T, bool>> expression, int skip, int take, List<string> sortProperties
             , out int queriedCnt, bool orderType, bool notDeleted = false);

        List<T> SelectListWithTransaction(Expression<Func<T, bool>> expression, DbTransaction transaction);

        List<SV> SelectViewList<SV>(Expression<Func<SV, bool>> expression, int skip, int take, List<string> sortProperties
             , out int queriedCnt, bool orderType) where SV : SqlView;

        //List<T> SelectDropList(Expression<Func<T, bool>> expression, int skip, int take, int id = 0);

        List<T> SelectDropList(Expression<Func<T, bool>> expression, int skip, int take, Expression<Func<T, int>> orderBy = null);
        List<T> SelectList();

        List<T> SelectList(int take);

        List<T> SelectListWith(int ID);




        List<SV> selectView<SV>() where SV : SqlView;
        List<SV> selectView<SV>(Expression<Func<SV, bool>> expression) where SV : SqlView;


        #endregion

        T insert(T model, DbTransaction transaction = null);
        T insert(T model);
        T insert(T model, bool saveLog = true);
        T insertWithCode(T model);
        T insertWithCode(T model, bool saveLog);


        T validateAndInsert<V>(T model) where V : BaseValidator<T>;

        bool update(T model, DbTransaction transaction = null);

        bool updateWithAttached(T model, DbTransaction transaction = null, List<T> attachedEntities = null);

        bool delete(T model);
        bool deleteOrSetAsDeleted(int ID);

        bool delete(Expression<Func<T, bool>> expression, DbTransaction transaction = null);



        //UserControllerMethod checkPermission(int userGroupID, int MethodControllerID);

        bool exist(int ID);
        bool exist(Expression<Func<T, bool>> expression);

        bool existAndNotDeleted(int ID);

        bool addOrUpdate(List<T> models);
        int CNT(Expression<Func<T, bool>> expression);
        int CNT();

        bool setAsDeleted(int ID, out string error);

        List<T> getListByOldSystemID(List<int> oldSystemIDs);

        void test(T entity);
    }
}
