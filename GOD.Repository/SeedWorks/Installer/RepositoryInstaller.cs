﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using asanito.Domain.SeedWorks.Installer;
using asanito.Repository.SeedWorks.Base;
using asanito.Repository.Modules.Interface;
using asanito.Repository.Modules.Interface.User;
using asanito.Repository.Modules.Interface.Address;

namespace asanito.Repository.SeedWorks.Installer
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Install(new DomainInstaller());

            



            container.Register(Classes.FromThisAssembly().BasedOn(typeof(IRepository<>)).WithServiceAllInterfaces().LifestyleTransient());

            container.Register(Classes.FromThisAssembly().BasedOn(typeof(IUserGroupRepository)).WithServiceAllInterfaces().LifestyleTransient());
            container.Register(Classes.FromThisAssembly().BasedOn(typeof(IUserRepository)).WithServiceAllInterfaces().LifestyleTransient());


            container.Register(Classes.FromThisAssembly().BasedOn(typeof(ICityRepository)).WithServiceAllInterfaces().LifestyleTransient());
            container.Register(Classes.FromThisAssembly().BasedOn(typeof(IProvinceRepository)).WithServiceAllInterfaces().LifestyleTransient());
            container.Register(Classes.FromThisAssembly().BasedOn(typeof(IAddressRepository)).WithServiceAllInterfaces().LifestyleTransient());

         

        }
    }
}
