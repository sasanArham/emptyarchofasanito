﻿using System;
using System.Linq;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using asanito.Infrastructure.Consts;

using Microsoft.AspNet.SignalR.Hubs;

namespace asanito.WebApi.Hubs
{

    [HubName("Chat")]
    public class NotificationHub : Hub
    {

        private static IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();

        
        



        public void Hello()
        {
            Clients.All.hello("fff");
        }

        public static void SayHello()
        {

            hubContext.Clients.All.hello();

        }


        public override Task OnConnected()
        {
            G.signalR_ConnectionIDs.Add(Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stoprd)
        {
            try
            {
                G.signalR_ConnectionIDs.Remove(Context.ConnectionId);
            }
            catch (Exception)
            {

            }




            return base.OnDisconnected(stoprd);
        }

      


        //public static bool sendMessage(string receiverConnectionID , ConsultationMessageDto messageDto )
        //{
        //    try
        //    {
        //        hubContext.Clients.Client(receiverConnectionID).AddChatMessage(messageDto);
        //        hubContext.Clients.All.hello(messageDto);
        //        hubContext.Clients.All.hello2(receiverConnectionID);

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        G.test = e.ToString();
        //        return false;
        //    }
            
        //}

        //public static bool sendToSenderMessage(string receiverConnectionID, ConsultationMessageDto messageDto)
        //{
        //    try
        //    {
        //        hubContext.Clients.Client(receiverConnectionID).showMyMessage(messageDto);
        //        hubContext.Clients.All.hello2(receiverConnectionID);
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }

        //}




        private int getSenderID(string connectionID)
        {
            try
            {
                string string_senderID = G.signalR_Connections.FirstOrDefault(x => x.Value == connectionID).Key;
                int senderID = Int32.Parse(string_senderID);
                return senderID;
            }
            catch (Exception e)
            {
                return -1;
            }
            
        }






    }
}