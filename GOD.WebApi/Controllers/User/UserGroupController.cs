﻿using asanito.Infrastructure.Consts;
using asanito.Service.Module.Interface.User;
using asanito.Service.Module.Message.User;
using asanito.Service.Module.QueryModel;
using asanito.WebApi.SeedWorks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace asanito.WebApi.Controllers.User
{
    /// <summary>
    /// گروه های کاربری
    /// </summary>
    public class UserGroupController : ApiController
    {
        private IUserGroupService service;

        public UserGroupController(IUserGroupService service)
        {
            this.service = service;
            int userID = service.getCurrentUser(HttpContext.Current.Request.RequestContext);
            this.service.setCurrentUser(userID);
        }



        /// <summary>
        /// افزودن گروه کاربری
        /// </summary>
        /// <param name="title"></param>
        /// /// <param name="description"></param>
        /// <returns></returns>
        [HttpPost]
        [Description("افزودن گروه کاربری")]
        [ResponseType(typeof(UserGroupDto))]
        [RequiredPermission("01")]
        public IHttpActionResult addNew(string title , string description)
        {
            int userID = service.getUserIDByToken(HttpContext.Current.Request.RequestContext);
            string error = "";
            UserGroupDto addedGroup = service.addNew(userID, title, description, out error);
            if (addedGroup == null)
            {
                return BadRequest(error);
            }
            return Ok(addedGroup);

        }


        /// <summary>
        /// لیست همه ی گروه های کاربری
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Description("لیست همه ی گروه های کاربری")]
        [ResponseType(typeof(List<UserGroupDto>))]
        [RequiredPermission("02")]
        public IHttpActionResult getAll()
        {
            List<UserGroupDto> userGroups = service.selectAll();
            return Ok(userGroups);
        }


        /// <summary>
        /// حذف گروه کاربری و انتقال کاربر ها به گروه کاربری دیگر
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete]
        [Description("حذف گروه کاربری ")]
        [RequiredPermission("03")]
        public IHttpActionResult Delete(int ID )
        {
            string error = "";
            bool deleted = service.delete(ID, out error);
            if (!deleted)
            {
                return BadRequest(error);
            }
            return Ok(ErrorMessage.OK);
        }

        /// <summary>
        /// ویرایش
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="title"></param>
        /// /// <param name="description"></param>
        /// <returns></returns>
        [HttpPut]
        [Description("ویرایش")]
        [ResponseType(typeof(UserGroupDto))]
        [RequiredPermission("04")]
        public IHttpActionResult edit(int ID , string title , string description)
        {
            string error = "";
            UserGroupDto edited = service.update(ID, title,description, out error);
            if (edited == null)
            {
                return BadRequest(error);
            }
            return Ok(edited);
        }

        /// <summary>
        /// لیست گروه های کاربری غیر پایه
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Description("لیست گروه های کاربری غیر پایه")]
        [ResponseType(typeof(List<UserGroupDto>))]
        [RequiredPermission("05")]
        public IHttpActionResult getNotBaseGroups()
        {
            List<UserGroupDto> userGroups = service.selectNotBaseGroups();
            return Ok(userGroups);
        }

        /// <summary>
        /// دراپ داون لیست گروه های کاربری غیر پایه با امکان جستجو
        /// </summary>
        /// <param name="qm"></param>
        /// <returns></returns>
        [HttpPost]
        [Description("دراپ داون لیست گروه های کاربری غیر پایه با امکان جستجو")]
        [ResponseType(typeof(List<UserGroupDto>))]
        [RequiredPermission("06")]
        public IHttpActionResult getDropNotBaseGroups(DropQM qm)
        {
            List<UserGroupDto> userGroups = service.selectDropNotBaseGroups(qm);
            return Ok(userGroups);
        }




    }
}
