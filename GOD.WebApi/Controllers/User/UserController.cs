﻿using asanito.Infrastructure.Consts;
using asanito.WebApi.SeedWorks.OAuth.Repository;
using asanito.Infrastructure.Enum.User;
using asanito.Service.Module.Interface.User;
using asanito.Service.Module.Message.User;
using asanito.WebApi.SeedWorks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using asanito.Service.Module.QueryModel.User;
using asanito.Service.Module.Message;
using asanito.Service.Module.QueryModel;

namespace asanito.WebApi.Controllers.User
{
    /// <summary>
    /// کاربر
    /// </summary>
    public class UserController : ApiController
    {
        private IUserService service;
        

        public UserController(IUserService service )
        {
            this.service = service;
        }


        /// <summary>
        /// لاگین
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="clientID"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(UserDto))]
        public async Task<IHttpActionResult> login(string userName, string password, string clientID)
        {
            try
            {
                string error = "";
                UserDto user = await service.loginAsync(userName, password, clientID);
                if (user == null)
                {
                    return BadRequest(ErrorMessage.User.loginError);
                }
                if (user.UserStatus.ID == (int)UserStatus.deleted)
                {
                    return BadRequest(ErrorMessage.User.notValidID);
                }
                return Ok(user);
            }
            catch (Exception e)
            {
                return BadRequest(ErrorMessage.serverError);
            }



        }



        /// <summary>
        /// getUserByToken
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(UserDto))]
        [Authorize]
        public IHttpActionResult  getUserByToken()
        {
            UserDto user = service.getUserDtoByToken(HttpContext.Current.Request.RequestContext);
            return Ok(user);
        }


        /// <summary>
        /// بررسی ولید بودن توکن
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet, Description("بررسی ولید بودن توکن")]
        public IHttpActionResult checkToken()
        {
            return Ok(ErrorMessage.OK);
        }

        
        /// <summary>
        /// لیست کاربران
        /// </summary>
        /// <param name="userGroupID"></param>
        /// <returns></returns>
        [HttpGet]
        [Description("لیست کاربران")]
        [ResponseType(typeof(List<UserForPanelDto>))]
        [RequiredPermission("01")]
        public IHttpActionResult getList(int userGroupID)
        {
            string error = "";
            List<UserForPanelDto> users = service.getUsers(userGroupID, out error);
            if (!error.Equals(""))
            {
                return BadRequest(error);
            }
            return Ok(users);
        }

        
        /// <summary>
        /// بازیابی رمز عبور
        /// </summary>
        /// <param name="usernaeOrMobile"></param>
        /// <returns></returns>
        [HttpPut]
        [Description("بازیابی رمز عبور")]
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> resetPassword(string usernaeOrMobile)
        {
            Domain.Module.User._User.User user = service.selectModel(c => c.Username == usernaeOrMobile);
            if (user != null)
            {
                switch (user.UserStatus)
                {
                    case UserStatus.active:
                        {
                            string randomGeneratedPassword = G.createRandomString(8);

                            AuthRepository _repo = new AuthRepository();
                            bool reseted = await _repo.resetPassword(usernaeOrMobile, randomGeneratedPassword);
                            if (!reseted)
                            {
                                return BadRequest(ErrorMessage.insertError);
                            }

                            bool sent = service.sendSMS(randomGeneratedPassword, user.Mobile, Int32.Parse(G.newPassword_smsTemplate));
                            if (!sent)
                            {
                                return BadRequest(ErrorMessage.Activation.smsNotSent);
                            }

                            return Ok(user.Mobile.Substring(0, 2) + "*******" + user.Mobile.Substring(user.Mobile.Length - 2, 2));

                        }
                    
                    case UserStatus.deleted:
                        {
                            return BadRequest(ErrorMessage.User.notValidID);
                        }

                    default:
                        {
                            return BadRequest(ErrorMessage.serverError);
                        }

                }
            }

            user = service.selectModel(c => c.Mobile == usernaeOrMobile);
            if (user == null)
            {
                return BadRequest(ErrorMessage.User.userNotFound);
            }
            else
            {
                switch (user.UserStatus)
                {
                    case UserStatus.active:
                        {
                            string username = user.Username;
                            string randomGeneratedPassword = G.createRandomString(8);

                            AuthRepository _repo = new AuthRepository();
                            bool reseted = await _repo.resetPassword(username, randomGeneratedPassword);
                            if (!reseted)
                            {
                                return BadRequest(ErrorMessage.insertError);
                            }

                            bool sent = service.sendSMS(randomGeneratedPassword, user.Mobile, Int32.Parse(G.newPassword_smsTemplate));
                            if (!sent)
                            {
                                return BadRequest(ErrorMessage.Activation.smsNotSent);
                            }

                            return Ok(user.Mobile);

                        }
                    case UserStatus.deleted:
                        {
                            return BadRequest(ErrorMessage.User.notValidID);
                        }

                    

                    default:
                        {
                            return BadRequest(ErrorMessage.serverError);
                        }

                }
            }
        }

        
        /// <summary>
        /// تغییر رمز عبور کاربر سایت
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        [HttpPut]
        [Description("تغییر رمز عبور")]
        [RequiredPermission("2")]
        public async Task<IHttpActionResult> changePasswordAsync(string oldPassword, string newPassword)
        {
            var user = service.getUserModelByToken(HttpContext.Current.Request.RequestContext);


            AuthRepository repo = new AuthRepository();

            #region change password 

            bool changed = false;

            if (oldPassword.Trim() != "" || newPassword.Trim() != "")
            {
                if (newPassword.Trim().Length < 6)
                {
                    return BadRequest(ErrorMessage.pawwsordCharaterLimit);
                }
                if (oldPassword.Trim() == "")
                {
                    return BadRequest(ErrorMessage.User.enterCurentPassword);
                }
                else
                {
                    changed = await repo.updateUserPassword(user.Username, oldPassword, newPassword);
                    if (!changed)
                    {
                        return BadRequest(ErrorMessage.User.wrongCurrentPassword);
                    }
                    return Ok(ErrorMessage.OK);
                }
            }
            return BadRequest(ErrorMessage.requaredParams);

            #endregion



        }


        
        /// <summary>
        /// ریست رمز عبور کاربر از پنل
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpPut]
        [Description("ریست رمز عبور کاربر از پنل")]
        //[RequiredPermission("3")]
        public async Task<IHttpActionResult> resetUserPasswordAsync(int userID)
        {
            var user = service.selectModelByID(userID);
            if (user == null)
            {
                return BadRequest(ErrorMessage.User.notValidID);
            }


            AuthRepository repo = new AuthRepository();

            #region change password 

            string randomGeneratedPassword = G.createRandomString(8);

            AuthRepository _repo = new AuthRepository();
            bool reseted = await _repo.resetPassword(user.Username, randomGeneratedPassword);
            if (!reseted)
            {
                return BadRequest(ErrorMessage.insertError);
            }

            bool sent = service.sendSMS(randomGeneratedPassword, user.Mobile , Int32.Parse(G.newPassword_smsTemplate));
            if (!sent)
            {
                return BadRequest(ErrorMessage.Activation.smsNotSent);
            }
            return Ok(ErrorMessage.OK);

            #endregion



        }



        /// <summary>
        /// ثبت جدید
        /// </summary>
        /// <param name="qm"></param>
        /// <returns></returns>
        [HttpPost]
        [Description("ثبت جدید")]
        [ResponseType(typeof(UserForPanelDto))]
        [RequiredPermission("4")]
        public async Task<IHttpActionResult> addNew(UserAddQM qm)
        {
            int userID = 0 ;//activationService.getUserIDByToken(HttpContext.Current.Request.RequestContext);
            string error = "";
            UserForPanelDto addedUser = service.createNewUser(userID,  qm, out error);

            if (addedUser == null)
            {
                return BadRequest(error);
            }

            AuthRepository _repo = new AuthRepository();
            IdentityResult result = await _repo.RegisterUser(qm.Username, qm.Password);
            string errorResult = GetErrorResult(result);
            if (errorResult != "")
            {
                service.delete(qm.Username);
                return BadRequest(errorResult);
            }

            return Ok(addedUser);
        }


        /// <summary>
        /// ویرایش
        /// </summary>
        /// <param name="qm"></param>
        /// <returns></returns>
        [HttpPut]
        [Description("ویرایش")]
        [ResponseType(typeof(UserForPanelDto))]
        [RequiredPermission("5")]
        public IHttpActionResult edit (UserEditQM qm)
        {
            string error = "";
            UserForPanelDto edited = service.edit(qm, out error);
            if (edited == null)
            {
                return BadRequest(error);
            }
            return Ok(edited);
        }


        /// <summary>
        /// حذف
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete]
        [Description("حذف")]
        [RequiredPermission("6")]
        public IHttpActionResult delete(int ID)
        {
            string error = "";
            bool deleted = service.setAsDeleted(ID,out error);
            if (!deleted)
            {
                return BadRequest(error);
            }
            return Ok(ErrorMessage.OK);
        }


        /// <summary>
        /// لیست کاربران
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [Description("لیست کاربران")]
        [ResponseType(typeof(List<DropDto> ))]
        [RequiredPermission("7")]
        public IHttpActionResult getListWith(int ID)
        {
            var users = service.getListWith(ID);
            return Ok(users);
        }



        /// <summary>
        /// افزودن مرکز برای کاربر
        /// stationID = 0 means all stations
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="stationID"></param>
        /// <returns></returns>
        [HttpPost]
        [Description("افزودن مرکز برای کاربر")]
        [RequiredPermission("8")]
        public IHttpActionResult addStation(int userID , int stationID)
        {
            var currentUser = service.getUserModelByToken(HttpContext.Current.Request.RequestContext);
            string error = "";
            bool added = service.addStationToUser(currentUser.ID, userID, stationID, out error);
            if (!added)
            {
                return BadRequest(error);
            }
            return Ok(ErrorMessage.OK);
        }



        /// <summary>
        /// حذف مرکز از کاربر
        /// stationID = 0 means all stations
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="stattionID"></param>
        /// <returns></returns>
        [HttpDelete]
        [Description("حذف مرکز از کاربر")]
        [RequiredPermission("9")]
        public IHttpActionResult deleteStation(int userID , int stattionID)
        {
            bool deleted = service.deltedStattion(userID, stattionID);
            if (!deleted)
            {
                return BadRequest(ErrorMessage.insertError);
            }
            return Ok(ErrorMessage.OK);
        }




        /// <summary>
        /// ایمپورت از اکسل
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Description("ایمپورت از اکسل")]
        [RequiredPermission("10")]
        public IHttpActionResult createListFromExcel()
        {
            var user = service.getUserDtoByToken(HttpContext.Current.Request.RequestContext);

            try
            {
                string error = "";
                var file = HttpContext.Current.Request.Files[0];
                bool result = service.createListFromExcel(user.ID, file, out error);
                if (!result)
                {
                    return BadRequest(error);
                }
                return Ok(ErrorMessage.OK);
            }
            catch (Exception e)
            {
                return BadRequest(ErrorMessage.noFileChoosen);
            }



            

        }



        /// <summary>
        /// آپلود عکس پروفایل
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Description("آپلود عکس پروفایل")]
        [ResponseType(typeof(string))]
        [RequiredPermission("11")]
        public IHttpActionResult uploadProfileImage()
        {
            var user = service.getUserDtoByToken(HttpContext.Current.Request.RequestContext);
            try
            {
                int fileCnt = HttpContext.Current.Request.Files.Count;
                if (fileCnt == 0)
                {
                    return BadRequest(ErrorMessage.noFileChoosen);
                }
            }
            catch (Exception e)
            {
                return BadRequest(ErrorMessage.noFileChoosen);
            }
            string error = "";
            string uploaded = service.uploadProfileImage(user.ID, HttpContext.Current.Request.Files[0], out error);
            if (uploaded == "")
            {
                return BadRequest(error);
            }
            return Ok(uploaded);
        }




        /// <summary>
        /// لیست کاربران برای پروژه
        /// </summary>
        /// <param name="projectID"></param>
        /// <returns></returns>
        [HttpGet]
        [Description("لیست کاربران برای پروژه")]
        [ResponseType(typeof(List<SelectedDropWithIconDto>))]
        [RequiredPermission("12")]
        public IHttpActionResult getUsersForProject(int projectID)
        {
            var users = service.getUsersForProject(projectID);
            return Ok(users);
        }



        private string GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return ErrorMessage.serverError;
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    string err = result.Errors.ToList()[0];
                    switch (err)
                    {
                        case "Name test is already taken.":
                            {
                                return ErrorMessage.User.alreadyExsistUsername;
                            }
                        case "Passwords must be at least 6 characters.":
                            {
                                return ErrorMessage.pawwsordCharaterLimit;
                            }
                        default:
                            {
                                return ErrorMessage.serverError;
                            }

                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return ErrorMessage.serverError;
                }


            }

            return "";
        }


        /// <summary>
        /// دراپ داون لیست کاربران با امکان جستجو
        /// </summary>
        /// <param name="qm"></param>
        /// <returns></returns>
        [HttpPost]
        [Description("دراپ داون لیست کاربران با امکان جستجو")]
        [ResponseType(typeof(List<DropDto>))]
        [RequiredPermission("13")]
        public IHttpActionResult getDropListWith(DropQM qm)
        {
            var users = service.getDropListWith(qm);
            return Ok(users);
        }

    }
}
