﻿using asanito.Infrastructure.Consts;
using asanito.Service.Module.Interface.Address;
using asanito.Service.Module.Message.Address;
using asanito.Service.Module.QueryModel;
using asanito.WebApi.SeedWorks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace asanito.WebApi.Controllers.Address
{
    /// <summary>
    /// استان
    /// </summary>
    public class ProvinceController : ApiController
    {
        private IProvinceService service;

        public ProvinceController(IProvinceService service)
        {
            this.service = service;
            int userID = service.getCurrentUser(HttpContext.Current.Request.RequestContext);
            this.service.setCurrentUser(userID);
        }



        /// <summary>
        /// ثبت استان جدید
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        [HttpPost]
        [Description("ثبت استان جدید")]
        [RequiredPermission("01")]
        public IHttpActionResult add(string title)
        {
            string error = "";
            bool added = service.addNew(title, out error);
            if (!added)
            {
                return BadRequest(error);
            }
            return Ok(ErrorMessage.OK);
        }


        /// <summary>
        /// ویرایش
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        [HttpPut]
        [Description("ویرایش")]
        [RequiredPermission("02")]
        public IHttpActionResult edit(int ID , string title)
        {
            string error = "";
            bool updated = service.update(ID, title, out error);
            if (!updated)
            {
                return BadRequest(error);
            }
            return Ok(ErrorMessage.OK);
        }


        /// <summary>
        /// حذف
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete]
        [Description("حذف")]
        [RequiredPermission("03")]
        public IHttpActionResult delete(int ID)
        {
            string error = "";
            bool deleted = service.delete(ID, out error);
            if (!deleted)
            {
                return BadRequest(error);
            }
            return Ok(ErrorMessage.OK);
        }


        /// <summary>
        /// لیست استان ها
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<ProvinceDto>))]
        [Description("لیست استان ها")]
        
        public IHttpActionResult getList()
        {
            List<ProvinceDto> provinces = service.getList();
            return Ok(provinces);
        }


        /// <summary>
        /// لیست استان ها
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<ProvinceDto>))]
        [Description("لیست استان ها")]
        public IHttpActionResult getListWith(int ID)
        {
            List<ProvinceDto> provinces = service.getListWith(ID);
            return Ok(provinces);
        }

        /// <summary>
        /// دراپ داون لیست استان ها با امکان جستجو
        /// </summary>
        /// <param name="qm"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(List<ProvinceDto>))]
        [Description("دراپ داون لیست استان ها با امکان جستجو")]
        public IHttpActionResult getDropListWith(DropQM qm)
        {
            List<ProvinceDto> provinces = service.getDropListWith(qm);
            return Ok(provinces);
        }

    }

}
