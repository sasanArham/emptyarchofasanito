﻿using asanito.Infrastructure.Consts;
using asanito.Service.Module.Interface.Address;
using asanito.Service.Module.Message.Address;
using asanito.WebApi.SeedWorks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace asanito.WebApi.Controllers.Address
{
    /// <summary>
    /// آدرس
    /// </summary>
    public class AdressController : ApiController
    {
        private IAdressService service;

        public AdressController(IAdressService service)
        {
            this.service = service;

            int userID = service.getCurrentUser(HttpContext.Current.Request.RequestContext);
            this.service.setCurrentUser(userID);
        }


        /// <summary>
        /// ویرایش
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="cityID"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        [HttpPut]
        [Description("ویرایش")]
        [RequiredPermission("01")]
        public IHttpActionResult edit(int ID , int cityID , string address)
        {
            string error = "";
            bool edited = service.edit(ID, cityID, address, out error);
            if (!edited)
            {
                return BadRequest(error);
            }
            return Ok(ErrorMessage.OK);
        }


        /// <summary>
        /// حذف
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete]
        [Description("حذف")]
        [RequiredPermission("02")]
        public IHttpActionResult delete(int ID)
        {
            bool deleted = service.deleteOrSetAsDeleted(ID);
            if (!deleted)
            {
                return BadRequest(ErrorMessage.deleteError);
            }
            return Ok(ErrorMessage.OK);
        }



        /// <summary>
        /// افزودن آدرس جدید برای شخص
        /// </summary>
        /// <param name="personID"></param>
        /// <param name="cityID"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        [HttpPost]
        [Description("افزودن آدرس جدید برای شخص")]
        [ResponseType(typeof(AddressDto))]
        [RequiredPermission("03")]
        public IHttpActionResult addNewToPerson(int personID , int cityID , string address)
        {
            int userID = service.getUserIDByToken(HttpContext.Current.Request.RequestContext);
            string error = "";
            AddressDto added = service.addNewToPerson(userID, personID, cityID, address, out error);
            if (added == null)
            {
                return BadRequest(error);
            }
            return Ok(added);
        }



        /// <summary>
        /// افزودن آدرس جدید برای شرکت
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="cityID"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        [HttpPost]
        [Description("افزودن آدرس جدید برای شرکت")]
        [ResponseType(typeof(AddressDto))]
        [RequiredPermission("04")]
        public IHttpActionResult addNewToCompanyID(int companyID, int cityID, string address)
        {
            int userID = service.getUserIDByToken(HttpContext.Current.Request.RequestContext);
            string error = "";
            AddressDto added = service.addNewToComany(userID, companyID, cityID, address, out error);
            if (added == null)
            {
                return BadRequest(error);
            }
            return Ok(added);
        }
    }
}
