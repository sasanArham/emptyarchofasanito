﻿using asanito.Infrastructure.Consts;
using asanito.Service.Module.Interface.Address;
using asanito.Service.Module.Message.Address;
using asanito.Service.Module.QueryModel;
using asanito.WebApi.SeedWorks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace asanito.WebApi.Controllers.Address
{
    /// <summary>
    /// شهر
    /// </summary>
    public class CityController : ApiController
    {
        private ICityService service;

        public CityController(ICityService service)
        {
            this.service = service;
            int userID = service.getCurrentUser(HttpContext.Current.Request.RequestContext);
            this.service.setCurrentUser(userID);
        }


        /// <summary>
        /// ثبت شهر جدید
        /// </summary>
        /// <param name="provinceID"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        [HttpPost]
        [Description("ثبت شهر جدید")]
        [RequiredPermission("01")]
        public IHttpActionResult add(int provinceID, string title)
        {
            string error = "";
            bool added = service.addNew(provinceID, title, out error);
            if (!added)
            {
                return BadRequest(error);
            }
            return Ok(ErrorMessage.OK);
        }


        /// <summary>
        /// ویرایش
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="provinceID"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        [HttpPut]
        [Description("ویرایش")]
        [RequiredPermission("02")]
        public IHttpActionResult edit(int ID, int provinceID, string title)
        {
            string error = "";
            bool updated = service.update(ID, provinceID, title, out error);
            if (!updated)
            {
                return BadRequest(error);
            }
            return Ok(ErrorMessage.OK);
        }


        /// <summary>
        /// حذف
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete]
        [Description("حذف")]
        [RequiredPermission("03")]
        public IHttpActionResult delete(int ID)
        {
            string error = "";
            bool deleted = service.delete(ID, out error);
            if (!deleted)
            {
                return BadRequest(error);
            }
            return Ok(ErrorMessage.OK);
        }

        /// <summary>
        /// لیست شهر های استان
        /// </summary>
        /// <param name="provinceID"></param>
        /// <returns></returns>
        [HttpGet]
        [Description("لیست شهر های استان")]
        [ResponseType(typeof(List<CityDto>))]
        public IHttpActionResult getList(int provinceID)
        {
            List<CityDto> cities = service.getList(provinceID);
            return Ok(cities);
        }


        /// <summary>
        /// لیست شهر های استان
        /// </summary>
        /// <param name="provinceID"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [Description("لیست شهر های استان")]
        [ResponseType(typeof(List<CityDto>))]
        public IHttpActionResult getListWith(int provinceID, int ID)
        {
            List<CityDto> cities  = service.getList(provinceID,ID);
            return Ok(cities);
        }


        /// <summary>
        /// دراپ داون لیسا شهر های استان با امکان جستجو
        /// </summary>
        /// <param name="provinceID"></param>
        /// <param name="qm"></param>
        /// <returns></returns>
        [HttpPost]
        [Description("دراپ داون لیست شهر های استان با امکان جستجو")]
        [ResponseType(typeof(List<CityDto>))]
        public IHttpActionResult getDropListWith(int provinceID, DropQM qm)
        {
            List<CityDto> cities = service.getDropList(provinceID, qm);
            return Ok(cities);
        }


        /// <summary>
        /// لیست شهر ها بر اساس آی دی های استان ها
        /// </summary>
        /// <param name="provinceIDs"></param>
        /// <returns></returns>
        [HttpGet]
        [Description("لیست شهر ها بر اساس آی دی های استان ها")]
        [ResponseType(typeof(List<CityDto>))]
        public IHttpActionResult getByProvinceIDs(string provinceIDs)
        {
            var cities = service.getByProvinceIDs(provinceIDs);
            return Ok(cities);
        }

    }
}
