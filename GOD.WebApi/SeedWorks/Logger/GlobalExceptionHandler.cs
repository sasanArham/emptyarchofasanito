﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;
using asanito.Domain.SeedWorks.Context;

namespace asanito.WebApi.SeedWorks.Logger
{
    public class GlobalExceptionHandler : ExceptionHandler
    {
        static GlobalExceptionHandler()
        {
            logDir = Path.Combine(HttpContext.Current.Server.MapPath("~/App_Data/"), "logs");

            if (!Directory.Exists(logDir))
                Directory.CreateDirectory(logDir);
        }

        static string logDir;
        public async override Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            ManageException(context);

            await base.HandleAsync(context, cancellationToken);
        }

        public async Task ManageException(ExceptionHandlerContext context)
        {
            var ex = context.Exception;

            try
            {
                using (var dbContext = new DataBaseContext())
                {
                    // sasan re write 
                    //dbContext.Logs.Add(new Domain.Module.Logger.Log(ex));
                    //await dbContext.SaveChangesAsync(); 
                }
            }
            catch
            {
            }

            try
            {
                var target = Path.Combine(logDir, $"{DateTime.Now:yyyy-MM-dd-HH-mm-ss-ffffff}.log.json");
                var exJson = JsonConvert.SerializeObject(ex);

                File.WriteAllText(target, exJson);
            }
            catch
            {
                // shit
            }
        }
    }
}