﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using asanito.WebApi.SeedWorks.StartUp;
using System.Web.Http;
using asanito.WebApi.SeedWorks.Installer;
using asanito.WebApi.SeedWorks.Resolvers;
using System.Web.Mvc;
using asanito.WebApi.SeedWorks.Configurations;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using asanito.WebApi.SeedWorks.OAuth.Provider;
using Microsoft.AspNet.SignalR;
using System.Web.Http.Dispatcher;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Http.Description;
using asanito.WebApi.SeedWorks;
using asanito.Service.Module.Implement.User;
using asanito.Service.Module.Interface.User;
using asanito.Domain.SeedWorks.Context;
using asanito.Repository.Modules.Implement.User;

[assembly: OwinStartup(typeof(Startup))]

namespace asanito.WebApi.SeedWorks.StartUp
{
    public class Startup
    {


        public static HttpConfiguration config;
     
        public void Configuration(IAppBuilder app)
        {
            // Branch the pipeline here for requests that start with "/signalr"
            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                // Setup the CORS middleware to run before SignalR.
                // By default this will allow all origins. You can 
                // configure the set of origins and/or http verbs by
                // providing a cors options with a different policy.
                //map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration
                {
                    // You can enable JSONP by uncommenting line below.
                    // JSONP requests are insecure but some older browsers (and some
                    // versions of IE) require JSONP to work cross domain
                    EnableJSONP = true,
                    EnableJavaScriptProxies = true,
                    EnableDetailedErrors = true
                };
                // Run the SignalR pipeline. We're not using MapSignalR
                // since this branch already runs under the "/signalr"
                // path.
                map.RunSignalR(hubConfiguration);
            });
            app.MapSignalR();





            var bootstrap = new DependencyContainer();
            bootstrap.Start();

            config = new HttpConfiguration
            {
                DependencyResolver = new ApiDependencyResolver(DependencyContainer.Container)
            };



            AreaRegistration.RegisterAllAreas();
            ConfigureOAuth(app);
            WebApiConfig.Register(ref config);
            app.UseCors(CorsOptions.AllowAll);

            app.UseWebApi(config);

            //RegisterWebApiFilters(config.Filters);


        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new SimpleAuthorizationServerProvider(),
                RefreshTokenProvider = new RefreshTokenProvider()
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            app.MapSignalR();








        }


        public static void RegisterWebApiFilters(System.Web.Http.Filters.HttpFilterCollection filters)
        {
            //DataBaseContext dbContext = new DataBaseContext();
            //UserRepository userRepository = new UserRepository(dbContext);
            //UserService service = new UserService(userRepository);
            //filters.Add(new CustomActionFilter(service));
            //filters.Add(new CustomActionFilter());
        }








    }
}
