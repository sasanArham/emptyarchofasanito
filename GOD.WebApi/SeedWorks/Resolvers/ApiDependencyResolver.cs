﻿using Castle.MicroKernel;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;

namespace asanito.WebApi.SeedWorks.Resolvers
{
    /// <summary>
    /// ساخت و تحویل وابستگی های سرویس ارائه شده
    /// </summary>
    public class ApiDependencyResolver : System.Web.Http.Dependencies.IDependencyResolver
    {
        public IWindsorContainer Container { get; private set; }
        public ApiDependencyResolver(IWindsorContainer container)
        {
            this.Container = container;
        }

        public void Dispose()
        {
        }

        /// <summary>
        /// ساخت یک آبجکت از سرویس ارائه شده با تمام وابستگی های مورد نیاز آن آبجکت
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            return this.Container.Kernel.HasComponent(serviceType) ? this.Container.Resolve(serviceType) : null;
        }


        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.Container.ResolveAll(serviceType).Cast<object>();
        }


        public IDependencyScope BeginScope()
        {
            return new DefaultDependencyScope(this.Container);
        }

    }
}