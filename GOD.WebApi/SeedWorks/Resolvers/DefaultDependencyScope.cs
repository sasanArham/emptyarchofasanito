﻿using Castle.MicroKernel.Lifestyle;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;

namespace asanito.WebApi.SeedWorks.Resolvers
{

    /// <summary>
    /// محدوده وابستگی برای محدود کردن انتقال یک وابستگی استفاده می شود و 
    /// همچنین بر روی مسیر کلاس که برای وظایف ساخت مختلف استفاده می شود را تحت تاثیر قرار می دهد.
    /// </summary>
    public class DefaultDependencyScope : IDependencyScope
    {
        public IDisposable Scope { get; private set; }
        public IWindsorContainer Container { get; private set; }

        public DefaultDependencyScope(IWindsorContainer container)
        {
            this.Container = container;
            this.Scope = container.BeginScope();
        }

        public object GetService(Type serviceType)
        {
           return this.Container.Kernel.HasComponent(serviceType) ? this.Container.Resolve(serviceType) : null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.Container.ResolveAll(serviceType).Cast<object>();
        }

        public void Dispose()
        {
            this.Scope.Dispose();
        }
    }
}