﻿using asanito.Infrastructure.Consts;
using asanito.Infrastructure.Enum.User;
using asanito.Infrastructure.Helpers;
using asanito.WebApi.SeedWorks.Installer;
using asanito.Infrastructure.Enum.User;
using asanito.Service.Module.Interface.User;
using asanito.Service.Module.Message.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace asanito.WebApi.SeedWorks
{
    public class RequiredPermission : ActionFilterAttribute
    {
        private IUserService userService;
        public string ActionNum { get; set; }

        public RequiredPermission(string actionNum)
        {
            ActionNum = actionNum;
            

        }



        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            this.userService = DependencyContainer.Container.Resolve<IUserService>();

            var context = actionContext.RequestContext;

            

            ClaimsIdentity claimsIdentity = context.Principal.Identity as ClaimsIdentity;
            string username = "";
            try
            {
                var zz = claimsIdentity.Claims.ToList();
                username = claimsIdentity.Claims.ToList()[0].Value.ToString();
            }
            catch (Exception e)
            {

                actionContext.Response = actionContext.Request.CreateResponse(
    HttpStatusCode.Unauthorized,
    new { message = ErrorMessage.AccessIsDenied + e.ToString() }
    ,
    actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                return;

            }

            Domain.Module.User._User.User user = userService.selectModel(c => c.Username == username);
            if (user == null)
            {
                actionContext.Response = actionContext.Request.CreateResponse(
        HttpStatusCode.Unauthorized,
        new { message = ErrorMessage.AccessIsDenied + "2" }
        ,
        actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                return;
            }


            // todo  jus return for adminUsers
            return;
           
        }

     




    }
}