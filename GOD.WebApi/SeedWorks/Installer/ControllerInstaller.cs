﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using asanito.Service.SeedWorks.Installer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace asanito.WebApi.SeedWorks.Installer
{
    public class ControllerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Install(new ServiceInstaller());
            container.Register(Classes.FromThisAssembly().BasedOn<ApiController>().LifestyleTransient());
            
        }
    }
}