﻿using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace asanito.WebApi.SeedWorks.Installer
{
    public class DependencyContainer
    {
        public static IWindsorContainer Container { get; private set; }

        public DependencyContainer()
        {
            Container = new WindsorContainer();
        }


        /// <summary>
        /// استارت رجیستر کردن وابستگی و نقطه آغاز
        /// </summary>
        public void Start()
        {
            Container.Install(new ControllerInstaller());
        }
        public static void Stop()
        {
            Container.Dispose();
        }

    }
}