﻿using asanito.Infrastructure;
using asanito.Infrastructure.Enum;
using asanito.Infrastructure.Helpers;

using asanito.WebApi.SeedWorks.Installer;
using asanito.WebApi.SeedWorks.OAuth.Model;
using asanito.WebApi.SeedWorks.OAuth.Repository;
using asanito.Service.Module.Interface.User;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace asanito.WebApi.SeedWorks.OAuth.Provider
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {

            #region sasan 
            //string clientId = string.Empty;
            //string clientSecret = string.Empty;
            //Client client = null;

            //context.Validated();
            #endregion

            #region ramin 

            string clientId = string.Empty;
            string clientSecret = string.Empty;
            Client client = null;

            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            if (context.ClientId == null)
            {
                //Remove the comments from the below line context.SetError, and invalidate context 
                //if you want to force sending clientId/secrects once obtain access tokens. 
                //context.Validated();
                context.SetError("invalid_clientId", "ClientId should be sent.");

                return Task.FromResult<object>(null);

            }

            using (AuthRepository _repo = new AuthRepository())
            {
                client = _repo.FindClient(context.ClientId);
            }

            if (client == null)
            {
                context.SetError("invalid_clientId", string.Format("Client '{0}' is not registered in the system.", context.ClientId));
                return Task.FromResult<object>(null);
            }

            if (client.ApplicationType == ApplicationTypes.NativeConfidential)
            {
                if (string.IsNullOrWhiteSpace(clientSecret))
                {
                    context.SetError("invalid_clientId", "Client secret should be sent.");
                    return Task.FromResult<object>(null);
                }
                else
                {
                    if (client.Secret != GetHashHelper.GetHash(clientSecret))
                    {
                        context.SetError("invalid_clientId", "Client secret is invalid.");
                        return Task.FromResult<object>(null);
                    }
                }
            }

            if (!client.Active)
            {
                context.SetError("invalid_clientId", "Client is inactive.");
                return Task.FromResult<object>(null);
            }

            context.OwinContext.Set<string>("as:clientAllowedOrigin", client.AllowedOrigin);
            context.OwinContext.Set<string>("as:clientRefreshTokenLifeTime", client.RefreshTokenLifeTime.ToString());
            context.OwinContext.Set<string>("as:client_id", context.ClientId);




        context.Validated();
           
            return Task.FromResult<object>(null);


            #endregion


        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            using (AuthRepository _repo = new AuthRepository())
            {
                IdentityUser identityUser = await _repo.FindUser(context.UserName, context.Password);

                if (identityUser == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }
            }



            var userService = DependencyContainer.Container.Resolve<IUserService>();
            //var user = userService.SELECT(tmp => tmp.Username == context.UserName || tmp.Mobile == context.UserName);
            var user = userService.selectModel(tmp => tmp.Username == context.UserName );

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("sub", context.UserName));
            identity.AddClaim(new Claim("role", "user"));
            identity.AddClaim(new Claim(ClaimTypes.Role, "users"));
            identity.AddClaim(new Claim("clientID", context.ClientId));
            identity.AddClaim(new Claim(Config.UserGroupClaimName, user.UserGroupID.ToString()));

            var form = await context.Request.ReadFormAsync();
            if (string.Equals(form["rememberMe"], "true", StringComparison.OrdinalIgnoreCase))
            {
                identity.AddClaim(new Claim("rememberMe", "true"));
            }
            else
            {
                identity.AddClaim(new Claim("rememberMe", "false"));
            }

            context.Validated(identity);
            //context.Validated();

        }



        //============================================================================================================================================


        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {



            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            var claims = context.Identity.Claims.Select(c => new
            {
                subject = c.Subject.Name,
                type = c.Type,
                value = c.Value
            }).ToList();
            string rememberMe = claims[4].value;




            if (rememberMe == "true")
            {
                //var time = context.Properties.ExpiresUtc;
                //context.Properties.ExpiresUtc = time.Value.AddDays(10);
                var time = DateTime.Now;
                context.Properties.ExpiresUtc = time.AddDays(10);

            }

            // zzzzzzz
            var _time = DateTime.Now;
            context.Properties.ExpiresUtc = _time.AddDays(30);
            // zzzzzzz

            return Task.FromResult<object>(null);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {

            // Change auth ticket for refresh token requests
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            newIdentity.AddClaim(new Claim("newClaim", "newValue"));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }


    }
}