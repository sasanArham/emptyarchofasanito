﻿using asanito.Infrastructure.Helpers;
using asanito.WebApi.SeedWorks.OAuth.Model;
using asanito.WebApi.SeedWorks.OAuth.Repository;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace asanito.WebApi.SeedWorks.OAuth.Provider
{
    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();

        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            //var clientid = context.Ticket.Properties.Dictionary["as:client_id"];

            var claims = context.Ticket.Identity.Claims.Select(c => new
            {
                subject = c.Subject.Name,
                type = c.Type,
                value = c.Value
            }).ToList();
            var clientid = claims[3].value;
            var userName = claims[0].value;

            // todo  : if client is not mobile should not create and return refresh token 


            if (string.IsNullOrEmpty(clientid))
            {
                return;
            }

            var refreshTokenId = Guid.NewGuid().ToString("n");

            using (AuthRepository _repo = new AuthRepository())
            {
                //var refreshTokenLifeTime = context.OwinContext.Get<string>("as:clientRefreshTokenLifeTime") ?? "60";
                var refreshTokenLifeTime = 30;

                var token = new RefreshToken()
                {
                    Id = GetHashHelper.GetHash(refreshTokenId),
                    ClientId = clientid,
                    //Subject = context.Ticket.Identity.Name,
                    Subject = userName,
                    IssuedUtc = DateTime.UtcNow,
                    //ExpiresUtc = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
                    ExpiresUtc = DateTime.Now.AddDays(refreshTokenLifeTime) 
                };
                
                context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
                context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;


                // prevent single signIN : 
                token.Subject += token.Id.Substring(0, 10);


                token.ProtectedTicket = context.SerializeTicket();

                var result = await _repo.AddRefreshToken(token);

                if (result)
                {
                    context.SetToken(refreshTokenId);
                }

            }
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            string hashedTokenId = GetHashHelper.GetHash(context.Token);

            using (AuthRepository _repo = new AuthRepository())
            {

                // sasan  : refresh token
                var refreshToken = await _repo.FindRefreshToken(hashedTokenId);
                //var refreshToken = await _repo.FindRefreshToken(context.Token);

                if (refreshToken != null)
                {
                    //Get protectedTicket from refreshToken class
                    context.DeserializeTicket(refreshToken.ProtectedTicket);
                    var result = await _repo.RemoveRefreshToken(refreshToken);
                }
            }
        }
    }
}