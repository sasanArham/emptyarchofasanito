namespace asanito.WebApi.Migrations
{
    using asanito.Infrastructure.Enum;
    using asanito.Infrastructure.Helpers;
    using asanito.WebApi.SeedWorks.OAuth.Model;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<asanito.WebApi.SeedWorks.OAuth.Context.AuthContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(asanito.WebApi.SeedWorks.OAuth.Context.AuthContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            if (context.Clients.Any())
            {
                return;
            }

            context.Clients.AddRange(BuildClientsList());
            context.SaveChanges();
        }


        private static List<Client> BuildClientsList()
        {

            List<Client> ClientsList = new List<Client>
                                           {
                                               new Client
                                                   {
                                                       Id = "panel",
                                                       Secret = GetHashHelper.GetHash("p@ne1"),
                                                       Name = "AngularJS front-end Application",
                                                       ApplicationType =
                                                           ApplicationTypes.JavaScript,
                                                       Active = true,
                                                       RefreshTokenLifeTime = 72000,
                                                       //AllowedOrigin = "http://panel.easyja.ir"
                                                       AllowedOrigin = "*"
                                                   },
                                                 new Client
                                                   {
                                                       Id = "ngAuthAppLocal",
                                                       Secret = GetHashHelper.GetHash("l0c@1"),
                                                       Name = "AngularJS front-end Local Application",
                                                       ApplicationType =
                                                           ApplicationTypes.JavaScript,
                                                       Active = true,
                                                       RefreshTokenLifeTime = 72000,
                                                       //AllowedOrigin = "http://localhost:22557/"
                                                       AllowedOrigin = "*"
                                                   },
                                               new Client
                                                   {
                                                       Id = "mobileApp",
                                                       Secret = GetHashHelper.GetHash("m0bi1e"),
                                                       Name = "Console Application",
                                                       ApplicationType =
                                                           ApplicationTypes.NativeConfidential,
                                                       Active = true,
                                                       RefreshTokenLifeTime = 144000,
                                                       AllowedOrigin = "*"
                                                   }
                                           };

            return ClientsList;
        }






    }
}
