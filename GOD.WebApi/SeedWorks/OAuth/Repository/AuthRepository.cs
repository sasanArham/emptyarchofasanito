﻿using asanito.WebApi.SeedWorks.OAuth.Context;
using asanito.WebApi.SeedWorks.OAuth.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace asanito.WebApi.SeedWorks.OAuth.Repository
{
    public class AuthRepository : IDisposable
    {
        private AuthContext _ctx;

        private UserManager<IdentityUser> _userManager;

        public AuthRepository()
        {
            _ctx = new AuthContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_ctx));
        }

        public async Task<IdentityResult> RegisterUser()
        {
            //Models.User userModel
            IdentityUser user = new IdentityUser
            {
                UserName = "admin"
            };

            var result = await _userManager.CreateAsync(user, "t8Q-.WD#h1K29)72DC*@>`}H__^^xpO");

            return result;
        }

        public async Task<IdentityResult> RegisterUser(string mobile)
        {
            //Models.User userModel
            IdentityUser user = new IdentityUser
            {
                UserName = mobile
            };

            var result = await _userManager.CreateAsync(user, mobile);

            return result;
        }

        

        public async Task<IdentityResult> RegisterUser(string username , string password)
        {
            //Models.User userModel
            IdentityUser user = new IdentityUser
            {
                UserName = username
            };

            var result = await _userManager.CreateAsync(user, password);

            return result;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public IdentityUser FindUser(string userName)
        {
            IdentityUser user =  _userManager.FindByName(userName);

            return user;
        }

        public Client FindClient(string clientId)
        {
            var client = _ctx.Clients.Find(clientId);

            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {

            var existingToken = _ctx.RefreshTokens.Where(r => r.Subject == token.Subject).FirstOrDefault();

            if (existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken);
            }

            _ctx.RefreshTokens.Add(token);

            return await _ctx.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            _ctx.RefreshTokens.Remove(refreshToken);
            return await _ctx.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _ctx.RefreshTokens.FindAsync(refreshTokenId);

            if (refreshToken != null)
            {
                _ctx.RefreshTokens.Remove(refreshToken);
                return await _ctx.SaveChangesAsync() > 0;
            }

            return false;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _ctx.RefreshTokens.FindAsync(refreshTokenId);

            return refreshToken;
        }

        //=============================================================================================================

        public async Task<bool> updateUserPassword(string username, string oldPassword, string password)
        {

            IdentityUser _user = await _userManager.FindByNameAsync(username);

            IdentityResult result = await _userManager.ChangePasswordAsync(_user.Id, oldPassword, password);
            
            if (result.Errors.ToList().Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public async Task<bool> resetPassword(string username ,string newPassword)
        {
            IdentityUser user = _userManager.FindByName(username);
            if (user != null)
            {
                string id = user.Id;
                IdentityResult result = await _userManager.RemovePasswordAsync(id);
                _userManager.RemovePassword(id);
                //result =  _userManager.ChangePassword(id, null, newPassword);
                result = _userManager.AddPassword(id, newPassword);
                return true;
            }

            return false;

            
            



        }

        public async Task<int> updateUser(string oldUsername,string username ,string oldPassword ,  string password)
        {
            IdentityUser _user = await _userManager.FindByNameAsync(oldUsername);
            _user.UserName = username;
            IdentityResult result = await _userManager.UpdateAsync(_user);


            if (result.Errors.ToList().Count == 0)
            {
                result = await _userManager.ChangePasswordAsync(_user.Id, oldPassword, password);
                if (result.Errors.ToList().Count == 0)
                {
                    return 1;
                }
                else
                {
                    _user.UserName = oldUsername;
                     result = await _userManager.UpdateAsync(_user);
                    return -2;
                }
                
            }
            else
            {
                return -1;
            }
        }

        public async Task<bool> updateUser(string oldUsername, string username)
        {
            IdentityUser _user = await _userManager.FindByNameAsync(oldUsername);
            _user.UserName = username;
            IdentityResult result = await _userManager.UpdateAsync(_user);


            if (result.Errors.ToList().Count == 0)
            {
                return true;

            }
            else
            {
                return false;
            }
        }



        public async Task<bool> deleteUserAsync(string username)
        {
            IdentityUser _user = await _userManager.FindByNameAsync(username);
            //var user = await _userManager.FindByIdAsync(_user.Id); 
            var result = await _userManager.DeleteAsync(_user);
            if (result.Succeeded)
            {
                return true;
            }
            return false;
        }


        public void Dispose()
        {
            _ctx.Dispose();
            _userManager.Dispose();

        }
    }
}