﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure
{
    public static class Config
    {
        public static readonly bool PermissionManageEnabled = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["PermissionManageEnabled"]);
        
        public static readonly string UserGroupClaimName = "userGroup";
    }
}
