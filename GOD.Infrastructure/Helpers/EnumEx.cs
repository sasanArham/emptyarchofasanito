﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Helpers
{
    public static class EnumEx
    {
        public static IComparable GetValueFromDescription(string description, Type t)
        {
            foreach (var field in t.GetFields())
            {
                if (Attribute.GetCustomAttribute(field,
                typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                {
                    if (attribute.Description.Contains(description))
                        return (IComparable)field.GetValue(null);
                }
                else
                {
                    if (field.Name.Contains(description))
                        return (IComparable)field.GetValue(null);
                }
            }
            return null;
            //or throw new ArgumentException("Not found.", nameof(description));
            // Or return default(T);
        }




    }
}
