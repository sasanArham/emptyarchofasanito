﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Helpers
{
    public static class DateTimeConverter
    {
        public static DateTime ToDate(this string sDate)
        {
            try
            {
                if (sDate == null)
                {
                    return DateTime.MinValue;
                }

                string time = sDate.Substring(10);
                sDate = sDate.Substring(0, 10); // remove hour and minute
                sDate = sDate.Replace("_", "/");

                string[] splited = sDate.Split('/');
                int year = int.Parse(splited[2]);
                int month = int.Parse(splited[0]);
                int day = int.Parse(splited[1]);

                string[] splitedTime = time.Split(':');
                int hour = Int32.Parse(splitedTime[0]);
                int minute = Int32.Parse(splitedTime[1]);

                PersianCalendar pc = new PersianCalendar();
                DateTime date = pc.ToDateTime(year, month, day, hour, minute, 0, 0);


                return date;
            }
            catch (Exception e)
            {
                return DateTime.MinValue;
            }
        }

        public static DateTimeOffset ToDateOffset(this string sDate)
        {
            try
            {
                if (sDate == null)
                {
                    return DateTimeOffset.MinValue;
                }
                string time = sDate.Substring(10);
                sDate = sDate.Substring(0, 10); // remove hour and minute
                sDate = sDate.Replace("_", "/");

                string[] splited = sDate.Split('/');
                int year = int.Parse(splited[2]);
                int month = int.Parse(splited[0]);
                int day = int.Parse(splited[1]);

                string[] splitedTime = time.Split(':');
                int hour = Int32.Parse(splitedTime[0]);
                int minute = Int32.Parse(splitedTime[1]);

                PersianCalendar pc = new PersianCalendar();
                DateTimeOffset date = pc.ToDateTime(year, month, day, hour, minute, 0, 0);
                return date;
            }
            catch (Exception e)
            {
                return DateTimeOffset.MinValue;
            }
        }

    }
}
