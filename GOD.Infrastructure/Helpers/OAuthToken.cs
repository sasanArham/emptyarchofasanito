﻿using asanito.Infrastructure.Consts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Helpers
{
    public static class OAuthToken
    {
        public static async Task<string> getToken(string username, string password, string clientID)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                    var _params = new List<KeyValuePair<string, string>>();
                    _params.Add(new KeyValuePair<string, string>("username", username));
                    _params.Add(new KeyValuePair<string, string>("password", password));
                    _params.Add(new KeyValuePair<string, string>("grant_type", "password"));
                    _params.Add(new KeyValuePair<string, string>("client_id", clientID));


                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, G.serverAddress + "/token") { Content = new FormUrlEncodedContent(_params) };

                    HttpResponseMessage resp = await client.SendAsync(request);
                    string accessstoKen = await resp.Content.ReadAsStringAsync();
                    if (accessstoKen.Contains("invalid_grant"))
                    {
                        return "";
                    }
                    if (accessstoKen.ToLower().Contains("error"))
                    {
                        return "";
                    }

                    string[] result = accessstoKen.Split(',');
                    accessstoKen = result[0].Replace("{\"access_token\":\"", "");
                    accessstoKen = accessstoKen.Remove(accessstoKen.Length - 1);



                    return accessstoKen;

                }


            }
            catch (Exception ex)
            {
                return "err : " + ex.ToString();
            }
        }


        public static async Task<string[]> getTokenWithRefreshToken(string username, string password, string clientID, string secretKey)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                    var _params = new List<KeyValuePair<string, string>>();
                    _params.Add(new KeyValuePair<string, string>("username", username));
                    _params.Add(new KeyValuePair<string, string>("password", password));
                    _params.Add(new KeyValuePair<string, string>("grant_type", "password"));
                    _params.Add(new KeyValuePair<string, string>("client_id", clientID));
                    if (clientID == "mobileApp")
                    {
                        _params.Add(new KeyValuePair<string, string>("client_secret", secretKey));
                    }


                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, G.serverAddress + "/token") { Content = new FormUrlEncodedContent(_params) };

                    HttpResponseMessage resp = await client.SendAsync(request);
                    string stringResponse = await resp.Content.ReadAsStringAsync();
                    if (stringResponse.Contains("invalid_grant"))
                    {
                        return null;
                    }
                    if (stringResponse.ToLower().Contains("error"))
                    {
                        return null;
                    }

                    string[] responseArray = stringResponse.Split(',');
                    string accessToken = responseArray[0].Replace("{\"access_token\":\"", "");
                    accessToken = accessToken.Remove(accessToken.Length - 1);

                    string refreshToken = responseArray[3].Replace("\"refresh_token\":\"", "");
                    refreshToken = refreshToken.Remove(refreshToken.Length - 1);

                    string[] result = new string[2];

                    result[0] = accessToken;
                    result[1] = refreshToken;

                    return result;

                }


            }
            catch (Exception ex)
            {
                //return "err : " + ex.ToString();
                return null;
            }
        }
    }
}
