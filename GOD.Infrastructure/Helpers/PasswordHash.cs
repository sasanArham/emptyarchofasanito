﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Helpers
{
    public static class PasswordHash
    {
        public static string hashPassword(string password)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes(password);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            String hash = System.Text.Encoding.ASCII.GetString(data);
            return hash;
        }

        public static string Hash(string input, int lentgh)
        {
            var allowedSymbols = "4efgh5167ijkml283opyzqwxruvst09abcde".ToCharArray();
            var hash = new char[lentgh];

            for (int i = 0; i < input.Length; i++)
            {
                hash[i % lentgh] = (char)(hash[i % lentgh] ^ input[i]);
            }

            for (int i = 0; i < lentgh; i++)
            {
                hash[i] = allowedSymbols[hash[i] % allowedSymbols.Length];
            }

            return new string(hash);
        }
    }
}
