﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Helpers
{
    public class ResponseCreator
    {
        public static HttpResponseMessage create(HttpStatusCode httpStatusCode , string message , string reasonPhrase)
        {
            var resp = new HttpResponseMessage(httpStatusCode)
            {
                Content = new StringContent("{\"Message\": \"  " + message + "   \"}", Encoding.UTF8, "application/json"),
                ReasonPhrase = reasonPhrase,
            };
            return resp;
        }
    }
}
