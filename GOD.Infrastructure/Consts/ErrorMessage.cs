﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Consts
{
    public static class ErrorMessage
    {
        public static Success OK = new Success();
        public static readonly string insertError = " خطا در ذخیره سازی اطلاعات";
        public static readonly string notUniqueData = " اطلاعات وارد شده تکراری هستند ";
        public static readonly string deleteCascadeError = " امکان حذف به دلیل استفاده از اطلاعات وجود ندارد ";
        public static readonly string notValidModelState = " داده ی ارسالی فاقد اعتبار است";
        public static readonly string loadError = "خطا در بارگزاری اطلاعات";
        public static readonly string deleteError = " خطا در حذف اطلاعات ";
        public static readonly string AccessIsDenied = " شما به این بخش دسترسی ندارید ";
        public static readonly string uploadError = " خطا در آپلود فایل ";
        public static readonly string noFileChoosen = "هیچ فایلی انتخاب نکرده اید";
        public static readonly string serverError = "خطا در برقراری ارتباط با سرور";
        public static readonly string identificationError = "خطا در احراز هویت";
        public static readonly string fileNotFound = "فایل مورد نظر یافت نشد";
        public static readonly string requaredParams = "پارامترهای خواسته شده را ارسال کنید";
        public static readonly string wrongParams = "مقادیر ارسالی معتبر نمیباشند";
        public static readonly string pawwsordCharaterLimit = "رمز عبور حد اقل باید 6 رقمی باشد";
        public static readonly string DateFormatError = " زمان را به فرمت MM_dd_yyyy  وارد کنید ";
        public static readonly string notValidDate = "تاریخ وارد شده معتبر نیست";
        public static readonly string NothingToConfirm = "تغییری برای تایید وجود ندارد";
        public static readonly string priceCanNotBeNegative = "قیمت نمیتواند منفی باشد";
        public static readonly string noContactChoosen = "هیچ مخاطبی انتخاب نکرده اید";
        public static readonly string smsNotSent = "خطا در ارسال پیام کوتاه";


        public class Success
        {
            public readonly string message = "OK";
        }

        public static class User
        {
            public static readonly string loginError = " نام کاربری یا رمز عبور اشتباه است ";
            public static readonly string signUpError = "خطا در ثبت نام";
            public static readonly string notEnoughWallet = "موجودی کیف پول کافی نیست";
            public static readonly string notValidID = "کاربر مورد نظر یافت نشد";
            public static readonly string alreadyExsistUsername = "این نام کاربری قبلا استفاده شده ";
            public static readonly string alreadyExsistMobile = "شماره موبایل وارد شده تکراری است";
            public static readonly string banedError = "اکانت شما مسدود گردیده";
            public static readonly string alreaduSignedUp = "قبلا ثبت نام کرده اید";
            public static readonly string userNotFound = "کاربری با مشخصات وارد شده یافت نشد";
            public static readonly string enterCurentPassword = "رمز عبور فعلی را وارد کنید";
            public static readonly string wrongCurrentPassword = "رمز عبور وارد شده صحیح نیست";

            public static readonly string tempUser = "ثبت نام شما ناقص است، از قسمت ثبت نام مشخصات خود را تکمیل کنید";
            public static readonly string packageviewStatusISForSiteUsers = "این قسمت مخصوص کاربران سایت میباشد";
            public static readonly string userDoesNotHaveEmail = " آدرس ایمیل و رمز عبور خود را (به عنوان فرستنده ایمیل ) در پنل کاربری ثبت کنید ";
            public static readonly string salarySettingExistForOrganization = "برای این سازمان قبلا حقوق ثبت کرده اید ، در صورت نیاز میتوانید آن را ویرایش کنید ";

        }

        public static class UserGroup
        {
            public static readonly string anotherUserGroupNotExsist = "گروه کاربری مقصد که برای انتقال انتخاب کرده اید وجود ندارد";
            public static readonly string usersTransferError = "خطا در انتقال کاربران";
            public static readonly string cannotDeleteBaseUserGroups = "گروه های کاربری پایه قابل حذف نیستند";
            public static readonly string siteUserGroupMembersCannotBeTransfered = "کاربران سایت را نمیتوانید منتقل کنید";
            public static readonly string cannotTransferUsersToSiteMembersGroup = "انتقال کاربر به گروه کاربران سایت مجاز نیست";
            public static readonly string noUserExixstInGroup = "این گروه کاربری هیچ کاربری ندارد";
            public static readonly string cannotAddMemeberToGroup = "مجاز به افزودن کاربر به این گروه کاربری نیستید";
            public static readonly string notvalidID = "گروه مورد نظر یافت نشد";

        }

        public static class Activation
        {
            public static readonly string activationError = "کد ارسالی معتببر نمیباشد";
            public static readonly string smsNotSent = "خطا در ارسال کد";
        }

        public static class Payment
        {
            public static readonly string PriceAmountError = "مبلغ تراکنش صحیح نیست";
            public static readonly string zarrinPalTransactionError = "تراکنش نا معتبر";
            public static readonly string notOkPayment = "تراکنش لغو گردید";
            public static readonly string walletLogInsertError = "خطا در شارژ کیف پول";
            public static readonly string walletLogUpdatError = "خطا در بروز رسانی کیف پول مربوطه";
            public static readonly string alreadySavedPayment = "این تراکنش قبلا ثبت شده";
            public static readonly string ShoppingBagIsEmpty = "سبد خرید خالی است ";
            public static readonly string ItemIsFree = "آیتم انتخاب شده رایگان میباشد";
            public static readonly string categoryError = "نوع درخواست معتبر نیست";
            public static readonly string zarrinpalServerError = "خطا در برقراری ارتباط با درگاه پرداخت";
            public static readonly string paymentNotFound = "تراکنش مورد نظر یافت نشد";

            public static readonly string packageOk = "اشتراک شما با موفقی فعال شد";
            public static readonly string applyFormSubmitOk = "فرم شما با موفقیت ثبت شد";
        }

        public static class ApiAction
        {
            public static readonly string notValidNewHashiD = "آی دی جدید فرستاده شده معتبر نیست";
            public static readonly string notValidID = "ای پی آی انتخاب شده یافت نشد";

        }


        public static class Page
        {
            public static readonly string duplicateLink = "لینک وارد شده تکراری است";
            public static readonly string duplicateInex = "ایندکس تکراری است";
            public static readonly string notvalidID = "صفحه انتخاب شده  یافت نشد";
            public static readonly string duplicateTitle = "عنوان انتخاب شده تکراری است";

        }


        public static class City
        {
            public static readonly string notvalidID = "شهر مورد نظر یافت نشد";
        }

        public static class Province
        {
            public static readonly string notvalidID = "استان مورد نظر یافت نشد";
        }

        public static class Station
        {
            public static readonly string notValidID = "مرکز مورد نظر یافت نشد";
        }

        public static class Company
        {
            public static readonly string notValidID = "شرکت مورد نظر یافت نشد";
        }

        public static class Person
        {
            public static readonly string notValidID = "شخص مورد نظر یافت نشد";
            public static readonly string doesNotHavEmail = "برای مخاطب مورد نظر ایمیل ثبت نشده";
        }

        public static class Warehouse
        {
            public static readonly string notValidID = "انبار مورد نظر یافت نشد";
            public static readonly string notValidDestinationID = "انبار مقصد یافت نشد";
            public static readonly string hostWarehouseRequired = "انبار مقصد را برای آیتم های لازم انتخاب کنید ";
            public static readonly string noAllowedWarehouse = "شما به هیچ انباری دسترسی ندارید";
            public static readonly string accessDeniead = "به انبار مورد نظر دسترسی ندارید";
        }

        public static class Product
        {
            public static readonly string notValidID = "محصول مورد نظر یافت نشد";
            public static readonly string noExistInWarehouse = "محصول به انداره مورد نظر شما در انبار موجود نمیباشد";
            public static readonly string duplicateProductCode = "کد محصول وارد شده تکراری میباشد";
            public static readonly string HasSerialPerStockProductCannotHavDoubleInventory = "محصول سریال دار نمیتواند موجودی اعشاری داشته باشد";
            public static readonly string cannotReduceInitialIncentory = "در حال حاضر امکان کم کردن موجودی اولیه امکان پذیر نمیباشد";
        }
        public class ProductCategory
        {
            public static readonly string notValidID = "دسته بندی مورد نظر یافت نشد";
        }

        public class Address
        {
            public static readonly string notValidID = " آدرس مورد نظر یافت نشد";
        }

        public class Funnel
        {
            public static readonly string notValidID = " قیف مورد نظر یافت نشد";
            public static readonly string duplicateCorrepondingStatus = "وضعیت متناظر انتخاب شده قبلا به مرحله ی دیگری اختصاص یافته ";
            public static readonly string duplicateIndex = "اولویت .ارد شده تکراری است";
        }

        public class Negotiation
        {
            public static readonly string notValidID = " مذاکره مورد نظر یافت نشد";
            public static readonly string alreadyHasAnInvoice = "برای این مذاکره قبلا فاکتور ثبت شده";
            //public static readonly string cantEditBecauseOfRelatedInvoiceStstus = "با توجه به وضعیت فاکتور متناظر این مذاکره قابل ویرایش نمیباشد";
            public static readonly string cantEditBecauseOfFunnelLevel = "مذاکره در این مرحله نمیتواند ویرایش شود";
            public static string userNotAllowedToLevel(string level)
            {
                return " شما مجاز به ویرایش مذاکره ای که در مرحله ی  " + level + " است نیستید ";
            }
        }

        public class Organization
        {
            public static readonly string notValidID = " سازمان مورد نظر یافت نشد";
            public static readonly string alreadyHasTemplate = "قبلا تنظیمات قرارداد ثبت شده";
            public static readonly string alreadyHasInvoiceSetting = "قبلا تنظیمات فاکتور ثبت شده";
            public static readonly string contractSettingNotValidID = "برای سازمان مورد نظر تنظیمات قرارداد اعمال نشده";
            public static readonly string invoiceSettingNotValidID = "برای سازمان مورد نظر تنظیمات فاکتور اعمال نشده";
            public static readonly string alreadyHasLetterTemplate = "قبلا تنظیمات نامه  ثبت شده";
            public static readonly string LetterSettingNotValidID = "برای سازمان مورد نظر تنظیمات نامه اعمال نشده";
        }

        public static class WorkDay
        {
            public static readonly string conflictError = "زمان های تعین شده تداخل دارند";
        }


        public static class FormComponent
        {
            public static readonly string notValidID = "کامپوننت مورد نظر یافت نشد";
        }

        public static class PaymentTerm
        {
            public static readonly string typeError = "همه ی ردیف های شرایط پرداخت باید از یک نوع باشند";
            public static readonly string percentSumError = "مجموع درصد های پرداختی باید 100 باشد";
            public static readonly string amountSumError = "مجموع مبالغ پرداختی باید برابر مبلغ کل قرارداد باشد";
        }


        public static class Contract
        {
            public static readonly string notvalidID = "قرارداد مورد نظر یافت نشد";
        }


        public static class Invoice
        {
            public static readonly string notValidID = "فاکتور مورد نظر یافت نشد ";
            public static readonly string editNotAllowed = "فاکتور در این حالت قابل ویزایش نمیباشد";
            public static readonly string invoiceUpdateError = "خطا در بروزرسانی فاکتور مربوطه";
            public static readonly string cannotUpdateToThisStatus = "امکان ثبت دستی به این حالت مجاز نمیباشد";
            public static readonly string cannotGroupPay = "امکان پرداخت گروهی فاکتورها به دلیل متفاوت بودن نوع آنها وجود ندارد";
            public static readonly string wrongPhoneNumberForSendSms = "شماره الفن انتخاب شده متعلق به مخاطب این فاکتور نمیباشد";
            public static readonly string canceledInvoice = "فاکتور انتخابی لغو شده است ";
            public static readonly string noMobileForInvoiceContact = "برای مخاطب این فاکتور هیچ شماره موبایلی ثبت نشده";
            public static readonly string sellDateRequired = "تاریخ فروش را مشخص کنید";
        }


        public static class Financial
        {
            public static readonly string WalletChargeAllowedOnly = "برای ثبت درآمد بیش از یک مورد فقط گزینه ی شارژ کیف پول امکان پذیر میباشد";
            public static readonly string WalletCheckoutAllowedOnly = "برای ثبت هزینه بیش از یک مورد فقط گزینه ی کسر از کیف پول امکان پذیر میباشد";
            public static readonly string noPaumentSent = "هیچ پرداختی ثبت نکرده اید";
            public static readonly string noContactChoosen = "هیچ مخاطبی برای درآمد انتخاب نکرده اید";
            public static readonly string noInvoiceChoosen = "هیچ فاکتوری برای تسویه حساب انتخاب نکرده اید";
            public static readonly string notValidCheckID = "چک مورد نظر یافت نشد";
        }



        public static class BankAccount
        {
            public static readonly string notValidID = "حساب  مورد نظر یافت نشد ";
            public static readonly string destinationAccountBalanceUpdateError = "خطا در بروز رسانی حساب بانکی مقصد";
            public static readonly string sourseAccountBalanceUpdateError = "خطا در بروز رسانی حساب بانکی مبدا";
            public static readonly string AccountBalanceUpdateError = "خطا در بروز رسانی حساب بانکی ";
        }


        public static class Email
        {
            public static readonly string notSent = "خطا در ارسال";
            public static readonly string sentButNotSaved = "ایمیل ارسال شد اما در سامانه خیره نشد ";
        }

        public static class ShareHolderTransaction
        {
            public static readonly string onlyFalseTypeAllowedForTrueSoucr = "برای سود سهام پرداختی تنها امکان برداشت وجود دارد";
        }

        public static class Loan
        {
            public static readonly string notValidID = "تسهیلات مورد نظر یافت نشد";
            public static readonly string noLoanSelected = "تسهیلات مورد نظر را مشخص کنید";
            public static readonly string loanPaymentError = "خطا در بروز رسانی تسهیلات";
        }


        public static class ExcelImportedDate
        {
            public static string insertErrorInRow(int rowIndex)
            {
                return " خطا در ذخیره سازی ردیف  " + rowIndex.ToString();
            }
            public static string foreignKeyError(string fieldName)
            {
                return " خطا در بارگذاری کلید های خارجی ستون " + fieldName;
            }
            public static string foreignKeyError(string fieldName, int rowIndex)
            {
                return " خطا در بارگذاری کلید  خارجی ستون " + fieldName + " ردیف " + rowIndex.ToString();
            }


        }

        public static class Income
        {
            public static readonly string spentCheckDeleteError = "خطا در حذف چک خرج شده";

        }

        public static class CheckPayment
        {
            public static readonly string expandedCheckCannotBeRecived = "این چک خرج شده است . نمیتوانید آن را وصول کنید";
            public static readonly string notValidID = "چک مورد نظر یافت نشد";
            public static readonly string notExpendableCheck = "فقط چک های دریافت شده و مهلت داده شده قابل خرج هستند";
            public static readonly string alreadyExpended = "این چک قبلا خرج شده است";
            public static readonly string cannotChangeStatusOfSpantCheck = "چک خرج شده را نمیتوانید تغییر وضعیت دهید";
            public static readonly string NocreditCheckCanBeChashedOrReturned = "چک برگشت خورده فقط میتنواند عودت داده داده شود یا وصول شود";
            public static readonly string notSpentCheck = "چک مورد نظر خرج نشده است";
            public static readonly string notNoCreditCheck = "چک مورد نظر برگشت نخورده است";
        }



        public static class Project
        {
            public static readonly string projectNotFound = "پروژه مورد نظر یافت نشد";
        }
        public static class PaymentReceipt
        {
            public static readonly string notValidID = " رسید پرداختی مورد نظر یافت نشد";
            public static readonly string noCustomerChoosen = "مشتری انتخاب نکرده اید";
        }
        public static class AcquaintionTyp
        {
            public static readonly string notValidID = "اطلاعات مورد نظر یافت نشد";
        }
        public static class UserSalary
        {
            public static readonly string notExists = "تنظیمات حقوق کاربر ثبت نشده است";
        }

        public static class SocialMediaType
        {
            public static readonly string notValidID = "اطلاعات مورد نظر یافت نشد";
        }
        public static class ReminderTask
        {
            public static readonly string notValidID = "یادآور مورد نظر یافت نشد";
            public static readonly string editNotAllowed = "امکان ویرایش  یادآوری که انجام شده وجود ندارد";
            public static readonly string deleteNoAllowed = "امکان حذف یادآوری که انجام شده وجود ندارد";
        }

        public static class PhoneNumber
        {
            public static readonly string notValidID = "شماره تماس مورد نظر یافت نشد";
            public static readonly string notMobile = "شماره ی انتخاب شده موبایل نیست ";
        }

        public static class ProcessType
        {
            public static readonly string notValidID = "ساختار مورد نظر یافت نشد";
        }

        public static class BusinessProcess
        {
            public static readonly string notValidID = "فرابند مورد نظر یافت نشد";

        }

        public static class PriceInquiry
        {
            
        }
    }
}

