﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Consts
{
    public static class G
    {
        
        public static readonly string serverAddress = System.Configuration.ConfigurationManager.AppSettings["serverAddress"];
        


        public static readonly string smsPannel_userApiKey = System.Configuration.ConfigurationManager.AppSettings["smsPannel_userApiKey"];
        public static readonly string sms_secretKey = System.Configuration.ConfigurationManager.AppSettings["sms_secretKey"];
        
        public static readonly string lineNumber = System.Configuration.ConfigurationManager.AppSettings["LineNumber"];
        public static readonly string smsTemplate = System.Configuration.ConfigurationManager.AppSettings["smsTemplate"];
        public static readonly string newPassword_smsTemplate = System.Configuration.ConfigurationManager.AppSettings["newPassword_smsTemplate"];
        public static readonly string reminder_smsTemplate = System.Configuration.ConfigurationManager.AppSettings["reminder_smsTemplate"];
        public static readonly string reminderSmsmTemplate = System.Configuration.ConfigurationManager.AppSettings["reminderSmsmTemplate"];
        public static readonly string invoiceLinkSmsTemplate = System.Configuration.ConfigurationManager.AppSettings["invoiceLinkSmsTemplate"];


        public static readonly string invoiceLinkSmsbaseUrl = System.Configuration.ConfigurationManager.AppSettings["invoiceLinkSmsbaseUrl"];
        
        
        
        


        public static List<string> signalR_ConnectionIDs = new List<string>();

        public static Dictionary<string, string> signalR_Connections = new Dictionary<string, string>();

        public static readonly int paymentReceiptDocNumber = 20000;
        public static readonly int paymentReceiptDocFixNumber = 100001;

        public static DateTime stringToDate(string sDate)
        {
            try
            {
                if (sDate == null)
                {
                    return DateTime.MinValue;
                }

                string time = sDate.Substring(10);
                sDate = sDate.Substring(0, 10); // remove hour and minute
                sDate = sDate.Replace("_", "/");

                string[] splited = sDate.Split('/');
                int year = int.Parse(splited[2]);
                int month = int.Parse(splited[0]);
                int day = int.Parse(splited[1]);

                string[] splitedTime = time.Split(':');
                int hour = Int32.Parse(splitedTime[0]);
                int minute = Int32.Parse(splitedTime[1]);

                PersianCalendar pc = new PersianCalendar();
                DateTime date = pc.ToDateTime(year, month, day, hour, minute, 0, 0);


                return date;
            }
            catch (Exception e)
            {
                return DateTime.MinValue;
            }
        }

        public static DateTimeOffset stringToDateOffset(string sDate)
        {
            try
            {
                if (sDate == null)
                {
                    return DateTimeOffset.MinValue;
                }
                string time = sDate.Substring(10);
                sDate = sDate.Substring(0, 10); // remove hour and minute
                sDate = sDate.Replace("_", "/");

                string[] splited = sDate.Split('/');
                int year = int.Parse(splited[2]);
                int month = int.Parse(splited[0]);
                int day = int.Parse(splited[1]);

                string[] splitedTime = time.Split(':');
                int hour = Int32.Parse(splitedTime[0]);
                int minute = Int32.Parse(splitedTime[1]);

                PersianCalendar pc = new PersianCalendar();
                DateTimeOffset date = pc.ToDateTime(year, month, day, hour, minute, 0, 0);
                return date;
            }
            catch (Exception e)
            {
                return DateTimeOffset.MinValue;
            }
        }

        public static string incompleteShamsiToStringDate(string sDate)
        {
            try
            {
                PersianCalendar pc = new PersianCalendar();
                if (sDate == null)
                {
                    return "";
                }

                if (sDate.Length >= 10)
                {
                    var completeDate =  stringToDate(sDate);
                    return DateToString(completeDate);
                }
                else if(sDate.Length <= 5 && sDate.Length >= 4)
                {
                    sDate = sDate.Replace("/", "");
                    int year = Int32.Parse(sDate.Substring(0,4));
                    DateTime date = pc.ToDateTime(year, 1, 1, 1, 1, 0, 0);
                    return date.Year.ToString();
                }
                else if (sDate.Length <= 8)
                {
                    sDate = sDate.Replace("/", "");
                    int year = Int32.Parse(sDate.Substring(0, 4));
                    int month = Int32.Parse(sDate.Substring(4));
                    DateTime date = pc.ToDateTime(year, month, 1, 1, 1, 0, 0);
                    //return date.Year.ToString() + "/" + date.Month.ToString();
                    return date.Month.ToString() + "-" + date.Year.ToString();
                }
                else
                {
                    sDate = sDate.Replace("/", "");
                    int year = Int32.Parse(sDate);
                    int month = Int32.Parse(sDate.Substring(4));
                    int day = Int32.Parse(sDate.Substring(7));
                    DateTime date = pc.ToDateTime(year, month, day, 1, 1, 0, 0);
                    return date.Year.ToString() + "/" + date.Month.ToString() + "/" + date.Day.ToString();
                }

            }
            catch (Exception e)
            {
                return "xxx";
            }
        }


        public static int[] stringToIntArray(string arrayString)
        {
            try
            {
                string[] sArr = arrayString.Split(',');

                int[] iArr = new int[sArr.Length];
                for (int i = 0; i < iArr.Length; i++)
                {
                    iArr[i] = Int32.Parse(sArr[i]);
                }

                return iArr;
            }
            catch (Exception)
            {
                return null;
            }


        }


        public static float MeanOfList(List<int> items)
        {
            float mean = 0;
            float sum = 0;

            for (int i = 0; i < items.Count; i++)
            {
                sum += items[i];
            }
            if (items.Count > 0)
                mean = sum / items.Count;

            return mean;
        }
        public static double MeanOfList(double[] items)
        {
            double mean = 0;
            double sum = 0;

            for (int i = 0; i < items.Length; i++)
            {
                sum += items[i];
            }
            if (items.Length > 0)
                mean = sum / items.Length;

            return mean;
        }
        public static float MeanOfFloatList(List<float> items)
        {
            float mean = 0;
            float sum = 0;

            for (int i = 0; i < items.Count; i++)
            {
                sum += items[i];
            }

            if (items.Count > 0)
                mean = sum / items.Count;

            return mean;
        }

        public static double MeanOfStringArray(string[] values)
        {
            double cnt = values.Length;
            double sum = 0;

            foreach (var val in values)
            {
                sum += double.Parse(val);
            }

            double mean = sum / cnt;
            return mean;


        }

        public static double MedianOfStringArray(string[] values)
        {
            int cnt = values.Length;
            double median;
            if (cnt == 1)
            {
                median = double.Parse(values[0]);
            }
            else if (cnt % 2 == 0)
            {
                double m1 = double.Parse(values[cnt / 2]);
                double m2 = double.Parse(values[(cnt / 2) - 1]);
                median = (m1 + m2) / 2;
            }
            else
            {
                median = double.Parse(values[cnt / 2]);
            }
            return median;
        }

        public static double VarianceOfStringArray(string[] arr)
        {
            double mean = MeanOfStringArray(arr);
            double[] diffMean = new double[arr.Length];

            for (int i = 0; i < arr.Length; i++)
            {
                double diff = mean - double.Parse(arr[i]);
                diff = Math.Pow(diff, 2);
                diffMean[i] = diff;
            }

            double variance = MeanOfList(diffMean);
            return variance;

        }


        


        public static double ModeOfStringArray(string[] arr)
        {
            var modesList = arr.GroupBy(values => values)
                .Select(valueCluster =>
                    new
                    {
                        value = valueCluster.Key,
                        occurance = valueCluster.Count(),
                    }).ToList();
            int maxOccurrence = modesList
            .Max(g => g.occurance);

            var mode = modesList
            .Where(x => x.occurance == maxOccurrence)
            .Select(x => x.value).First();

            return double.Parse(mode);

        }


        public static string removeZeroIDs(string IDs)
        {
            if (IDs == "0")
            {
                return "";
            }
            if (IDs.Contains("0,"))
            {
                IDs = IDs.Replace("0,", "");
            }
            if (IDs.Contains(",0"))
            {
                IDs = IDs.Replace(",0", "");
            }
            return IDs;
        }

        public static string DateToString(DateTime date)
        {
            try
            {
                PersianCalendar pc = new PersianCalendar();
                string stringDate = string.Format("{0}/{1}/{2}", pc.GetYear(date), pc.GetMonth(date), pc.GetDayOfMonth(date));
                string[] splited = stringDate.Split('/');
                if (splited[1].Length == 1)
                {
                    splited[1] = "0" + splited[1];
                }
                if (splited[2].Length == 1)
                {
                    splited[2] = "0" + splited[2];
                }
                stringDate = splited[0] + "/" + splited[1] + "/" + splited[2] ;
                return stringDate;
            }
            catch (Exception e)
            {
                return "";
            }
            
        }

        



        public static string DateToTimeString(DateTime date)
        {
            try
            {
                PersianCalendar pc = new PersianCalendar();
                string hour = pc.GetHour(date).ToString();
                if (hour.Length == 1)
                {
                    hour = "0" + hour;
                }
                string minute = pc.GetMinute(date).ToString();
                if (minute.Length == 1)
                {
                    minute = "0" + minute;
                }
                //string stringDate = string.Format("{0}:{1}", pc.GetHour(_date), pc.GetMinute(_date));
                string stringDate = hour + ":" + minute;
                return stringDate;
            }
            catch (Exception e)
            {
                return "";
            }
            
        }

        public static string DateToTimeString(DateTimeOffset date)
        {
            try
            {
                //DateTime _date = date;
                //PersianCalendar pc = new PersianCalendar();

                string hour = date.Hour.ToString();
                if (hour.Length == 1)
                {
                    hour = "0" + hour;
                }
                string minute = date.Minute.ToString();
                if (minute.Length == 1)
                {
                    minute = "0" + minute;
                }
                //string stringDate = string.Format("{0}:{1}", pc.GetHour(_date), pc.GetMinute(_date));
                string stringDate = hour + ":" + minute;
                return stringDate;
            }
            catch (Exception e)
            {
                return "";
            }
            
        }

        public static string DateToString(DateTimeOffset dateOffset)
        {
            try
            {
                DateTime date = DateTime.Parse(dateOffset.ToString());

                PersianCalendar pc = new PersianCalendar();
                string stringDate = string.Format("{0}/{1}/{2}", pc.GetYear(date), pc.GetMonth(date), pc.GetDayOfMonth(date));
                string[] splited = stringDate.Split('/');
                if (splited[1].Length == 1)
                {
                    splited[1] = "0" + splited[1];
                }
                if (splited[2].Length == 1)
                {
                    splited[2] = "0" + splited[2];
                }
                stringDate = splited[0] + "/" + splited[1] + "/" + splited[2] ;
                return stringDate;
            }
            catch (Exception e)
            {
                return "";
            }
            
        }


        public static DateTime excelStringToDate(string sDate)
        {
            try
            {
                string mode = sDate.Substring(sDate.Length - 3);
                sDate = sDate.Replace(mode, "");
                sDate = sDate.Substring(0, sDate.Length - 4);
                DateTime date = DateTime.ParseExact(sDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                

                PersianCalendar pc_date = new PersianCalendar();
                date = pc_date.ToDateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Millisecond);
                if (mode == "ب.ظ")
                {
                    date = date.AddHours(12);
                }
                return date;
            }
            catch (Exception e)
            {
                return DateTime.MinValue;
            }
        }

        


        public static bool areEqule(DateTime first , DateTime second)
        {
            if (first.Year == second.Year && first.Month == second.Month && first.Day == second.Day)
            {
                return true;
            }
            return false;
        }

        public static string createRandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }



    }
}
