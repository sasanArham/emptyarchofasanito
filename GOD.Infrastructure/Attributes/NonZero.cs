﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Attributes
{
    /// <summary>
    /// NonZero
    /// </summary>
    [Description("NonZero")]
    public class NonZero :  ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var number = Int32.Parse(value.ToString());
            if (number != null)
            {
                return number != 0;
            }
            return false;
        }
    }
}
