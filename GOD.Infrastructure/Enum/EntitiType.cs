﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Enum
{
    public enum EntitiType
    {
        [Description("شخص")]
        Person = 1,

        [Description("شرکت")]
        Company = 2,

        [Description("فاکتور")]
        Invoice = 3,

        [Description("مذاکره")]
        Negotiation = 4,

        [Description("قرارداد")]
        Contract = 5,


        [Description("نامه")]
        Letter = 6,


        [Description("محصول")]
        Product = 7,

        [Description("درامد")]
        Income = 8,

        [Description("هزینه")]
        Cost = 9,

        [Description("تراکنش داخلی")]
        InternalBankAccountTrnsaction = 10 , 

        [Description("فرایند")]
        BusinessProcess = 11
    }
}
