﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Enum
{
    public enum FilterType
    {
        [Description("بدون فیلتر")]
        None = 0,

        [Description("بزرگتر")]
        Greater = 1,

        [Description("کوچکتر")]
        Less = 2,

        [Description("بین")]
        Between = 3,

    }
}
