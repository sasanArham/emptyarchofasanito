﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Enum.User
{
    public enum UserStatus
    {
        [Description("فعال")]
        active = 1 ,

        [Description("حذف شده")]
        deleted =2
    }
}
