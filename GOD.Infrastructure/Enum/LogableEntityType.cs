﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Infrastructure.Enum
{
    public enum LogableEntityType
    {

        [Description("سایر")]
        NotLogable = 1,

        [Description("مخاطب")]
        Person = 2,

        [Description("شرکت")]
        Company = 3,

        [Description("درگاه پرداخت")]
        PaymentGateWay = 4,

        [Description("شماره تماس")]
        PhoneNumber = 5,

        [Description("سوشال")]
        SocialMedia = 6,

        [Description("مرکز")]
        Station = 7,

        [Description("قرارداد")]
        Contract = 8,
        

        [Description("بانک")]
        Bank = 10,

        [Description("حساب بانکی")]
        BankAccount = 11,

        [Description("چک")]
        BankChack = 12,

        [Description("برگه ی چک")]
        CheckSheet = 13,

        [Description("هزینه")]
        Cost = 14,

        [Description("دسته هزینه")]
        CostGroup = 15,


        [Description("نوع هزینه")]
        CostType = 16,


        //[Description("ضمانت")]
        //Guarantee = 17,


        [Description("درامد")]
        Income = 18,


        [Description("سر فصل درامد")]
        IncomeHeading = 19,


        [Description("تراکنش داخلی")]
        InternalBankAccountTrnsaction = 20,


        [Description("تسهیلات")]
        Loan = 21,


        [Description("اموال")]
        Property = 22,


        [Description("دسته اموال")]
        PropertyGroup = 23,


        [Description("سهامدار")]
        ShareHolder = 24,


        [Description("تراکنش سهامدار")]
        ShareHolderTransaction = 25,


        [Description("تمپلیت نامه")]
        LetterTemplate = 26,


        [Description("تمپلیت قرارداد")]
        ContractTemplate = 27,


        [Description("فاکتور")]
        Invoice = 28,


        [Description("نامه")]
        Letter = 29,


        [Description("ایمیل")]
        Email = 30,


        [Description("ملاقات")]
        Meeting = 31,


        [Description("یادداشت")]
        Note = 32,


        [Description("تماس")]
        PhoneCall = 33,


        [Description("قیف")]
        Funnel = 34,


        [Description("مرحله قیف")]
        FunnelLevel = 35,


        [Description("مذاکره")]
        Negotiation = 36,


        [Description("سازمان")]
        Organization = 37,


        [Description("محصول")]
        Product = 38,


        [Description("دسته بندی محصول")]
        ProductCategory = 39,


        [Description("انتقال محصول")]
        ProductTransfer = 40,

        [Description("انبار")]
        Warehouse = 41 ,

        [Description("ضمانت")]
        PromissingNoteGuarantee = 42 ,

        [Description("ضمانت")]
        CheckGuarantee = 43 ,

        [Description("ضمانت")]
        BankGuarantee = 44 ,

        [Description("یادآور")]
        ReminderTask = 45 ,

        [Description("ساختار")]
        ProcessType = 46 ,

        [Description("مرحله فرایند")]
        ProcessLevel = 47 ,

        [Description("فرایند")]
        BusinessProcess = 48






    }
}
