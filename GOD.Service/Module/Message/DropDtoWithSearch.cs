﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message
{
    public class DropDtoWithSearch
    {
        public List<DropDto> DropDtos { get; set; }

        public int QueriedCnt { get; set; }
    }
}
