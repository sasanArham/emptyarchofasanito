﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message
{
    public class DropDto
    {
        public int ID { get; set; }

        public string Title { get; set; }
    }
}
