﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message
{
    public class FileDto
    {
        public string Title { get; set; }
        public string Base64 { get; set; }
    }
}
