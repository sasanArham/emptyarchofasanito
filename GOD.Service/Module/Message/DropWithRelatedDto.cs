﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message
{
    public class DropWithRelatedDto : SelectedDropDto
    {
        public bool Related { get; set; }
    }
}
