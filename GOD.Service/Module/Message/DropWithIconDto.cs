﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message
{
    public class DropWithIconDto : DropDto
    {
        public string Url { get; set; }

        public DropWithIconDto()
        {

        }

        public DropWithIconDto(Domain.Module.User._User.User user)
        {
            ID = user.ID;
            Title = user.Name + " " + user.LastName;
            Url = user.ProfileImageUrl;
        }
    }
}
