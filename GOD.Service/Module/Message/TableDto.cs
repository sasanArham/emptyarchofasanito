﻿using asanito.Service.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message
{
    public class TableDto 
    {
        public int QueriedCnt { get; set; }

        public object ResultList { get; set; }

    }
}
