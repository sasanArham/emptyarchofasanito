﻿using asanito.Service.Module.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message
{
    public class FileWithExtensionDto : FileDto
    {
        public string extension { get; set; }
    }
}
