﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message.User
{
    public class UserForPanelDto
    {
        public int ID { get; set; }

        

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Mobile { get; set; }

        public string Province { get; set; }

        public string City { get; set; }

        public int CityID { get; set; }

        public string UserGroup { get; set; }

        public int UserGroupID { get; set; }

        public UserStatusDto Status { get; set; }

        
        public int ProvinceID { get; set; }

        public string Description { get; set; }

        public string Email { get; set; }




        /// <summary>
        /// عکس پروفایل
        /// </summary>
        public string ProfileImageUrl { get; set; }

        public string Username { get; set; }


        public UserForPanelDto()
        {

        }


        public UserForPanelDto(Domain.Module.User._User.User model)
        {
            ID = model.ID;
            Name = model.Name;
            LastName = model.LastName;
            Mobile = model.Mobile;
            Email = model.Email;
            Description = model.Description;
        
            
            if (model.City != null)
            {
                Province = model.City.Province.Title;
                ProvinceID = model.City.ProvinceID;
                City = model.City.Title;
                CityID = model.CityID??0;
            }
            UserGroup = model.UserGroup.Title;
            UserGroupID = model.UserGroupID??0;
            Status = new UserStatusDto(model.UserStatus);


            ProfileImageUrl = model.ProfileImageUrl;
            Username = model.Username;
        }


        
    }
}
