﻿using asanito.Service.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message.User
{
    public class UserDto : BaseDto
    {
        public int ID;
        public string Name;
        public string UserName;
        public string LastName;
        public string Mobile;
        public int UserGroupID;
        public UserStatusDto UserStatus;
        public string Token;
        public string RefreshToken { get; set; }
        public UserGroupDto UserGroupDto { get; set; }

        /// <summary>
        /// عکس پروفایل
        /// </summary>
        public string ProfileImageUrl { get; set; }




        public UserDto()
        {

        }

        public UserDto(Domain.Module.User._User.User model, bool includePrivate = false)
        {
            this.ID = model.ID;
            this.Name = model.Name;
            this.LastName = model.LastName;
            this.UserGroupID = model.UserGroupID??0;
            this.Deleted = model.Deleted;
            this.Description = model.Description;
            
            this.UserStatus = new UserStatusDto(model.UserStatus);
            if (includePrivate)
            {
                this.Mobile = model.Mobile;
                
                this.UserName = model.Username;

            }
            ProfileImageUrl = model.ProfileImageUrl;
        }

        public void setAllInfo(Domain.Module.User._User.User model)
        {
            this.ID = model.ID;
            this.Name = model.Name;
            this.UserName = model.Username;
            this.LastName = model.LastName;
            this.Mobile = model.Mobile;
            this.UserStatus = new UserStatusDto(model.UserStatus);
            this.UserGroupDto = new UserGroupDto(model.UserGroup);
            this.Deleted = model.Deleted;
            this.Description = model.Description;
            ProfileImageUrl = model.ProfileImageUrl;

        }
    }
}
