﻿using asanito.Domain.Module.User;
using asanito.Service.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message.User
{
    public class UserGroupDto : BaseDto
    {
        
        public string Title { get; set; }

        public int UsersCount { get; set; }


        public UserGroupDto()
        {

        }

        public UserGroupDto(UserGroup model) :base(model)
        {
            Title = model.Title;
            if (model.Users != null)
            {
                UsersCount = model.Users.Count;
            }
            
        }
    }
}
