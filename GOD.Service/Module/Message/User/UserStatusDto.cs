﻿using asanito.Infrastructure.Enum.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message.User
{
    public class UserStatusDto
    {
        public int ID { get; set; }
        public string Title { get; set; }

        public UserStatusDto()
        {

        }

        public UserStatusDto(UserStatus model)
        {
            ID = (int)model;
            FieldInfo fieldInfo = model.GetType().GetField(model.ToString());
            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                Title = attributes[0].Description;
            else
                Title = model.ToString();
        }
    }
}
