﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message
{
    public class ChartDto
    {
        public string X { get; set; }

        public double Y { get; set; }

        public ChartDto()
        {
                
        }

        public ChartDto(string x , long y)
        {
            X = x;
            Y = y;
        }
    }
}
