﻿using asanito.Infrastructure.Consts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace asanito.Service.Module.Message
{
    public class FileUrlDto
    {
        public int ID { get; set; }
        public string Title { get; set; }

        public long Size { get; set; }

        public string Url { get; set; }


        public FileUrlDto()
        {

        }


        public FileUrlDto(int ID , string title , string url , string dir) 
        {
            this.ID = ID;
            Title = title;
            
            Url = dir + "/" + url;
            var path = Path.Combine(HttpContext.Current.Server.MapPath("~//" + Url + ""));

            try
            {
                FileInfo fileInfo = new FileInfo(path);
                Size = fileInfo.Length;
            }
            catch (Exception e)
            {
                
            }
            
        }
    }
}
