﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message
{
    public class SelectedDropDto :DropDto
    {
        public bool Selected { get; set; }
    }
}
