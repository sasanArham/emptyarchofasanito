﻿using asanito.Domain.Module.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message.Address
{
    public class CityDto 
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public int ProvinceID { get; set; }

        public CityDto()
        {

        }

        public CityDto(City model)
        {
            ID = model.ID;
            Title = model.Title;
            ProvinceID = model.ProvinceID;
        }
    }
}
