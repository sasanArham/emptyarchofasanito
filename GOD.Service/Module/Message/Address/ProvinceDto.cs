﻿using asanito.Domain.Module.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message.Address
{
    public class ProvinceDto
    {
        public int ID { get; set; }

        public string Title { get; set; }


        public ProvinceDto()
        {

        }

        public ProvinceDto(Province model )
        {
            ID = model.ID;
            Title = model.Title;
        }
    }
}
