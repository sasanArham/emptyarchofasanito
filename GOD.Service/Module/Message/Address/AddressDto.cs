﻿using asanito.Service.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Message.Address
{
    public class AddressDto : BaseDto
    {
        public int CityID { get; set; }

        public int ProvinceID { get; set; }

        public string Address { get; set; }

        public bool IsDefault { get; set; }


        public AddressDto(Domain.Module.Address._Adress.Adress model)  :base(model)
        {
            if (model.City != null)
            {
                CityID = model.CityID??0;
                ProvinceID = model.City.ProvinceID;
            }
            Address = model.Address;
            IsDefault = model.IsDefault;



        }
    }
}
