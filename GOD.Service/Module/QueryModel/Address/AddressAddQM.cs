﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.QueryModel.Address
{
    public class AddressAddQM
    {
        public int CityID { get; set; }

        public string Address { get; set; }
    }
}
