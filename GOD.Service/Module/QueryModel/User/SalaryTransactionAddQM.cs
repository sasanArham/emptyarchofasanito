﻿using asanito.Infrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.QueryModel.User
{
    public class SalaryTransactionAddQM
    {
        /// <summary>
        /// <para>آی دی کاربر</para>
        /// </summary>
        [NonZero]
        public int UserID { get; set; }


        /// <summary>
        /// ماه
        /// <para> فروردین:1 اردیبهشت:2 و ...</para>
        /// </summary>
        [Range(1,12)]
        public int Month { get; set; }


        /// <summary>
        /// سازمان
        /// </summary>
        [NonZero]
        public int OrganizationID { get; set; }


        /// <summary>
        /// تعداد روز_ساعت
        /// </summary>
        [Required]
        public int Period { get; set; }


        /// <summary>
        /// حساب بانکی
        /// </summary>
        [NonZero]
        public int AccountID { get; set; }





    }
}
