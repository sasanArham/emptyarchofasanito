﻿using asanito.Service.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.QueryModel.User
{
    public class UserAddQM : BaseQM
    {
        public string Name { get; set; }

        public string LastName { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Mobile { get; set; }

        public int UserGroupID { get; set; }

        public int[] StationIDs { get; set; }


        public int CityID { get; set; }

        public string Email { get; set; }


        public List<UserSalaryAddQM> Salaries { get; set; }


        public string AppPassword { get; set; }

    }
}
