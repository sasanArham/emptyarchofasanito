﻿using asanito.Infrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.QueryModel.User
{
    public class UserSalaryEditQM
    {

        public int ID { get; set; }

        /// <summary>
        /// مبلغ حقوق به ازای ساعت یا روز
        /// </summary>
        [NonZero]
        public long AmountPerHours_Day { get; set; }


        /// <summary>
        /// مشمول بیمه 
        /// </summary>
        public bool Insured { get; set; }


        /// <summary>
        /// حق بیمه کارگر
        /// </summary>
        [Required]
        public int WorkerPermiumPercent { get; set; }


        /// <summary>
        /// حق بیمه کارفرما
        /// </summary>
        [Required]
        public int EmployerPermiumPercent { get; set; }



        /// <summary>
        /// <para>آی دی سازمان</para>
        /// <para> get: organization/geList </para>
        /// </summary>
        [NonZero]
        public int OrganizationID { get; set; }
    }
}
