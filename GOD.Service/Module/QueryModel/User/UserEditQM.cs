﻿using asanito.Service.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.QueryModel.User
{
    public class UserEditQM : BaseQM
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }
        

        public string Mobile { get; set; }

        public int UserGroupID { get; set; }
        

        public int CityID { get; set; }

        public string Email { get; set; }

        public string AppPassword { get; set; }

        public bool AppPasswordChanged { get; set; }
    }
}
