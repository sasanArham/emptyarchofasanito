﻿using asanito.Infrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.QueryModel.User
{
    public class UserSalaryAddQM
    {
        /// <summary>
        /// نوع محاسبه ی حقوق
        /// <para> true : ساعتی </para>
        /// <para> false : روزانه </para>
        /// </summary>
        public bool CalculationType { get; set; }


        /// <summary>
        /// مبلغ حقوق به ازای ساعت یا روز
        /// </summary>
        [NonZero]
        public long AmountPerHours_Day { get; set; }


        /// <summary>
        /// مشمول بیمه 
        /// </summary>
        public bool Insured { get; set; }


        /// <summary>
        /// حق بیمه کارگر
        /// </summary>
        [Required]
        public int WorkerPermiumPercent { get; set; }


        /// <summary>
        /// حق بیمه کارفرما
        /// </summary>
        [Required]
        public int EmployerPermiumPercent { get; set; }
        


        /// <summary>
        /// <para>آی دی سازمان</para>
        /// <para> get: organization/geList </para>
        /// </summary>
        [NonZero]
        public int OrganizationID { get; set; }


        /// <summary>
        /// <para>آی دی کاربر</para>
        /// <para> get: user/geListWith </para>
        /// </summary>
        [Required]
        public int UserID { get; set; }
    }
}
