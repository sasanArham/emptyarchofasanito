﻿
namespace asanito.Service.Module.QueryModel
{
    public class DropQM 
    {
        public int ID { get; set; }

        /// <summary>
        /// رشته مورد جستجو
        /// </summary>
        public string Value { get; set; }


        public int Skip{ get; set; }

        public int Take { get; set; }

    }
}
