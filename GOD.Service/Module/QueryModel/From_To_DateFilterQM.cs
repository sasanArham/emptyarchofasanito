﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.QueryModel
{
    public class From_To_DateFilterQM
    {
        /// <summary>
        /// از تاریخ
        /// </summary>
        public string FromDate { get; set; }

        /// <summary>
        /// تا تاریخ
        /// </summary>
        public string ToDate { get; set; }
    }
}
