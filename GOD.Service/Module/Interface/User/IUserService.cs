﻿using asanito.Service.SeedWorks.Base;
using asanito.Infrastructure.Enum.User;
using asanito.Service.Module.Message.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Linq.Expressions;
using System.Web.Routing;
using asanito.Service.Module.QueryModel.User;
using asanito.Service.Module.Message;
using asanito.Service.Module.QueryModel;

namespace asanito.Service.Module.Interface.User
{
    public interface IUserService 
    {
        Task<UserDto> loginAsync(string userName, string password, string clientID);

        List<UserForPanelDto> getUsers(int userGroupID, out string error);

        int checkDupication(string username, string mobile);

        UserForPanelDto createNewUser(int userID, UserAddQM qm, out string error);


        UserForPanelDto edit(UserEditQM qm, out string error);

        bool delete(string username);



        bool update(int ID, string name, string lastName, string mobile, int cityID, int userGroupID, out string error);


        Domain.Module.User._User.User selectModelByID(int ID);


        Domain.Module.User._User.User selectModel(Expression<Func<Domain.Module.User._User.User, bool>> expression);

        bool sendSMS(string message, string mobile);

        bool sendSMS(string message, string mobile, int smsTemplateCode);

        Domain.Module.User._User.User getUserModelByToken(RequestContext actionContext);

        bool setAsDeleted(int ID, out string error);

        UserDto getUserDtoByToken(RequestContext actionContext);

        List<DropDto> getListWith(int ID);

        List<DropDto> getDropListWith(DropQM qm);

        bool addStationToUser(int creatorUserID, int userID, int stationID, out string error);

        bool deltedStattion(int userID, int stationID );

        List<int> getUserAllowedWarehouses(int ID);

        bool createListFromExcel(int userID, HttpPostedFile file, out string error);

        string uploadProfileImage(int ID, HttpPostedFile file, out string error);

        List<SelectedDropWithIconDto> getUsersForProject(int projectID);
    }
}
