﻿using asanito.Service.SeedWorks.Base;
using asanito.Domain.Module.User;
using asanito.Service.Module.Message.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using asanito.Service.Module.QueryModel;

namespace asanito.Service.Module.Interface.User
{
    public interface IUserGroupService : IService<UserGroup>
    {

        UserGroupDto addNew(int userID, string title, string description, out string error);

        bool exist(string title);

        List<UserGroupDto> selectAll();
        UserGroupDto update(int ID, string title, string description, out string error);
        bool delete(int ID,  out string error);
        

        

        List<UserGroupDto> selectNotBaseGroups();

        List<UserGroupDto> selectDropNotBaseGroups(DropQM qm);
    }
}
