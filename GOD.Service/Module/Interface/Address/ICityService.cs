﻿using asanito.Service.SeedWorks.Base;
using asanito.Domain.Module.Address;
using asanito.Service.Module.Message.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using asanito.Service.Module.QueryModel;

namespace asanito.Service.Module.Interface.Address
{
    public interface ICityService : IService<City>
    {
        bool exist(int ID);

        bool addNew(int provinceID, string title, out string error);

        bool update(int ID, int provinceID, string title, out string error);

        bool delete(int ID, out string error);

        List<CityDto> getList(int provinceID, int ID = 0);

        List<CityDto> getDropList(int provinceID, DropQM qm);

        List<CityDto> getByProvinceIDs(string provinceIDs);
    }
}
