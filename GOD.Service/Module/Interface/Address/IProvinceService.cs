﻿using asanito.Service.SeedWorks.Base;
using asanito.Domain.Module.Address;
using asanito.Service.Module.Message.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using asanito.Service.Module.QueryModel;

namespace asanito.Service.Module.Interface.Address
{
    public interface IProvinceService  :IService<Province>
    {
        bool addNew(string title, out string error);

        bool update(int ID, string title, out string error);

        bool delete(int ID, out string error);

        List<ProvinceDto> getList();

        bool exist(int ID);

        List<ProvinceDto> getListWith(int ID);

        List<ProvinceDto> getDropListWith(DropQM qm);
    }
}
