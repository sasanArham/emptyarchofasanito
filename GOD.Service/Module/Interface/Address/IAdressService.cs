﻿using asanito.Domain.Module.Address._Adress;
using asanito.Service.Module.Message.Address;
using asanito.Service.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Interface.Address
{
    public interface IAdressService  :IService<Adress>
    {
        bool edit(int ID, int cityID, string addressText, out string error);

        AddressDto addNewToPerson(int userID, int personID, int cityID
            , string addressText, out string error);

        AddressDto addNewToComany(int userID, int companyID, int cityID
            , string addressText, out string error);
    }
}
