﻿using asanito.Infrastructure.Consts;
using asanito.Service.SeedWorks.Base;
using asanito.Domain.Module.Address;
using asanito.Repository.Modules.Interface.Address;
using asanito.Service.Module.Interface.Address;
using asanito.Service.Module.Message.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using asanito.Service.Module.QueryModel;
using System.Linq.Expressions;

namespace asanito.Service.Module.Implement.Address
{
    public  class CityService : Service<City> , ICityService
    {
        private ICityRepository repository;
        private IProvinceService provinceService;

        //public CityService(ICityRepository repo) : base(repo)
        //{
        //    repository = repo;
        //}


        

        public CityService(ICityRepository repo , IProvinceService provinceService) : base(repo)
        {
            repository = repo;
            this.provinceService = provinceService;
        }

        public override void setCurrentUser(int userID)
        {
            base.setCurrentUser(userID);
            provinceService.setCurrentUser(userID);
        }

        public virtual bool exist (int ID)
        {
            City city = repository.Select(c => c.ID == ID);
            if (city == null)
            {
                return false;
            }
            return true;
        }

        public bool addNew(int provinceID , string title  ,out string error)
        {
            error = "";
            bool exist = provinceService.exist(provinceID);
            if (!exist)
            {
                error = ErrorMessage.Province.notvalidID;
                return false;
            }

            City city = new City();
            city.ProvinceID = provinceID;
            city.Title = title;
            city = repository.insert(city);
            if (city == null)
            {
                error = ErrorMessage.insertError;
                return false;
            }
            return true;


        }

        public  bool update (int ID , int provinceID, string title, out string error)
        {
            bool testt = this.exist(1);
            error = "";
            City city = repository.Select(c => c.ID == ID);
            if (city == null)
            {
                error = ErrorMessage.City.notvalidID;
                return false;
            }
            bool provinceExist = provinceService.exist(provinceID);
            if (!provinceExist)
            {
                error = ErrorMessage.Province.notvalidID;
                return false;
            }
            city.Title = title;
            city.ProvinceID = provinceID;
            bool updated = repository.update(city);
            if (!updated)
            {
                error = ErrorMessage.insertError;
                return false;
            }
            return true;
        }

        public bool delete(int ID , out string error)
        {
            error = "";
            City city = repository.Select(c => c.ID == ID);
            if (city == null)
            {
                error = ErrorMessage.City.notvalidID;
                return false;
            }
            city.Deleted = true;
            bool updated = repository.update(city);
            if (!updated)
            {
                error = ErrorMessage.deleteError;
                return false;
            }
            return true;
        }


        public List<CityDto> getList(int provinceID , int ID= 0)
        {
            List<CityDto> cityDtos = new List<CityDto>();
            List<City> cities = repository.SelectList(c => c.ProvinceID == provinceID 
            && !c.Province.Deleted 
            && !c.Deleted || c.ID == ID);
            if (cities == null)
            {
                return cityDtos;
            }
            for (int i = 0; i < cities.Count; i++)
            {
                CityDto cityDto = new CityDto(cities[i]);
                cityDtos.Add(cityDto);
            }
            return cityDtos;
        }

        public List<CityDto> getDropList(int provinceID, DropQM qm)
        {
            List<CityDto> cityDtos = new List<CityDto>();
            Expression<Func<City, bool>> expression = c => (c.ProvinceID == provinceID
            && !c.Province.Deleted
            && !c.Deleted || c.ID == qm.ID)
            && (string.IsNullOrEmpty(qm.Value) || c.Title.Contains(qm.Value));

            var cities = repository.SelectDropList(expression, qm.Skip, qm.Take);

            if (cities == null)
            {
                return cityDtos;
            }
            for (int i = 0; i < cities.Count; i++)
            {
                CityDto cityDto = new CityDto(cities[i]);
                cityDtos.Add(cityDto);
            }
            return cityDtos;
        }

        public List<CityDto> getByProvinceIDs(string provinceIDs)
        {
            List<CityDto> dtos = new List<CityDto>();
            int[] IDs = G.stringToIntArray(provinceIDs);
            if (IDs == null)
            {
                return dtos;
            }
            if (IDs.Length == 0 )
            {
                return dtos;
            }
            var cities = repository.SelectList(c => IDs.Contains(c.ProvinceID));
            if (cities == null)
            {
                return dtos;
            }
            foreach (var city in cities)
            {
                CityDto dto = new CityDto(city);
                dtos.Add(dto);
            }
            return dtos;
        }

    }
}
