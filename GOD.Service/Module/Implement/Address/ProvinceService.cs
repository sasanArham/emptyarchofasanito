﻿using asanito.Infrastructure.Consts;
using asanito.Service.SeedWorks.Base;
using asanito.Domain.Module.Address;
using asanito.Repository.Modules.Interface.Address;
using asanito.Service.Module.Interface.Address;
using asanito.Service.Module.Message.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using asanito.Service.Module.QueryModel;

namespace asanito.Service.Module.Implement.Address
{
    public class ProvinceService : Service<Province> , IProvinceService
    {
        private IProvinceRepository repository;

        public ProvinceService(IProvinceRepository repo) : base(repo)
        {
            repository = repo;
        }

        


        public bool addNew(string title , out string error)
        {
            error = "";
            Province province = repository.Select(c => c.Title == title  && !c.Deleted);
            if (province != null)
            {
               
                error = ErrorMessage.notUniqueData;
                return false;
            }

            province = new Province();
            province.Title = title;
            province.CreateDate = DateTime.Now;
            province.CreatorUserID = 1;
            province = repository.insertWithCode(province);
            if (province == null)
            {
                error = ErrorMessage.insertError;
                return false;
            }
            return true;

        }

        public bool update(int ID , string title , out string error)
        {
            error = "";
            Province province = repository.Select(c => c.ID == ID);
            if (province == null)
            {
                error = ErrorMessage.loadError;
                return false;
            }
            Province anotherProvince = repository.Select(c => c.Title == title && c.ID != ID && !c.Deleted);
            if (anotherProvince != null)
            {
                
                error = ErrorMessage.notUniqueData;
                return false;
            }

            province.Title = title;
            bool updated = repository.update(province);
            if (!updated )
            {
                error = ErrorMessage.insertError;
                return false;
            }

            return true;

        }

        public bool delete(int ID , out string error)
        {
            error = "";
            Province province = repository.Select(c => c.ID == ID);
            if (province == null)
            {
                error = ErrorMessage.loadError;
                return false;
            }

            province.Deleted = true;
            bool updated = repository.update(province);
            if (!updated)
            {
                error = ErrorMessage.deleteError;
                return false;
            }

            return true;

        }

        public List<ProvinceDto> getList()
        {
            List<ProvinceDto> provinceDtos = new List<ProvinceDto>();
            List<Province> provinces = repository.SelectList(c=>!c.Deleted);
            if (provinces == null)
            {
                return provinceDtos;
            }

            for (int i = 0; i < provinces.Count; i++)
            {
                ProvinceDto provinceDto = new ProvinceDto(provinces[i]);
                provinceDtos.Add(provinceDto);
            }

            return provinceDtos; 
        }

        public bool exist(int ID)
        {
            Province province = repository.Select(c => c.ID == ID);
            if (province == null)
            {
                return false;
            }
            return true;
        }


        public List<ProvinceDto> getListWith(int ID)
        {
            List<ProvinceDto> dtos = new List<ProvinceDto>();
            var provinces = repository.SelectListWith(ID);
            if (provinces == null)
            {
                return dtos;
            }
            foreach (var province in provinces)
            {
                ProvinceDto dto = new ProvinceDto(province);
                dtos.Add(dto);
            }

            return dtos;
        }
        public List<ProvinceDto> getDropListWith(DropQM qm)
        {
            List<ProvinceDto> dtos = new List<ProvinceDto>();
            Expression<Func<Province, bool>> expression = c => (!c.Deleted || c.ID == qm.ID)
            && (string.IsNullOrEmpty(qm.Value) || c.Title.Contains(qm.Value));

            var provinces = repository.SelectDropList(expression, qm.Skip, qm.Take);

            if (provinces == null)
            {
                return dtos;
            }
            foreach (var province in provinces)
            {
                ProvinceDto dto = new ProvinceDto(province);
                dtos.Add(dto);
            }

            return dtos;
        }

    }
}
