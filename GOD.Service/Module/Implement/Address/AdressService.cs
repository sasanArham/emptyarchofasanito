﻿using asanito.Domain.Module.Address._Adress;
using asanito.Infrastructure.Consts;
using asanito.Repository.Modules.Interface.Address;
using asanito.Service.Module.Interface.Address;
using asanito.Service.Module.Message.Address;
using asanito.Service.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.Module.Implement.Address
{
    public class AdressService : Service<Adress> , IAdressService
    {
        private IAddressRepository repository ;

        public AdressService(IAddressRepository repo) : base(repo)
        {
            repository = repo;
        }


        

        public bool edit(int ID , int cityID , string addressText , out string error)
        {
            error = "";
            var address = repository.Select(c => c.ID == ID);
            if (address == null)
            {
                error = ErrorMessage.Address.notValidID;
                return false;
            }
            address.Address = addressText;
            if (cityID != 0)
            {
                address.CityID = cityID;
            }
            bool updated = repository.update(address);
            if (updated)
            {
                error = ErrorMessage.insertError;
            }
            return updated;



        }

        public AddressDto addNewToPerson(int userID , int personID , int cityID
            , string addressText, out string error)
        {
            error = "";
            var address = new Adress(userID, cityID, addressText, false);
            address.PersonID = personID;
            address = repository.insert(address);
            if (address == null)
            {
                error = ErrorMessage.insertError;
                return null;
            }
            AddressDto added = new AddressDto(address);
            return added;

        }


        public AddressDto addNewToComany(int userID, int companyID, int cityID
            , string addressText, out string error)
        {
            error = "";
            var address = new Adress(userID, cityID, addressText, false);
            address.CompanyID = companyID;
            address = repository.insert(address);
            if (address == null)
            {
                error = ErrorMessage.insertError;
                return null;
            }
            AddressDto added = new AddressDto(address);
            return added;

        }
    }
}
