﻿using asanito.Infrastructure.Consts;
using asanito.Infrastructure.Enum.User;
using asanito.Service.SeedWorks.Base;
using asanito.Domain.Module.User;
using asanito.Domain.Module.User._UserGroup.Factory;
using asanito.Repository.Modules.Interface;
using asanito.Service.Module.Interface.User;
using asanito.Service.Module.Message.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using asanito.Service.Module.QueryModel;

namespace asanito.Service.Module.Implement.User
{
    public class UserGroupService : Service<UserGroup> , IUserGroupService
    {
        private IUserGroupRepository repository;
        private UserGroupFactory userGroupFactory;


        public UserGroupService(IUserGroupRepository repo) : base(repo)
        {
            repository = repo;
        }
        

        public UserGroupDto addNew(int userID , string title , string description 
            , out string error)
        {
            error = "";

            bool exist = this.exist(title);
            if (exist)
            {
                error = ErrorMessage.notUniqueData;
                return null;
            }
            
            userGroupFactory = new SimpleUserGroupFactory(userID, title,description);
            UserGroup userGroup = userGroupFactory.getUserGroup();
            userGroup = repository.insertWithCode(userGroup);
            if (userGroup == null)
            {
                error = ErrorMessage.insertError;
                return null;
            }
            UserGroupDto addedDto = new UserGroupDto(userGroup);
            return addedDto;


        }

        public bool delete(int ID, out string error)
        {
            error = "";

           
            
            bool deleted = repository.deleteOrSetAsDeleted(ID);
            if (!deleted)
            {
                error = ErrorMessage.deleteError;
                return false;
            }

            return true;




        }

        public bool exist(string title)
        {
            UserGroup userGroup = repository.Select(c => c.Title == title && !c.Deleted);
            if (userGroup != null)
            {
                return true;
            }
            return false;
        }

        public List<UserGroupDto> selectAll()
        {
            List<UserGroup> userGroups = repository.SelectList(c => !c.Deleted);
            List<UserGroupDto> userGroupDtos = new List<UserGroupDto>();
            if (userGroups == null)
            {
                return userGroupDtos;
            }
            for (int i = 0; i < userGroups.Count; i++)
            {
                UserGroupDto userGroupDto = new UserGroupDto(userGroups[i]);
                userGroupDtos.Add(userGroupDto);
            }

            return userGroupDtos;
        }

        public List<UserGroupDto> selectNotBaseGroups()
        {
            List<UserGroup> userGroups = repository.SelectList(c => !c.Deleted);
            List<UserGroupDto> userGroupDtos = new List<UserGroupDto>();
            if (userGroups == null)
            {
                return userGroupDtos;
            }
            for (int i = 0; i < userGroups.Count; i++)
            {
                UserGroupDto userGroupDto = new UserGroupDto(userGroups[i]);
                userGroupDtos.Add(userGroupDto);
            }

            return userGroupDtos;
        }

        public List<UserGroupDto> selectDropNotBaseGroups(DropQM qm)
        {
            List<UserGroupDto> userGroupDtos = new List<UserGroupDto>();

            Expression<Func<UserGroup, bool>> expression = c => !c.Deleted
            && (string.IsNullOrEmpty(qm.Value) || c.Title.Contains(qm.Value));

            var userGroups = repository.SelectDropList(expression, qm.Skip, qm.Take);

            if (userGroups == null)
            {
                return userGroupDtos;
            }
            for (int i = 0; i < userGroups.Count; i++)
            {
                UserGroupDto userGroupDto = new UserGroupDto(userGroups[i]);
                userGroupDtos.Add(userGroupDto);
            }

            return userGroupDtos;
        }
        public UserGroupDto update(int ID, string title ,string description 
            ,  out string error)
        {
            error = "";
            UserGroup userGroup = repository.Select(c => c.ID == ID);
            if (userGroup == null)
            {
                error = ErrorMessage.UserGroup.notvalidID;
                return null;
            }
            UserGroup anotherGroup = repository.Select(c => c.Title == title && c.ID != ID && !c.Deleted);
            if (anotherGroup !=null)
            {
                error = ErrorMessage.notUniqueData;
                return null;
            }
            userGroup.Title = title;
            bool updated = repository.update(userGroup);
            if (!updated)
            {
                error = ErrorMessage.insertError;
                return null;
            }
            UserGroupDto dto = new UserGroupDto(userGroup);
            return dto;
            

        }

        


        


    }
}
