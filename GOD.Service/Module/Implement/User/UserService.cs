﻿using asanito.Infrastructure.Consts;
using asanito.Infrastructure.Helpers;
using asanito.Domain.Module.User;
using asanito.Domain.Module.User._User.Factory;
using asanito.Repository.Modules.Interface;
using asanito.Repository.Modules.Interface.User;
using asanito.Service.Module.Interface.Address;
using asanito.Service.Module.Interface.User;
using asanito.Service.Module.Message.User;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System;
using SmsIrRestful;
using System.Security.Claims;
using System.Web.Routing;
using System.Net;
using System.Web.Http;
using System.Linq;
using asanito.Service.Module.QueryModel.User;
using asanito.Service.Module.Message;
using System.Data;
using System.Web;
using System.IO;
using ClosedXML.Excel;

using asanito.Service.Module.QueryModel;

namespace asanito.Service.Module.Implement.User
{
    public class UserService : IUserService
    {
        private IUserRepository repository;
        private IUserGroupRepository userGroupRepository;
        private IUserGroupService userGroupService;
        private ICityService cityService;

        public UserService(IUserRepository repo, IUserGroupRepository userGroupRepository
            , IUserGroupService userGroupService, ICityService cityService
              )
        {
            repository = repo;
            this.userGroupRepository = userGroupRepository;
            this.userGroupService = userGroupService;
            this.cityService = cityService;
            
        }

        public async Task<UserDto> loginAsync(string userName, string password, string clientID)
        {
            string token = await OAuthToken.getToken(userName, password, clientID);
            if (token == "")
            {
                return null;
            }

            Domain.Module.User._User.User user = repository.selectByUserName(userName);
            if (user == null)
            {
                return null;
            }


            UserDto userDto = new UserDto(user);
            userDto.Token = token;
            return userDto;
        }

        public List<UserForPanelDto> getUsers(int userGroupID, out string error)
        {
            error = "";

            List<UserForPanelDto> dtos = new List<UserForPanelDto>();
            List<Domain.Module.User._User.User> users;
            if (userGroupID != 0)
            {
                UserGroup userGroup = userGroupRepository.Select(c => c.ID == userGroupID && c.Deleted == false);
                if (userGroup == null)
                {
                    error = ErrorMessage.loadError;
                    return dtos;
                }
                if (userGroup.Users == null)
                {
                    return dtos;
                }
                
                users = userGroup.Users.Where(c => c.Deleted == false).OrderByDescending(c => c.ID).ToList();
            }
            else
            {
                users = repository.SelectList();
            }

            if (users == null)
            {
                return dtos;
            }

            foreach (var item in users)
            {
                UserForPanelDto dto = new UserForPanelDto(item);
                dtos.Add(dto);
            }


            return dtos;

        }

        /// <summary>
        /// افزودن کاربر از پنل
        /// </summary>
        /// <param name="name"></param>
        /// <param name="lastName"></param>
        /// <param name="mobile"></param>
        /// <param name="username"></param>
        /// <param name="cityID"></param>
        /// <param name="userGroupID"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public UserForPanelDto createNewUser(int userID, UserAddQM qm, out string error)
        {
            error = "";

            //bool duplicateNumber = phoneNumberRepository.checkDuplicatePersonNumber(qm.Mobile, out error);
            //if (duplicateNumber)
            //{
            //    return null;
            //}

            int duplicateCheckResult = this.checkDupication(qm.Username, qm.Mobile);
            //if (duplicateCheckResult == 2)
            //{
            //    error = ErrorMessage.User.alreadyExsistMobile;
            //    return null;
            //}
            if (duplicateCheckResult == 3)
            {
                error = ErrorMessage.User.alreadyExsistUsername;
                return null;
            }


            var group = userGroupRepository.Select(c => c.ID == qm.UserGroupID);
            if (group == null)
            {
                error = ErrorMessage.UserGroup.notvalidID;
                return null;
            }


            UserFactory factory = new PanelCreatedUserFactory(userID, qm.Description, qm.Name, qm.LastName
                , qm.Username, qm.Mobile, qm.CityID, qm.UserGroupID, qm.Email, qm.StationIDs, qm.AppPassword);
                
            var user = factory.GetUser();
            user = repository.insert(user);
            if (user == null)
            {
                error = ErrorMessage.insertError;
                return null;
            }

            

            UserForPanelDto addedItem = new UserForPanelDto(user);
            return addedItem;


        }


        public UserForPanelDto edit(UserEditQM qm, out string error)
        {
            error = "";
            var user = repository.Select(c => c.ID == qm.ID);
            if (user == null)
            {
                error = ErrorMessage.User.notValidID;
                return null;
            }

            if (qm.CityID != 0)
            {
                user.CityID = qm.CityID;
            }
            user.Description = qm.Description;
            user.Email = qm.Email;
            user.LastName = qm.LastName;
            user.Mobile = qm.Mobile;
            user.Name = qm.Name;
            user.UserGroupID = qm.UserGroupID;
            if (qm.AppPasswordChanged)
            {
                if (string.IsNullOrEmpty(qm.AppPassword))
                {
                    user.AppPassword = "";
                }
                else
                {
                    user.AppPassword = StringCipher.Encrypt(qm.AppPassword);
                }
            }

            bool updated = repository.update(user);
            if (!updated)
            {
                error = ErrorMessage.insertError;
                return null;
            }
            UserForPanelDto updatedDto = new UserForPanelDto(user);
            return updatedDto;
        }


        public bool delete(int ID)
        {
            return repository.delete(c => c.ID == ID);
        }

        /// <summary>
        /// 1 : not duplicate
        /// <para>2 : duplicate mobile</para>
        /// <para>3 : duplicate username</para>
        /// </summary>
        /// <param name="username"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public int checkDupication(string username, string mobile)
        {

            Domain.Module.User._User.User user = repository.Select(c => c.Username == username);
            if (user != null)
            {
                return 3;
            }
            user = repository.Select(c => c.Mobile == mobile);
            if (user != null)
            {
                return 2;
            }
            return 1;
        }

        public bool isDuplicateUsername(string username)
        {

            Domain.Module.User._User.User user = repository.Select(c => c.Username == username);
            if (user != null)
            {
                return true;
            }

            return false;
        }

        public bool isDuplicate(string mobile, int currentUserID)
        {

            Domain.Module.User._User.User user = repository.Select(c => c.Mobile == mobile && c.ID != currentUserID);
            if (user != null)
            {
                return true;
            }
            return false;
        }


        public bool setAsDeleted(int ID, out string error)
        {
            error = "";
            var user = repository.Select(c => c.ID == ID);
            if (user == null)
            {
                error = ErrorMessage.User.notValidID;
                return false;
            }
            user.Deleted = true;
            bool updated = repository.update(user);
            if (!updated)
            {
                error = ErrorMessage.deleteError;
            }
            return updated;
        }


        public bool update(int ID, string name, string lastName, string mobile, int cityID, int userGroupID, out string error)
        {
            error = "";
            Domain.Module.User._User.User user = repository.Select(c => c.ID == ID);
            if (user == null)
            {
                error = ErrorMessage.User.notValidID;
                return false;
            }



            bool cityExsist = cityService.exist(cityID);
            if (!cityExsist)
            {
                error = ErrorMessage.City.notvalidID;
                return false;
            }

            bool isDuplicate = this.isDuplicate(mobile, ID);
            if (isDuplicate)
            {
                error = ErrorMessage.User.alreadyExsistMobile;
                return false;
            }

            user.Name = name;
            user.LastName = lastName;
            user.Mobile = mobile;
            user.CityID = cityID;
            user.UserGroupID = userGroupID;

            bool updated = repository.update(user);
            if (!updated)
            {
                error = ErrorMessage.insertError;
                return false;
            }
            return true;


        }



        public UserForPanelDto getUserProfile(int userID, out string error)
        {
            error = "";
            UserForPanelDto dto = new UserForPanelDto();
            Domain.Module.User._User.User user = repository.Select(c => c.ID == userID);
            if (user == null)
            {
                error = ErrorMessage.loadError;
                return dto;
            }
            dto = new UserForPanelDto(user);
            return dto;
        }

        public bool delete(string username)
        {
            bool deleted = repository.delete(c => c.Username == username);
            return deleted;
        }


        public Domain.Module.User._User.User selectModelByID(int ID)
        {
            var user = repository.Select(c => c.ID == ID);
            return user;
        }


        public Domain.Module.User._User.User selectModel(Expression<Func<Domain.Module.User._User.User, bool>> expression)
        {
            var user = repository.Select(expression);
            return user;
        }



        public bool sendSMS(string message, string mobile)
        {
            try
            {
                var token = new Token().GetToken(G.smsPannel_userApiKey, G.sms_secretKey);

                var ultraFastSend = new UltraFastSend()
                {
                    Mobile = long.Parse(mobile),
                    TemplateId = Int32.Parse(G.smsTemplate),
                    ParameterArray = new List<UltraFastParameters>()
                        {
                            new UltraFastParameters()
                            {
                                Parameter = "VerificationCode" , ParameterValue = message
                            }
                        }.ToArray()

                };

                UltraFastSendRespone ultraFastSendRespone = new UltraFast().Send(token, ultraFastSend);

                if (ultraFastSendRespone.IsSuccessful)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool sendSMS(string message, string mobile, int smsTemplateCode)
        {
            try
            {
                var token = new Token().GetToken(G.smsPannel_userApiKey, G.sms_secretKey);

                var ultraFastSend = new UltraFastSend()
                {
                    Mobile = long.Parse(mobile),
                    TemplateId = smsTemplateCode,
                    ParameterArray = new List<UltraFastParameters>()
                        {
                            new UltraFastParameters()
                            {
                                Parameter = "VerificationCode" , ParameterValue = message
                            }
                        }.ToArray()

                };

                UltraFastSendRespone ultraFastSendRespone = new UltraFast().Send(token, ultraFastSend);

                if (ultraFastSendRespone.IsSuccessful)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Domain.Module.User._User.User getUserModelByToken(RequestContext actionContext)
        {
            var context = actionContext;

            ClaimsIdentity claimsIdentity;
            try
            {
                claimsIdentity = context.HttpContext.User.Identity as ClaimsIdentity;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            string username = "";
            try
            {
                username = claimsIdentity.Claims.ToList()[0].Value.ToString();
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            Domain.Module.User._User.User user = repository.selectByUserName(username);
            if (user == null)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            return user;
        }


        public UserDto getUserDtoByToken(RequestContext actionContext)
        {
            var user = this.getUserModelByToken(actionContext);
            UserDto userDto = new UserDto(user);
            return userDto;
        }

        public List<DropDto> getListWith(int ID)
        {
            List<DropDto> dtos = new List<DropDto>();
            var users = repository.SelectListWith(ID);
            if (users == null)
            {
                return dtos;
            }
            foreach (var item in users)
            {
                DropDto dto = new DropDto();
                dto.ID = item.ID;
                dto.Title = item.Name + " " + item.LastName;
                dtos.Add(dto);
            }
            return dtos;
        }


        public List<DropDto> getDropListWith(DropQM qm)
        {
            List<DropDto> dtos = new List<DropDto>();

            Expression<Func<Domain.Module.User._User.User, bool>> expression = c => (c.ID == qm.ID || !c.Deleted)
            && (string.IsNullOrEmpty(qm.Value) || c.Name.Contains(qm.Value) || c.LastName.Contains(qm.Value));

            Expression<Func<Domain.Module.User._User.User, int>> orderBy = p => (p.ID == qm.ID) ? 0 : 1;
            var users = repository.SelectDropList(expression, qm.Skip, qm.Take, orderBy);

            if (users == null)
            {
                return dtos;
            }
            foreach (var item in users)
            {
                DropDto dto = new DropDto();
                dto.ID = item.ID;
                dto.Title = item.Name + " " + item.LastName;
                dtos.Add(dto);
            }
            return dtos;
        }










        public bool createListFromExcel(int userID, HttpPostedFile file, out string error)
        {
            error = "";

            string uploadedFile = this.uploadFileToAppData(file, FileUrl.excelImports);
            if (uploadedFile == "")
            {
                error = ErrorMessage.uploadError;
                return false;
            }
            var filePath = FileUrl.excelImports + "/" + uploadedFile;
            var path = Path.Combine(HttpContext.Current.Server.MapPath("~/App_Data/" + filePath + ""));
            var userGroup = userGroupRepository.Select(c => c.ID == 2);



            using (XLWorkbook wb = new XLWorkbook(path))
            {
                var ws = wb.Worksheets.First();
                var range = ws.RangeUsed();

                for (int i = 2; i < range.RowCount() + 1; i++)
                {
                    var row = ws.Rows(i.ToString());
                    var cells = row.Cells();
                    var factory = new ExcelImportedUserFactory(userID);
                    bool seted = factory.setDate(cells);
                    if (!seted)
                    {
                        error = ErrorMessage.ExcelImportedDate.insertErrorInRow(i);
                        return false;
                    }
                    
                    var user = factory.GetUser();
                    user = repository.insert(user);
                    if (user == null)
                    {
                        error = ErrorMessage.ExcelImportedDate.insertErrorInRow(i);
                        return false;
                    }
                }
            }

            return true;
        }

        public string uploadFileToAppData(HttpPostedFile file, string folderPath)
        {
            var filename = Path.GetFileName(file.FileName);
            string fileExtention = Path.GetExtension(filename).Replace(".", "");
            filename = Guid.NewGuid().ToString("N").ToUpper() + "." + fileExtention;
            var path = Path.Combine(HttpContext.Current.Server.MapPath("~/App_Data/" + folderPath + ""), filename);
            try
            {
                file.SaveAs(path);
                //return path;
                return (filename);

            }
            catch (Exception e)
            {
                return "";
            }
        }


        public string uploadFile(HttpPostedFile file, string folderPath)
        {
            var filename = Path.GetFileName(file.FileName);
            string fileExtention = Path.GetExtension(filename).Replace(".", "");
            filename = Guid.NewGuid().ToString("N").ToUpper() + "." + fileExtention;
            var path = Path.Combine(HttpContext.Current.Server.MapPath("~/" + folderPath + ""), filename);


            if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + folderPath + ""))))
                Directory.CreateDirectory(Path.Combine(HttpContext.Current.Server.MapPath("~/" + folderPath + "")));

            try
            {
                file.SaveAs(path);
                //return path;
                return (filename);

            }
            catch (Exception e)
            {
                return "";
            }
        }

        public string uploadProfileImage(int ID  , HttpPostedFile file , out string error)
        {
            error = "";
            var user = repository.Select(c => c.ID == ID);
            if (user == null)
            {
                error = ErrorMessage.User.notValidID;
                return "";
            }

            string uploadedUrl = this.uploadFile(file, FileUrl.test);
            if (uploadedUrl == "")
            {
                error = ErrorMessage.uploadError;
                return "";
            }
            user.ProfileImageUrl = uploadedUrl;
            bool updated = repository.update(user);
            if (!updated)
            {
                error = ErrorMessage.insertError;
                return "";
            }
            return uploadedUrl;
        }

        public bool addStationToUser(int creatorUserID, int userID, int stationID, out string error)
        {
            throw new NotImplementedException();
        }

        public bool deltedStattion(int userID, int stationID)
        {
            throw new NotImplementedException();
        }

        public List<int> getUserAllowedWarehouses(int ID)
        {
            throw new NotImplementedException();
        }

        public List<SelectedDropWithIconDto> getUsersForProject(int projectID)
        {
            throw new NotImplementedException();
        }
    }
}
