﻿

using asanito.Domain.SeedWorks.Base;
using asanito.Domain.SeedWorks.Validator;
using asanito.Infrastructure.Consts;
using asanito.Infrastructure.Helpers;
using asanito.Repository.SeedWorks.Base;
using asanito.Service.Module.Message;
using asanito.Service.Module.Message.User;

using SmsIrRestful;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Routing;

namespace asanito.Service.SeedWorks.Base
{
    public class Service<T> : IService<T> where T : BaseEntity
    {

        public int UserID { get; set; }

        public  virtual void setCurrentUser(int userID)
        {
            this.UserID = userID;
            _repository.setCurrentUser(userID);
        }
        

        private readonly IRepository<T> _repository;

        public Service(IRepository<T> repository)
        {
            _repository = repository;
            //user = null;
        }




        public T selectModelByID(int ID)
        {
            T model = _repository.Select(c => c.ID == ID);
            return model;
        }

        public T selectModel(Expression<Func<T, bool>> expression)
        {
            T model = _repository.Select(expression);
            return model;
        }

        public List<T> selectModelList()
        {
            List<T> model = _repository.Select();
            return model;
        }

        public List<T> selectModelList(Expression<Func<T, bool>> expression)
        {
            List<T> model = _repository.SelectList(expression);
            return model;
        }

        


        public T insert(T model)
        {
            model = _repository.insert(model);
            return model;
        }

        public T validateAndInsert<V>(T model) where V : BaseValidator<T>
        {
            model = _repository.validateAndInsert<V>(model);
            return model;
        }


        public bool delete(T model)
        {
            bool deleted = _repository.delete(model);
            return deleted;
        }

        public bool delete(Expression<Func<T, bool>> expression)
        {
            bool deleted = _repository.delete(expression);
            return deleted;
        }

        public bool update(T model)
        {
            bool updated = _repository.update(model);
            return updated;
        }




        public string getUserName(ClaimsIdentity identity)
        {
            string username;
            var _identity = identity;
            var claims = identity.Claims.Select(c => new
            {
                subject = c.Subject.Name,
                type = c.Type,
                value = c.Value
            }).ToList();
            username = claims[0].value;

            return username;
        }


        public UserDto getUserByToken(ClaimsIdentity identity)
        {
            string userName = getUserName(identity);
            var user = _repository.selectByUserName(userName);
            UserDto userDto = new UserDto();
            userDto.UserName = userName;
            userDto.ID = user.ID;
            userDto.UserGroupID = user.UserGroupID??0;
            userDto.LastName = user.LastName;
            userDto.Mobile = user.Mobile;
            userDto.Name = user.Name;



            return userDto;
        }





        //public List<Tdto> selectList(Expression<Func<T, bool>> expression)
        //{
        //    List<T> modelList = _repository.SelectList(expression);
        //    List<Tdto> dtoList = new List<Tdto>();
        //    if (modelList == null)
        //    {
        //        return dtoList;
        //    }

        //    for (int i = 0; i < modelList.Count; i++)
        //    {
        //        Tdto dto = (Tdto)Activator.CreateInstance(typeof(Tdto), BindingFlags.OptionalParamBinding |
        //                BindingFlags.InvokeMethod |
        //                BindingFlags.CreateInstance,
        //                null, new object[] { modelList[i] }
        //                , CultureInfo.InvariantCulture);
        //        dtoList.Add(dto);
        //    }
        //    return dtoList;
        //}

        //public List<Tdto> selectList()
        //{
        //    List<T> modelList = _repository.SelectList();
        //    List<Tdto> dtoList = new List<Tdto>();

        //    for (int i = 0; i < modelList.Count; i++)
        //    {
        //        Tdto dto = (Tdto)Activator.CreateInstance(typeof(Tdto), BindingFlags.OptionalParamBinding |
        //                BindingFlags.InvokeMethod |
        //                BindingFlags.CreateInstance,
        //                null, new object[] { modelList[i] }
        //                , CultureInfo.InvariantCulture);
        //        dtoList.Add(dto);
        //    }
        //    return dtoList;
        //}




        public void checkPermission(int controllerMethodID, ClaimsIdentity identity)
        {
            //UserDto userDto = this.getUserByToken(identity);

            //UserControllerMethod user_CotrollerMethod = _repository.checkPermission(userDto.ID, controllerMethodID);
            //if (user_CotrollerMethod == null)
            //{
            //    var resp = new HttpResponseMessage(HttpStatusCode.Forbidden)
            //    {
            //        Content = new StringContent("{\"Message\": \"  " + ErrorMessage.AccessIsDenied + "   \"}", Encoding.UTF8, "application/json"),
            //        ReasonPhrase = "Forbidden",
            //    };
            //    throw new HttpResponseException(resp);
            //}

        }


        public string createRandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public bool sendSMS(string message, string mobile)
        {
            try
            {
                var token = new Token().GetToken(G.smsPannel_userApiKey, G.sms_secretKey);

                var ultraFastSend = new UltraFastSend()
                {
                    Mobile = long.Parse(mobile),
                    TemplateId = Int32.Parse(G.smsTemplate),
                    ParameterArray = new List<UltraFastParameters>()
                        {
                            new UltraFastParameters()
                            {
                                Parameter = "VerificationCode" , ParameterValue = message
                            }
                        }.ToArray()

                };

                UltraFastSendRespone ultraFastSendRespone = new UltraFast().Send(token, ultraFastSend);

                if (ultraFastSendRespone.IsSuccessful)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool sendSMS(string message, string mobile,int smsTemplateCode)
        {
            try
            {
                var token = new Token().GetToken(G.smsPannel_userApiKey, G.sms_secretKey);

                var ultraFastSend = new UltraFastSend()
                {
                    Mobile = long.Parse(mobile),
                    TemplateId = smsTemplateCode,
                    ParameterArray = new List<UltraFastParameters>()
                        {
                            new UltraFastParameters()
                            {
                                Parameter = "VerificationCode" , ParameterValue = message
                            }
                        }.ToArray()

                };

                UltraFastSendRespone ultraFastSendRespone = new UltraFast().Send(token, ultraFastSend);

                if (ultraFastSendRespone.IsSuccessful)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public int getCount(Expression<Func<T, bool>> expression)
        {
            int cnt = _repository.CNT(expression);
            return cnt;
        }

        public int getCount()
        {
            int cnt = _repository.CNT();
            return cnt;
        }


        public HttpResponseMessage downloadFromAppData(string path)
        {
            //var path = @"E:\97_programming\asanitoh\BackEnd\GOD_Project\GOD.WebApi\App_Data\learning\video\0AC3355714664980A866ED68F7D34815.jpg";
            //path = "App_Data/" + path;

            path = Path.Combine(HttpContext.Current.Server.MapPath("~/App_Data/" + path + ""));



            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            result.Content = new StreamContent(stream);

            //result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "myFile.jpg" };
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = Path.GetFileName(path) };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        public HttpResponseMessage download(string path)
        {
            //var path = @"E:\97_programming\asanitoh\BackEnd\GOD_Project\GOD.WebApi\App_Data\learning\video\0AC3355714664980A866ED68F7D34815.jpg";

            path = Path.Combine(HttpContext.Current.Server.MapPath("~/" + path + ""));



            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            result.Content = new StreamContent(stream);

            //result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "myFile.jpg" };
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = Path.GetFileName(path) };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }

        public string getBase64File(string path, bool fromAppData)
        {
            if (fromAppData)
            {
                path = Path.Combine(HttpContext.Current.Server.MapPath("~/App_Data/" + path + ""));
            }
            else
            {
                path = Path.Combine(HttpContext.Current.Server.MapPath("~/" + path + ""));
            }


            byte[] bytes = File.ReadAllBytes(path);

            string file = Convert.ToBase64String(bytes);
            return file;

        }

        public string getBase64File(string path)
        {

            byte[] bytes = File.ReadAllBytes(path);

            string file = Convert.ToBase64String(bytes);
            return file;

        }

        public string uploadFile(HttpPostedFile file, string folderPath)
        {
            var filename = Path.GetFileName(file.FileName);
            string fileExtention = Path.GetExtension(filename).Replace(".", "");
            filename = Guid.NewGuid().ToString("N").ToUpper() + "." + fileExtention;
            var path = Path.Combine(HttpContext.Current.Server.MapPath("~/" + folderPath + ""), filename);


            if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + folderPath + ""))))
                Directory.CreateDirectory(Path.Combine(HttpContext.Current.Server.MapPath("~/" + folderPath + "")));

            try
            {
                file.SaveAs(path);
                //return path;
                return (filename);

            }
            catch (Exception e)
            {
                return "";
            }
        }

        public string uploadFileToAppData(HttpPostedFile file, string folderPath)
        {
            var filename = Path.GetFileName(file.FileName);
            string fileExtention = Path.GetExtension(filename).Replace(".", "");
            filename = Guid.NewGuid().ToString("N").ToUpper() + "." + fileExtention;
            var path = Path.Combine(HttpContext.Current.Server.MapPath("~/App_Data/" + folderPath + ""), filename);
            try
            {
                file.SaveAs(path);
                //return path;
                return (filename);

            }
            catch (Exception e)
            {
                return "";
            }
        }

        public bool deleteFile(string filePath)
        {
            try
            {
                filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/" + filePath + ""));

                File.Delete(filePath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool encryptFile(string fileUrl, string encryptedFileUrl)
        {
            //var fileUrl = @"E:\97_programming\asanitoh\BackEnd\GOD_Project\GOD.WebApi\App_Data\learning\video\0AC3355714664980A866ED68F7D34815.jpg";
            //var encryptedFileUrl = @"E:\97_programming\asanitoh\BackEnd\GOD_Project\GOD.WebApi\App_Data\learning\video\ss.jpg";

            fileUrl = Path.Combine(HttpContext.Current.Server.MapPath("~/" + fileUrl + ""));
            encryptedFileUrl = Path.Combine(HttpContext.Current.Server.MapPath("~/" + encryptedFileUrl + ""));

            try
            {
                byte[] fileBytes = File.ReadAllBytes(fileUrl);
                String skey = "!qAzx@#Rv0m%Kz^A";
                RijndaelManaged r = GetRijndaelManaged(skey);
                byte[] encrypted = Encrypt(fileBytes, r);
                File.WriteAllBytes(encryptedFileUrl, encrypted);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        private static byte[] Encrypt(byte[] plainBytes, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        private static RijndaelManaged GetRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        public bool breakFile(string fileUrl, string brokenFileDirectory)
        {
            try
            {

                fileUrl = Path.Combine(HttpContext.Current.Server.MapPath("~/" + fileUrl + ""));
                brokenFileDirectory = Path.Combine(HttpContext.Current.Server.MapPath("~/" + brokenFileDirectory + ""));

                if (!Directory.Exists(brokenFileDirectory))
                    Directory.CreateDirectory(brokenFileDirectory);

                //int brokenFileCNT = 5;
                byte[] fileBytes = File.ReadAllBytes(fileUrl);
                long fileLen = fileBytes.Length;
                long partSize = fileLen / 5;

                byte[] p1 = new byte[partSize];
                byte[] p2 = new byte[partSize];
                byte[] p3 = new byte[partSize];
                byte[] p4 = new byte[partSize];
                byte[] p5;

                long lastpartSize;
                if (fileLen % 5 == 0)
                {
                    lastpartSize = partSize;
                }
                else
                {
                    lastpartSize = fileLen - (partSize * 4);
                }

                p5 = new byte[lastpartSize];

                long nextIndex = partSize;

                Array.Copy(fileBytes, 0, p1, 0, partSize);

                Array.Copy(fileBytes, nextIndex, p2, 0, partSize);
                nextIndex += partSize;

                Array.Copy(fileBytes, nextIndex, p3, 0, partSize);
                nextIndex += partSize;

                Array.Copy(fileBytes, nextIndex, p4, 0, partSize);
                nextIndex += partSize;

                Array.Copy(fileBytes, nextIndex, p5, 0, lastpartSize - 1);

                File.WriteAllBytes(brokenFileDirectory + "p1.pok", p1);
                File.WriteAllBytes(brokenFileDirectory + "p2.pok", p2);
                File.WriteAllBytes(brokenFileDirectory + "p3.pok", p3);
                File.WriteAllBytes(brokenFileDirectory + "p4.pok", p4);
                File.WriteAllBytes(brokenFileDirectory + "p5.pok", p5);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }



        }


        public string uploadAndBreakFile(HttpPostedFile file, string folderPath, string brokenFileDirectory)

        {
            string result = "";
            string fileUrl = this.uploadFileToAppData(file, folderPath);
            result = fileUrl;
            if (fileUrl == "")
            {
                return "";
            }

            string rawFileUrl = "App_Data/" + folderPath + "/" + fileUrl;

            fileUrl = Path.GetFileNameWithoutExtension(fileUrl);
            bool breaked = this.breakFile(rawFileUrl, brokenFileDirectory + "/" + fileUrl);
            if (!breaked)
            {
                return "";
            }
            return result;

        }


        public string createRandomNumber(int length)
        {
            Random random = new Random();
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        //private User user;
        //public User GetUser(bool throwIfIsNull = true)
        //{
        //    if (user == null)
        //    {
        //        try
        //        {
        //            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
        //            string userName = getUserName(identity);
        //            user = _repository.selectByUserName(userName);
        //        }
        //        catch
        //        {
        //            user = null;
        //        }

        //        if (user == null && throwIfIsNull)
        //            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        //    }

        //    return user;
        //}

        public T Validate<V>(T model) where V : BaseValidator<T>
        {
            V validator = (V)Activator.CreateInstance(typeof(V), new object[] { });
            var validateResult = validator.Validate(model);

            if (!validateResult.IsValid)
            {
                string errorMessage = validateResult.Errors[0].ErrorMessage;
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("{\"Message\": \"  " + errorMessage + "   \"}", Encoding.UTF8, "application/json"),
                    ReasonPhrase = "Badrequest",
                };
                throw new HttpResponseException(resp);
            }

            return model;
        }


        public DateTime stringToDate(string sDate)
        {
            try
            {
                if (sDate == null)
                {
                    return DateTime.MinValue;
                }

                string time = sDate.Substring(10);
                sDate = sDate.Substring(0, 10); // remove hour and minute
                sDate = sDate.Replace("_", "/");

                string[] splited = sDate.Split('/');
                int year = int.Parse(splited[2]);
                int month = int.Parse(splited[0]);
                int day = int.Parse(splited[1]);

                string[] splitedTime = time.Split(':');
                int hour = Int32.Parse(splitedTime[0]);
                int minute = Int32.Parse(splitedTime[1]);

                PersianCalendar pc = new PersianCalendar();
                DateTime date = pc.ToDateTime(year, month, day, hour, minute, 0, 0);


                return date;
            }
            catch (Exception e)
            {
                return DateTime.MinValue;
            }
        }

        public DateTimeOffset stringToDateOffset(string sDate)
        {
            try
            {
                if (sDate == null)
                {
                    return DateTimeOffset.MinValue;
                }
                string time = sDate.Substring(10);
                sDate = sDate.Substring(0, 10); // remove hour and minute
                sDate = sDate.Replace("_", "/");

                string[] splited = sDate.Split('/');
                int year = int.Parse(splited[2]);
                int month = int.Parse(splited[0]);
                int day = int.Parse(splited[1]);

                string[] splitedTime = time.Split(':');
                int hour = Int32.Parse(splitedTime[0]);
                int minute = Int32.Parse(splitedTime[1]);

                PersianCalendar pc = new PersianCalendar();
                DateTimeOffset date = pc.ToDateTime(year, month, day, hour, minute, 0, 0);
                return date;
            }
            catch (Exception e)
            {
                return DateTimeOffset.MinValue;
            }
        }

        public int getUserIDByToken(RequestContext actionContext)
        {
            var context = actionContext;

            ClaimsIdentity claimsIdentity;
            try
            {
                claimsIdentity = context.HttpContext.User.Identity as ClaimsIdentity;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            string username = "";
            try
            {
                username = claimsIdentity.Claims.ToList()[0].Value.ToString();
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            Domain.Module.User._User.User user = _repository.selectByUserName(username);
            if (user == null)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            return user.ID;
        }

        public int getCurrentUser(RequestContext actionContext)
        {
            var context = actionContext;

            ClaimsIdentity claimsIdentity;
            try
            {
                claimsIdentity = context.HttpContext.User.Identity as ClaimsIdentity;
            }
            catch (Exception e)
            {
                return 0;
            }

            string username = "";
            try
            {
                username = claimsIdentity.Claims.ToList()[0].Value.ToString();
            }
            catch (Exception e)
            {
                return 0;
            }

            Domain.Module.User._User.User user = _repository.selectByUserName(username);
            if (user == null)
            {
                return 0;
            }
            return user.ID;
        }


        public Domain.Module.User._User.User getUserModelByToken(RequestContext actionContext)
        {
            var context = actionContext;

            ClaimsIdentity claimsIdentity;
            try
            {
                claimsIdentity = context.HttpContext.User.Identity as ClaimsIdentity;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            string username = "";
            try
            {
                username = claimsIdentity.Claims.ToList()[0].Value.ToString();
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            Domain.Module.User._User.User user = _repository.selectByUserName(username);
            if (user == null)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            return user;
        }


        public long calcDiscountedPrice(int offPercent, long price)
        {
            long discountedAmount = (price * offPercent) / 100;
            long discountedPrice = price - discountedAmount;
            return discountedPrice;

        }

        public string getFileType(string fileName)
        {
            
            string extention = "";
            for (int i = fileName.Length - 1; i != 0; i--)
            {
                if (fileName.Substring(i, 1) == ".")
                {
                    extention = fileName.Substring(i + 1);
                    break;
                }
            }


            return extention;
        }

        public bool deleteOrSetAsDeleted(int ID)
        {
            bool deleted = _repository.deleteOrSetAsDeleted(ID);
            return deleted;
        }


        public string createRandomRGB()
        {
            string[] colorCodes = new string[]
            {
                "0" ,
                "1" ,
                "2" ,
                "3" ,
                "4" ,
                "5" ,
                "6" ,
                "7" ,
                "8" ,
                "9" ,
                "A" ,
                "B" ,
                "C" ,
                "D" ,
                "E" ,
                "F"
            };


            string rgbColor = "";
            Random random = new Random();
            for (int i = 0; i < 6; i++)
            {
                int index = random.Next(0, 15);
                rgbColor += colorCodes[index];
            }
            
            return rgbColor;


        }


        public bool setAsDeleted(int ID , out string error)
        {
            error = "";
            bool updated = _repository.setAsDeleted(ID, out error);
            return updated;
        }


        public Expression createExpression(string prop, object value, Expression x)
        {
            try
            {
                MethodInfo containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
                Expression currentBody;

                var para = prop.Split('.').Aggregate<string, Expression>(x, Expression.PropertyOrField);
                var val = Expression.Constant(value.ToString());
                MethodInfo toStringMethod = null;


                if (para.Type == typeof(string))
                {
                    currentBody = Expression.Call(para, containsMethod, val);
                }
                else if (para.Type == typeof(DateTime) || para.Type == typeof(DateTimeOffset))
                {
                    toStringMethod = para.Type.GetMethod("ToString", System.Type.EmptyTypes);
                    var toStringExpr = Expression.Call(para, toStringMethod);
                    var stringDate = G.incompleteShamsiToStringDate(value.ToString());
                    
                    var dateval = Expression.Constant(stringDate);
                    
                    currentBody = Expression.Call(toStringExpr, containsMethod, dateval);
                   

                }
           
                else if (para.Type.BaseType == (typeof(Enum)))
                {
                    var enumValue = EnumEx.GetValueFromDescription(value.ToString(), para.Type);
                    if (enumValue == null)
                    {
                        return null;
                    }
                    var enumValExpression = Expression.Constant(enumValue);
                    currentBody = Expression.Equal(para, enumValExpression);
                }
                else
                {
                    toStringMethod = para.Type.GetMethod("ToString", System.Type.EmptyTypes);
                    var toStringExpr = Expression.Call(para, toStringMethod);
                    currentBody = Expression.Equal(toStringExpr, val);
                }

                return currentBody;
            }
            catch (Exception e)
            {
                return null;
            }
        }

       }
    }

