﻿using asanito.Domain.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Service.SeedWorks.Base
{
    public class BaseDto
    {
        public int ID { get; set; }
        public bool Deleted { get; set; }

        public string Description { get; set; }

        public BaseDto(BaseEntity model)
        {
            this.ID = model.ID;
            this.Deleted = model.Deleted;
            this.Description = model.Description;
        }

        public BaseDto()
        {

        }
    }
}
