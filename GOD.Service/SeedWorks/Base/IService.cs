﻿using asanito.Domain.SeedWorks.Base;
using asanito.Domain.SeedWorks.Validator;
using asanito.Service.Module.Message.User;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Routing;

namespace asanito.Service.SeedWorks.Base
{
    public interface IService<T> where T : BaseEntity
    {

        void setCurrentUser(int userID);

        int getCurrentUser(RequestContext actionContext);

        T selectModelByID(int ID);
        T selectModel(Expression<Func<T, bool>> expression);

        List<T> selectModelList();
        List<T> selectModelList(Expression<Func<T, bool>> expression);


        T insert(T model);
        T validateAndInsert<V>(T model) where V : BaseValidator<T>;

        

        bool delete(Expression<Func<T, bool>> expression);

        bool update(T model);


        UserDto getUserByToken(ClaimsIdentity identity);

        int getUserIDByToken(RequestContext actionContext);

        string getUserName(ClaimsIdentity identity);


        //List<Tdto> selectList(Expression<Func<T, bool>> expression);
        //List<Tdto> selectList();


        void checkPermission(int controllerMethodID, ClaimsIdentity identity);

        string uploadFile(HttpPostedFile file, string folderPath);
        string uploadFileToAppData(HttpPostedFile file, string folderPath);




        string createRandomString(int length);

        bool sendSMS(string message, string mobile);
        bool sendSMS(string message, string mobile, int smsTemplateCode);

        int getCount(Expression<Func<T, bool>> expression);
        int getCount();

        HttpResponseMessage downloadFromAppData(string path);
        HttpResponseMessage download(string path);
        bool deleteFile(string filePath);

        string getBase64File(string path, bool fromAppData);
        string getBase64File(string path);

        bool encryptFile(string fileUrl, string encryptedFileUrl);

        bool breakFile(string fileUrl, string brokenFileDirectory);
        //Domain.Module.User._User.User GetUser(bool throwIfIsNull = true);
        T Validate<V>(T model) where V : BaseValidator<T>;

        DateTime stringToDate(string sDate);
        DateTimeOffset stringToDateOffset(string sDate);

        asanito.Domain.Module.User._User.User getUserModelByToken(RequestContext actionContext);


        long calcDiscountedPrice(int offPercent, long price);

        string getFileType(string fileName);

        bool deleteOrSetAsDeleted(int ID );

        string createRandomRGB();

        bool setAsDeleted(int ID, out string error);

        Expression createExpression(string prop, object value, Expression x);
    }
}
