﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.Module.User._UserGroup
{
    public class SimpleUserGroup : UserGroup
    {
        /// <summary>
        /// عنوان
        /// </summary>
        public override string Title { get; set; }

        /// <summary>
        /// navigationProperty
        /// <para>کاربران گروه کاربری</para>
        /// </summary>
        public override List<Domain.Module.User._User.User> Users { get; set; }


        public SimpleUserGroup(string title , string description , DateTime creteDate , int cretorUserID)
        {
            this.Title = title;
            this.Description = description;
            this.CreateDate = creteDate;
            this.CreatorUserID = cretorUserID;
            Users = new List<Domain.Module.User._User.User>();
        }

        public SimpleUserGroup()
        {

        }
    }
}
