﻿using asanito.Domain.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.Module.User._UserGroup.Factory
{
    public class SimpleUserGroupFactory : UserGroupFactory 
    {
        private string title;
        public override UserGroup getUserGroup()
        {
            return new SimpleUserGroup(title,this.Description,CreateDate,CreatorUerID);
        }

        public SimpleUserGroupFactory(int cretorUserID ,string title,string description)  :base(cretorUserID,description)
        {
            this.title = title;
        }
    }
}
