﻿using asanito.Domain.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.Module.User._UserGroup.Factory
{
    public abstract class UserGroupFactory : BaseFactory
    {
        public abstract UserGroup getUserGroup();
        public UserGroupFactory(int cretorUserID , string description="")  :base(cretorUserID,description)
        {

        }

    }
}
