﻿
using asanito.Domain.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.Module.User
{
    /// <summary>
    /// گروه کاربری
    /// </summary>
    public abstract class UserGroup : BaseEntity
    {
        /// <summary>
        /// عنوان
        /// </summary>
        public virtual string Title { get; set; }


        /// <summary>
        /// navigationProperty
        /// <para>کاربران گروه کاربری</para>
        /// </summary>
        public virtual List<Domain.Module.User._User.User> Users { get; set; }



    }
}
