﻿using asanito.Domain.SeedWorks.Base;
using asanito.Domain.Module.Address;
using asanito.Infrastructure.Enum.User;
using System;
using System.Collections.Generic;


namespace asanito.Domain.Module.User._User
{
    /// <summary>
    /// کاربر
    /// </summary>
    public abstract class User  
    {
        public int ID { get; set; }

        /// <summary>
        /// برای وقتی استفاده میشود که بخواهیم از طریق اکسل یا ... از یک دیتابیس قدیمی به دیتابیس جدید انتقال بدهیم
        /// </summary>
        public int? OldSystemID { get; set; }

        /// <summary>
        /// نام
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// نام خانوادگی
        /// </summary>
        public virtual string LastName { get; set; }


        /// <summary>
        /// نام کاربری
        /// </summary>
        public virtual string Username { get; set; }

        /// <summary>
        /// شماره موبایل
        /// </summary>
        public virtual string Mobile { get; set; }
        

        /// <summary>
        /// وضعیت کاربر
        /// </summary>
        public virtual UserStatus UserStatus { get; set; }

        /// <summary>
        /// تاریخ ثبت نام کاربر در سایت
        /// </summary>
        public virtual DateTime RegisterDate { get; set; }

        public bool Deleted { get; set; }

        /// <summary>
        /// آدرس ایمیل
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// رمز عبور برای ارسال ایمیل
        /// <para> to do : not a saf way to save password </para>
        /// </summary>
        public string AppPassword { get; set; }

     

        /// <summary>
        /// کاید خارجی
        /// <para>آی دی شهر</para>
        /// </summary>
        public virtual int? CityID { get; set; }

        /// <summary>
        /// کلید خارجی
        /// <para>آی دی گروه کاربری</para>
        /// </summary>
        public virtual int? UserGroupID { get; set; }

        ///// <summary>
        ///// کلید خارجی
        ///// <para>آی دی مرکز</para>
        ///// </summary>
        //public int? StationID { get; set; }
        

        public int? CreatorUserID { get; set; }

        public DateTime CreateDate { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// آدرس عکس پروفایل
        /// </summary>
        public string ProfileImageUrl { get; set; }


        /// <summary>
        /// navigationProperty
        /// <para>گروه کاربری</para>
        /// </summary>
        public virtual UserGroup UserGroup { get; set; }

        /// <summary>
        /// navigationProperty
        /// <para>شهر</para>
        /// </summary>
        public virtual City City { get; set; }



        

    }


}
