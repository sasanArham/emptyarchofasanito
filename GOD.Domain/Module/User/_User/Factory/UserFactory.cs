﻿using asanito.Domain.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.Module.User._User.Factory
{
    public abstract class UserFactory : BaseFactory
    {
        public UserFactory(int creatorUerID, string description = "") : base(creatorUerID, description)
        {
        }


        public abstract User GetUser();

        
    }
}
