﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using asanito.Infrastructure.Consts;

namespace asanito.Domain.Module.User._User.Factory
{
    public class ExcelImportedUserFactory 
    {
        private int UserID;
        private PanelCreatedUser user;

        public ExcelImportedUserFactory(int creatorUerID  )
        {
            
            this.UserID = creatorUerID;
            user = new PanelCreatedUser();
            user.CreatorUserID = UserID;
            user.CreateDate = DateTime.Now;
            
        }

        public bool setDate (IXLCells cells )
        {
            var rowCells = cells.ToList();
            try
            {
                user.OldSystemID = Int32.Parse(rowCells[0].Value.ToString());
                user.Name = rowCells[1].Value.ToString();
                user.LastName = rowCells[2].Value.ToString();
                user.Username = rowCells[3].Value.ToString();
                user.Mobile = rowCells[5].Value.ToString();
                user.RegisterDate = DateTime.Now;
                user.Email = rowCells[7].Value.ToString();
                user.AppPassword = rowCells[8].Value.ToString();
                user.Description = rowCells[9].Value.ToString();
                user.CityID = 1;// todo
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            
        }
        
        public  User GetUser()
        {
            return user;
        }

        
    }
}
