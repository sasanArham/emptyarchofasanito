﻿
using asanito.Infrastructure.Enum.User;
using asanito.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.Module.User._User.Factory
{
    public class PanelCreatedUserFactory : UserFactory
    {
        private string name;
        private string lastName;
        private string username;
        private string mobile;
        private int cityID;
        private int userGroupID;
        private string Email;
        private int[]  StationIDs ;
        private PanelCreatedUser user;
        private int userID;
        private string AppPassword;

        public override User GetUser()
        {
            user.Name = name;
            user.LastName = lastName;
            user.Username = username;
            user.Mobile = mobile;
            if (cityID != 0)
            {
                user.CityID = cityID;
            }
            
            user.UserGroupID = userGroupID;
            user.UserStatus = UserStatus.active;
            user.RegisterDate = DateTime.Now;
            user.Email = this.Email;
            user.Deleted = false;
            user.CreateDate = CreateDate;
            user.CreatorUserID = CreatorUerID;
            user.AppPassword = this.AppPassword;
         


            return user;
        }

        
   


        public PanelCreatedUserFactory(int creatorUserID , string description ,  string name 
            ,string lastName ,string username ,string mobile, int cityID , int userGroupID 
            , string email, int[] stationIDs , string appPassword) : base(creatorUserID,description)
        {
            user = new PanelCreatedUser();
            this.name = name;
            this.lastName = lastName;
            this.username = username;
            this.mobile = mobile;
            this.cityID = cityID;
            this.userGroupID = userGroupID;
            this.Email = email;
            this.StationIDs = stationIDs;
            this.userID = creatorUserID;
            if (!string.IsNullOrEmpty(appPassword) )
            {
                this.AppPassword = StringCipher.Encrypt(appPassword);
            }

        }
    }
}
