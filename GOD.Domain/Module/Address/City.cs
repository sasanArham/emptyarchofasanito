﻿using asanito.Domain.Module.Address._Adress;
using asanito.Domain.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.Module.Address
{
    /// <summary>
    /// شهر
    /// </summary>
    public class City : BaseEntity
    {
        /// <summary>
        /// نام
        /// </summary>
        public string Title { get; set; }


        /// <summary>
        /// حذف شده
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// کلید خارجی
        /// <para> آی دی استان</para>
        /// </summary>
        public int ProvinceID { get; set; }

        /// <summary>
        /// navigationProperty
        /// <para>استان</para>
        /// </summary>
        public virtual Province Province { get; set; }


        public virtual IList<Domain.Module.User._User.User> Users { get; set; }



        /// <summary>
        /// navigationProperty
        /// <para>آدرس ها</para>
        /// </summary>
        public virtual List<Adress> Adresses { get; set; }


        public City()
        {

        }


        public City(int userID , string title , int provinceID)
        {
            CreatorUserID = userID;
            CreateDate = DateTime.Now;
            Title = title;
            ProvinceID = provinceID;
        }

        
    }
}
