﻿
using asanito.Domain.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.Module.Address._Adress
{
    /// <summary>
    /// آدرس
    /// </summary>
    public class Adress  :BaseEntity
    {
        /// <summary>
        /// آدرس متنی
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// آیا پیش فرض است
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// کد بستی
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// کلید خارجی
        /// <para>آی دی شهر</para>
        /// </summary>
        public int? CityID { get; set; }

        /// <summary>
        /// کلید خارجی
        /// <para>آی دی شرکت</para>
        /// </summary>
        public int? CompanyID { get; set; }

        /// <summary>
        /// کلید خارجی
        /// <para>آی دی شخص</para>
        /// </summary>
        public int? PersonID { get; set; }

        /// <summary>
        /// کلید خارجی
        /// <para>آی دی انبار</para>
        /// </summary>
        public int? WareHouseID { get; set; }
        

        /// <summary>
        /// navigatopnProperty
        /// <para>شهر</para>
        /// </summary>
        public virtual City City { get; set; }


        


        public Adress()
        {

        }

        public Adress(int userID , int cityID , string address , bool isDefault)

        {
            this.CreatorUserID = userID;
            this.CreateDate = DateTime.Now;
            this.Deleted = false;
            if (cityID != 0)
            {
                this.CityID = cityID;
            }
            this.Address = address;
            this.IsDefault = isDefault;
        }
        public Adress(int userID, int cityID, string address, bool isDefault,string postalCode)

        {
            this.CreatorUserID = userID;
            this.CreateDate = DateTime.Now;
            this.Deleted = false;
            if (cityID != 0)
            {
                this.CityID = cityID;
            }
            this.Address = address;
            this.IsDefault = isDefault;
            this.PostalCode = postalCode;
        }






    }
}
