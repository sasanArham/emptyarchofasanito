﻿using asanito.Domain.SeedWorks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.Module.Address
{
    /// <summary>
    /// استان
    /// </summary>
    public class Province : BaseEntity
    {
        /// <summary>
        /// نام
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// حذف شده
        /// </summary>
        public bool Deleted { get; set; }


        /// <summary>
        /// navigationProperty
        /// <para>شهر ها</para>
        /// </summary>
        public virtual IList<City> Cities { get; set; }



        public Province()
        {

        }

        public Province(int userID , string title )
        {
            this.CreatorUserID = userID;
            Title = title;
            CreateDate = DateTime.Now;
        }
    }
}
