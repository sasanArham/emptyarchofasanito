﻿using asanito.Infrastructure.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.SeedWorks.Base
{
    public abstract class BaseEntity
    {
        public int ID { get; set; }

        public int CreatorUserID { get; set; }

        public DateTime CreateDate { get; set; }

        public string Description { get; set; }

        public bool Deleted { get; set; }

        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? Code { get; set; }

        [NotMapped]
        public LogableEntityType EntityType { get; set; }

        /// <summary>
        /// برای وقتی استفاده میشود که بخواهیم از طریق اکسل یا ... از یک دیتابیس قدیمی به دیتابیس جدید انتقال بدهیم
        /// </summary>
        public int? OldSystemID { get; set; }



    }
}
