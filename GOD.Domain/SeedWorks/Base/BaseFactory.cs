﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.SeedWorks.Base
{
    public class BaseFactory
    {
        protected bool Deleted;
        protected string Description;
        protected DateTime CreateDate;
        protected int CreatorUerID;
        

        public BaseFactory(int creatorUerID ,string description ="")
        {
            
            this.Deleted = false;
            this.Description = description;
            this.CreateDate = DateTime.Now;
            this.CreatorUerID = creatorUerID;
        }
    }
}
