﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.SeedWorks.Base
{
    public interface IBaseEntity
    {
        int ID { get; set; }
    }
}
