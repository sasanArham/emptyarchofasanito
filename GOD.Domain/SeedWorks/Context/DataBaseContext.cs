﻿using System;
using System.Data.Entity;
using System.Web;
using System.IO;
using asanito.Domain.Module.User;
using asanito.Domain.Module.Address;
using asanito.Domain.Module.Address._Adress;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
namespace asanito.Domain.SeedWorks.Context
{
    public class DataBaseContext : DbContext, IDataBaseContext
    {
        public DataBaseContext() : base("mainProjectConnection")
        {
            this.Initialize();
        }

        public DataBaseContext(DbConnection con) : base(con, false)
        {

        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Configurations.Add(new PriceInquiryConfig());
        }



     

      
        public DbSet<Domain.Module.User._User.User> User { get; set; }
        public DbSet<UserGroup> UserGroup { get; set; }
       
      

        public DbSet<Province> Province { get; set; }
        public DbSet<City> City { get; set; }
        


        public new void Dispose()
        {
        }

        private void Initialize()
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        static DataBaseContext()
        {
            //CreateSp();
        }

        public static void CreateSp()
        {
            try
            {
                var context = new DataBaseContext();

                var baseUrl = HttpContext.Current.Server.MapPath("~");
                var files = Directory.GetFiles(Path.Combine(baseUrl, "app_data", "sps"));

                foreach (var item in files)
                    context.Database.ExecuteSqlCommand(File.ReadAllText(item));
            }
            catch (Exception ex) { }
        }

    }
}
