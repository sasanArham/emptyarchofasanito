﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using asanito.Domain.SeedWorks.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asanito.Domain.SeedWorks.Installer
{
    public class DomainInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly().BasedOn(typeof(IDataBaseContext)).WithServiceAllInterfaces().LifestyleTransient());
            //container.Register(Component.For(typeof(DatabaseContext)).LifestyleTransient());

        }
    }
}
